<?php
namespace backend\assets;

use yii\web\AssetBundle;

class AceBootstrapAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/themes/ace/assets/';
    public $css = [
        'css/bootstrap.min.css',
        'css/ace-fonts.css',
        'css/ace.min.css',
        //Добавил для layouts/login
        //'css/font-awesome.min.css',
        //'css/bootstrap.min.css',
        //'css/ace-part2.min.css',
        //'css/ace-rtl.min.css',
        //'css/ace-ie.min.css',
        //'css/ace.onpage-help.css',

    ];
    public $js = [
        'js/bootstrap.min.js',
        'js/ace.min.js',
        'js/ace-elements.min.js',
        'js/jquery.nestable.min.js',
        'js/flot/jquery.flot.min.js',
        //Добавил для layouts/login
        //'js/jquery.min.js',
        //'js/jquery1x.min.js',
        //'js/jquery.mobile.custom.min.js'
    ];

}
