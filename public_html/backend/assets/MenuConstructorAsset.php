<?php

namespace backend\assets;

use yii\web\AssetBundle;


class MenuConstructorAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/js/';
    public $js = [
        'menu-constructor.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}