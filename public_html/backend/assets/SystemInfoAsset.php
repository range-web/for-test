<?php

namespace backend\assets;

use yii\web\AssetBundle;


class SystemInfoAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/js/';
    public $js = [
        'system-info.js',
    ];
    public $depends = [
        '\yii\web\JqueryAsset',
        '\common\assets\Flot',
    ];
}