<?php

namespace backend\components;

use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


class SortDataColumn extends DataColumn
{

    public function getDataCellValue($model, $key, $index)
    {
        $this->format = 'raw';

        if ($this->attribute !== null) {
            $val = ArrayHelper::getValue($model, $this->attribute);
            if (is_int($val)) {
                $down = ($val < $model->maxSort) ? Html::a('<i class="fa fa-chevron-down"></i>',Url::toRoute(['move', 'id' => ArrayHelper::getValue($model, 'id'), 'attribute'=>$this->attribute,'moving'=>'down']), ['class'=>'btn btn-white btn-default btn-minier btn-sort-item', 'data-grid-id'=>$this->grid->id]):null;
                $up = ($val > $model->minSort) ? Html::a('<i class="fa fa-chevron-up"></i>',Url::toRoute(['move', 'id' => ArrayHelper::getValue($model, 'id'), 'attribute'=>$this->attribute,'moving'=>'up']), ['class'=>'btn btn-white btn-default btn-minier btn-sort-item', 'data-grid-id'=>$this->grid->id]):null;
                return $val.' '.$up.$down;
            }
        }
        return null;
    }

}
