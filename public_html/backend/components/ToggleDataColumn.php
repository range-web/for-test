<?php

namespace backend\components;

use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class ToggleDataColumn extends DataColumn
{

    //public $checkedButtonLabel;
    //public $uncheckedButtonLabel;

    public function getDataCellValue($model, $key, $index)
    {
        $this->format = 'raw';
        $modelName = $model->className();
        if ($this->attribute !== null) {
            $val = ArrayHelper::getValue($model, $this->attribute);
            $checked = '';
            if ($val == 1) {
                $checked = 'checked="checked"';
                //$label = $this->checkedButtonLabel;
            } else {
                //$label = $this->uncheckedButtonLabel;
            }
            return '<label data-rel="tooltip">
                    <input data-url="'.Url::toRoute(['toggle','model'=>$modelName, 'id' => ArrayHelper::getValue($model, 'id'), 'attribute'=>$this->attribute]).'" name="switch-field-1" class="ace ace-switch ace-switch-6 btn-toggle-published" type="checkbox" '.$checked.'>
                    <span class="lbl"></span>
            </label>';
        }

        return null;
    }

}
