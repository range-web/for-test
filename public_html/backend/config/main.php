<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'layoutPath' => '@backend/web/themes/ace/views/layouts',
    'viewPath' => '@backend/web/themes/ace/views',
    'bootstrap' => ['log'],
    'homeUrl' => '/',
    'components' => [
        'request' => [
            'baseUrl' => '/admin',
        ],
        'user' => [
            'identityClass' => 'common\modules\users\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => false,
            'enableStrictParsing' => false,
            'showScriptName' => true,
            'rules' => [
                '/' => 'site/index',
                'login' => 'site/default/login',
               // '/' => 'debts/',
                '<controller>/<action>' => '<controller>/<action>',
                '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
            ]
        ],

    ],
    'modules' => [
        'filesystem' => [
            'class' => 'rangeweb\filesystem\Module',
        ],
        'users' => [
            'class' => 'backend\modules\users\Module',
        ],
        'rbac' => [
            'class' => 'backend\modules\rbac\Module',
        ],
        'pages' => [
            'class' => 'backend\modules\pages\Module',
        ],
        'contacts' => [
            'class' => 'backend\modules\contacts\Module',
        ],

    ],

    'params' => $params,
];
