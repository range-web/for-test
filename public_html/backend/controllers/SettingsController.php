<?php
namespace backend\controllers;

use backend\components\AdminController;
use common\models\search\SettingsSearch;
use common\models\Settings;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SettingsController extends AdminController
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {

            if (!\Yii::$app->user->isGuest && !(Yii::$app->user->can('superadmin') || Yii::$app->user->can('admin'))) {
                throw new ForbiddenHttpException('Доступ запрещен');
            }

            return true;
        } else {
            return false;
        }
    }

    public function actionIndex()
    {
        $searchModel = new SettingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Успешно сохранено!');
            return $this->redirect(['index']);
        } else {

            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the PagesAdmin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Settings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Settings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
