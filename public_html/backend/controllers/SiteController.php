<?php
namespace backend\controllers;

use backend\components\AdminController;
use common\modules\users\models\User;
use vova07\imperavi\actions\GetAction;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\web\Controller;
use common\modules\users\models\LoginForm;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends AdminController
{
    public $layout;

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCache()
    {
        if (isset($_REQUEST['clear_cache'])) {

            if ($_REQUEST['clear_cache'] == 'all') {
                $cachePath = FileHelper::normalizePath(Yii::getAlias('@cache'));
                $cacheFiles = array_diff(scandir($cachePath), array('.','..'));
                foreach ($cacheFiles as $cacheFile) {
                    if (is_dir("$cachePath/$cacheFile")) {
                        FileHelper::removeDirectory("$cachePath/$cacheFile");
                    } else {
                        unlink("$cachePath/$cacheFile");
                    }
                }
            }

            $assetsPath = FileHelper::normalizePath(Yii::getAlias('@frontend/web/assets'));
            $assetsFiles = array_diff(scandir($assetsPath), array('.','..','.gitignore'));

            foreach ($assetsFiles as $assetsFile) {
                if (is_dir("$assetsPath/$assetsFile")) {
                    FileHelper::removeDirectory("$assetsPath/$assetsFile");
                } else {
                    unlink("$assetsPath/$assetsFile");
                }
            }

            $assetsPath = FileHelper::normalizePath(Yii::getAlias('@frontend/web/minify'));
            $assetsFiles = array_diff(scandir($assetsPath), array('.','..','.gitignore'));

            foreach ($assetsFiles as $assetsFile) {
                if (is_dir("$assetsPath/$assetsFile")) {
                    FileHelper::removeDirectory("$assetsPath/$assetsFile");
                } else {
                    unlink("$assetsPath/$assetsFile");
                }
            }

            Yii::$app->session->setFlash('success', 'Кеш успешно очищен!');
        }

        return $this->render('cache');
    }

    public static function delFolder($dir)
    {
        $files = array_diff(scandir($dir), array('.','..'));

        foreach ($files as $file) {
            if (is_dir("$dir/$file")) {
                self::delFolder("$dir/$file");
            } else {
                unlink("$dir/$file");
            }
        }
    }

    public function actionLogin()
    {
        $this->layout = 'login';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            //$user = User::findByEmail($model->email);
            if ($model->login()) {
                return $this->redirect(['index']);
            }
        }
        
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionCkeImageUpload()
    {
        $uploadedFile = UploadedFile::getInstanceByName('upload');
        $mime = FileHelper::getMimeType($uploadedFile->tempName);
        $file = time()."_".$uploadedFile->name;

        $path = '@frontend/web/uploads/ckeditor/';
        $path = FileHelper::normalizePath(Yii::getAlias($path));

        if (!file_exists($path))
            mkdir($path, 0775, true);

        $url = \Yii::$app->params['frontendUrl'].'/uploads/ckeditor/'.$file;
        $uploadPath = $path.'/'.$file;

        //extensive suitability check before doing anything with the file…
        if ($uploadedFile==null)
        {
            $message = "No file uploaded.";
        }
        else if ($uploadedFile->size == 0)
        {
            $message = "The file is of zero length.";
        }
        else if ($mime!="image/jpeg" && $mime!="image/png")
        {
            $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
        }
        else if ($uploadedFile->tempName==null)
        {
            $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
        }
        else {
            $message = "";
            $move = $uploadedFile->saveAs($uploadPath);
            if(!$move)
            {
                $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
            }
        }
        $funcNum = $_GET['CKEditorFuncNum'] ;
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionFileManager()
    {
        return $this->render('filemanager');
    }
}
