<?php

namespace backend\modules\contacts\controllers;

use backend\components\AdminController;
use backend\modules\contacts\models\ContactsForm;

class DefaultController extends AdminController
{
    public function actionIndex()
    {
        $model = new ContactsForm();

        if (isset($_POST['ContactsForm'])) {

            if ($model->save($_POST['ContactsForm'])/* && $model->saveAdditional($_POST['AddContacts'])*/) {
                \Yii::$app->session->setFlash('success', 'Успешно сохранено!');
            }
        }

        $model->getData();

        return $this->render('index', [
            'model'=>$model
        ]);
    }
}
