<?php

namespace backend\modules\contacts\models;

use common\models\AdditionalContacts;
use common\models\Settings;
use yii\base\Model;

class ContactsForm extends Model
{
    public $contactAddress;
    public $contactPhone;
    public $contactEmail;
    public $sendContactFormEmail;
    public $contactAddressCoords;
    public $contactMapIcon;
    public $contactAdditionally;

    public $addPhone = [];
    public $addAddress = [];
    public $addEmail = [];

    public function rules()
    {
        return [
            [['contactEmail','sendContactFormEmail'], 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'contactAddress' => 'Адрес',
            'contactPhone' => 'Телефон',
            'contactEmail' =>'Контактный Email',
            'sendContactFormEmail' =>'Email на который будет отправляться форма обратной связи',
            'contactAddressCoords' =>'Координаты',
            'contactMapIcon' =>'Иконка на карте',
            'contactAdditionally' =>'Дополнительная информация',
        ];
    }

    public function getData()
    {

        $this->contactAddress = Settings::getActValue('contactAddress', 'contacts');
        $this->contactPhone = Settings::getActValue('contactPhone', 'contacts');
        $this->contactEmail = Settings::getActValue('contactEmail', 'contacts');
        $this->sendContactFormEmail = Settings::getActValue('sendContactFormEmail', 'contacts');
        $this->contactAddressCoords = Settings::getActValue('contactAddressCoords', 'contacts');
        $this->contactMapIcon = Settings::getActValue('contactMapIcon', 'contacts');
        $this->contactAdditionally = Settings::getActValue('contactAdditionally', 'contacts');

        $this->getAddData();
    }

    protected function getAddData()
    {
        $contacts = AdditionalContacts::find()
            ->select('value, value2, type, view_home')
            ->all();

        foreach ($contacts as $contact) {

            if ($contact->type == AdditionalContacts::TYPE_ADDRESS) {
                $this->addAddress[] = [
                    'address' =>   $contact->value,
                    'view_home' => $contact->view_home,
                ];
            } else if ($contact->type == AdditionalContacts::TYPE_EMAIL) {
                $this->addEmail[] = [
                    'email' =>   $contact->value,
                    'view_home' => $contact->view_home,
                ];
            } else if ($contact->type == AdditionalContacts::TYPE_PHONE) {
                $this->addPhone[] = [
                    'phone' =>   $contact->value,
                    'view_home' => $contact->view_home,
                ];
            }
        }
    }

    public function saveAdditional($attributes)
    {
        if (!empty($attributes)) {
            if (isset($attributes['Address'])) {
                AdditionalContacts::deleteAll(['type'=>AdditionalContacts::TYPE_ADDRESS]);

                $addresses = [];

                foreach ($attributes['Address'] as $k=>$address) {
                    $newAddress = new AdditionalContacts();
                    $newAddress->type = AdditionalContacts::TYPE_ADDRESS;
                    $newAddress->value = $address;
                    $newAddress->value2 = $this->determinationCoords($address);
                    $newAddress->view_home = (isset($attributes['AddressView'][$k]))?1:0;
                    $newAddress->save();

                    $addresses[] = [
                        'address' => $newAddress->value,
                        'coords' => $newAddress->value2,
                        'viewHome' => $newAddress->view_home
                    ];
                }

                \Yii::$app->cache->set('additionalAddresses', $addresses);

            }
            if (isset($attributes['Phone'])) {
                AdditionalContacts::deleteAll(['type'=>AdditionalContacts::TYPE_PHONE]);

                $phones = [];

                foreach ($attributes['Phone'] as $k=>$phone) {
                    $newPhone = new AdditionalContacts();
                    $newPhone->type = AdditionalContacts::TYPE_PHONE;
                    $newPhone->value = $phone;
                    $newPhone->view_home = (isset($attributes['PhoneView'][$k]))?1:0;
                    $newPhone->save();

                    $phones[] = [
                        'phone' => $newPhone->value,
                        'viewHome' => $newPhone->view_home
                    ];
                }
                \Yii::$app->cache->set('additionalPhones', $phones);
            }
            if (isset($attributes['Email'])) {
                AdditionalContacts::deleteAll(['type'=>AdditionalContacts::TYPE_EMAIL]);

                $emails = [];

                foreach ($attributes['Email'] as $k=>$email) {
                    $newEmail = new AdditionalContacts();
                    $newEmail->type = AdditionalContacts::TYPE_EMAIL;
                    $newEmail->value = $email;
                    $newEmail->view_home = (isset($attributes['EmailView'][$k]))?1:0;
                    $newEmail->save();

                    $emails[] = [
                        'email' => $newEmail->value,
                        'viewHome' => $newEmail->view_home
                    ];
                }
                \Yii::$app->cache->set('additionalEmails', $emails);
            }
        }

        return true;
    }

    protected function determinationCoords($address)
    {
        $coords = '';

        $params = array(
            'geocode' => $address, // адрес
            'format' => 'json', // формат ответа
            'results' => 1, // количество выводимых результатов
            'key' => 'ABPMRVIBAAAA1hpAIgMA0qMhzEeMYJvNZQf9bukysfqUaQcAAAAAAAAAAADMml8_35NZ0idB0Vig39vSENtD0Q==', // ваш api key
        );
        $response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params)));

        if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0)
        {
            $coords = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;

            $firstCoords = strstr($coords, ' ');
            $lastCoords = strstr($coords, ' ',true);
            $coords = $firstCoords . ',' . $lastCoords;
        }

        return $coords;
    }

    public function save($attributes)
    {
        $errors = 0;
        foreach ($attributes as $k=>$val) {
            $set = Settings::find()->where('name=:name AND module=:module', [':name'=>$k,':module'=>'contacts'])->one();
            $set->value = $val;

            if ($k == 'contactAddress') {
                $coords = Settings::find()->where('name=:name AND module=:module', [':name'=>'contactAddressCoords',':module'=>'contacts'])->one();
                $coords->value = $this->determinationCoords($val);
                $coords->save();
            }
            if (!$set->save()) {
                $errors++;
            }
        }
        return ($errors == 0) ? true : false;
    }
}