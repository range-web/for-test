<?php use common\models\Settings; ?>
<?= $form->field($model, 'contactAddress')->textarea() ?>
<?= $form->field($model, 'contactPhone')->textInput() ?>
<?= $form->field($model, 'contactEmail')->textInput() ?>
<?= $form->field($model, 'sendContactFormEmail')->textInput() ?>
<?= Settings::getRedactor($form,$model,'contactAdditionally')?>