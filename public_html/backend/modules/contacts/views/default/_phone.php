<?php
use yii\bootstrap\Html;
?>
<div class="row">
    <div class="col-lg-6 add-phone-form">
        <?php $count = 0; ?>
        <? foreach ($model->addPhone as $phone) : ?>
            <?php $count++; ?>
            <div class="form-group"  data-id="<?= $count ?>">
                <?= Html::textInput('AddContacts[Phone]['.$count.']', $phone['phone'], ['style'=>'width:100%']); ?><a href="#" class="btn btn-default btn-xs btn-del-add-contact" style="position:absolute;top:4px;right:4px;"><span class="fa fa-times"></span></a>
                <label><input type="checkbox" name="AddContacts[PhoneView][<?= $count ?>]" <?= ($phone['view_home'] == 1)?'checked="checked"':'' ?>> Показывать на главной странице</label>
            </div>
        <? endforeach ?>
    </div>
    <div class="col-lg-12">
        <a href="#" class="btn btn-info btn-xs btn-add-phone">Добавить телефон</a>
    </div>
</div>