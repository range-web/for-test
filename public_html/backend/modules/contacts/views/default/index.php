<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\pages\models\search\PagesAdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-admin-index">
    <?php $form = ActiveForm::begin(['id' => 'contacts-form']); ?>
    <div class="row">
        <div class="col-lg-12">
            <?= \yii\bootstrap\Tabs::widget([
                'items' => [
                    [
                        'label' => 'Контактные данные',
                        'content' => $this->render('_common', ['form'=>$form, 'model'=>$model]),
                        'active' => true
                    ],
                    /*[
                        'label' => 'Доп. адреса',
                        'content' => $this->render('_address', ['form'=>$form, 'model'=>$model]),
                    ],
                    [
                        'label' => 'Доп. телефоны',
                        'content' => $this->render('_phone', ['form'=>$form, 'model'=>$model]),
                    ],
                    [
                        'label' => 'Доп. email',
                        'content' => $this->render('_email', ['form'=>$form, 'model'=>$model]),
                    ],*/
                ],
            ]);
            ?>
        </div>

    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
