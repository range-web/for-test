<?php

namespace backend\modules\pages\controllers;

use backend\components\AdminController;
use common\models\PageFile;
use common\models\PageSlide;
use rangeweb\filesystem\models\File;
use Yii;
use backend\modules\pages\models\PagesAdmin;
use backend\modules\pages\models\search\PagesAdminSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DefaultController implements the CRUD actions for PagesAdmin model.
 */
class DefaultController extends AdminController
{
    public function actions()
    {
        return [
            'upload' => [
                'class' => 'rangeweb\filesystem\actions\UploadAction',
                'uploadOnlyImage' => true,
                'path' =>'images',
                'callback' => [$this, 'imageToPage']
            ],
            'delete-image' => [
                'class' => 'rangeweb\filesystem\actions\DeleteAction',
                'callback' => [$this, 'pageImagesDelete']
            ],
            'upload-file' => [
                'class' => 'rangeweb\filesystem\actions\UploadAction',
                'uploadOnlyImage' => false,
                'path' =>'files',
                'callback' => [$this, 'fileToPage']
            ],
            'delete-file' => [
                'class' => 'rangeweb\filesystem\actions\DeleteAction',
                'callback' => [$this, 'pageFilesDelete']
            ]
        ];
    }

    public function imageToPage($imageId)
    {
        $workImage = new PageSlide();
        $workImage->page_id = intval($_REQUEST['page_id']);
        $workImage->image_id = $imageId;
        $workImage->sort = 0;
        $workImage->save();
    }

    public function pageImagesDelete($imageId)
    {
        PageSlide::deleteAll('image_id = :image_id', ['image_id' => $imageId]);
    }

    public function fileToPage($fileId)
    {
        $workImage = new PageFile();
        $workImage->page_id = intval($_REQUEST['page_id']);
        $workImage->file_id = $fileId;
        $workImage->sort = 0;
        $workImage->save();
    }

    public function pageFilesDelete($fileId)
    {
        PageFile::deleteAll('file_id = :file_id', ['file_id' => $fileId]);
    }

    public function actionSaveSlide()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isAjax) {
            $model = PageSlide::find()
                    ->where([
                        'image_id' => $_POST['PageSlide']['image_id'],
                        'page_id' => $_POST['PageSlide']['page_id'],
                    ])
                    ->one();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
               return ['status' => true];
            }
        }
        return ['status' => false];
    }

    public function actionSaveFile()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isAjax) {
            $model = PageFile::find()
                ->where([
                    'file_id' => $_POST['PageFile']['file_id'],
                    'page_id' => $_POST['PageFile']['page_id'],
                ])
                ->one();

            

            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                $file = File::findOne($model->file_id);

                if ($file) {
                    $file->name = $_POST['File']['name'];
                    $file->description = $_POST['File']['description'];
                    $file->save();
                }

                return ['status' => true];
            }
        }
        return ['status' => false];
    }
    
    /**
     * Lists all PagesAdmin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PagesAdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PagesAdmin model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PagesAdmin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($parent=null)
    {
        $model = new PagesAdmin();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->parent_id) {
                $parent = PagesAdmin::find()
                    ->where(['id' => $model->parent_id])
                    ->one();
                $model->appendTo($parent);
            } else {
                $model->parent_id = 0;
                $model->makeRoot();
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Успешно добавлено!');
                PagesAdmin::updatePathsMap();

                if (isset($_POST['apply'])) {
                    return $this->redirect(['update', 'id'=>$model->id]);
                } else {
                    return $this->redirect(['index']);
                }
            }
        }

        $model->parent_id = $parent;

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing PagesAdmin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {

            if ($model->parent_id) {
                $parent = PagesAdmin::find()
                    ->where(['id' => $model->parent_id])
                    ->one();

                $model->appendTo($parent);
            } else {
                $model->parent_id = 0;
            }

            if ($model->save()) {
                PagesAdmin::updatePathsMap();
                Yii::$app->session->setFlash('success', 'Успешно сохранено!');
                if (isset($_POST['apply'])) {
                    return $this->redirect(['update', 'id'=>$model->id]);
                } else {
                    return $this->redirect(['index']);
                }
            }

        } else {

            $pageSlide = new PageSlide();

            $slidesDataProvider = new ActiveDataProvider([
                'query' => PageSlide::find()->where(['page_id' => $model->id])->orderBy('sort ASC'),
                'pagination' => [
                    'pageSize' => 9,
                ],
            ]);
            
            $pageFile = new PageFile();

            $filesDataProvider = new ActiveDataProvider([
                'query' => PageFile::find()->where(['page_id' => $model->id])->orderBy('sort ASC'),
                'pagination' => [
                    'pageSize' => 9,
                ],
            ]);
            
            /*if (Yii::$app->request->isAjax) {
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            }*/
            return $this->render('update', [
                'model' => $model,
                'slidesDataProvider' => $slidesDataProvider,
                'pageSlide' => $pageSlide,
                'filesDataProvider' => $filesDataProvider,
                'pageFile' => $pageFile
            ]);

        }
    }

    /**
     * Deletes an existing PagesAdmin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithChildren();
        Yii::$app->session->setFlash('success', 'Успешно удалено!');
        return $this->redirect(['index']);
    }

    /**
     * Finds the PagesAdmin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PagesAdmin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PagesAdmin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
