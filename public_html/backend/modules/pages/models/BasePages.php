<?php

namespace backend\modules\pages\models;

use common\models\PageFile;
use common\models\PageSlide;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;

/**
 * This is the model class for table "{{%pages}}".
 *
 * @property integer $id
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $parent_id
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $title
 * @property string $content
 * @property string $date_create
 * @property string $date_update
 * @property integer $is_published
 * @property integer $noindex
 * @property integer $access
 * @property integer $number
 * @property integer $text_redactor
 * @property integer $is_folder
 * @property integer $sort
 * @property string $layout
* @property string $styles
 * @property string $css_class
 */
class BasePages extends \yii\db\ActiveRecord
{
    public $textEditor;
    public $parents = [];

    const STATUS_PUBLISHED = 1;
    const STATUS_NO_PUBLISHED = 0;

    const PAGES_PATH_MAP = 'pagesPathsMap';

    const ACCESS_ALL = 1;
    const ACCESS_REGISTER = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages}}';
    }

    public function getStatuses()
    {
        return [
            self::STATUS_PUBLISHED => 'Да',
            self::STATUS_NO_PUBLISHED => 'нет',
        ];
    }

    public function getAccessList()
    {
        return [
            self::ACCESS_ALL => 'Все пользователи',
            self::ACCESS_REGISTER => 'Зарегестрированные пользователи',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'is_published', 'noindex', 'access', 'number', 'text_redactor', 'is_folder', 'sort'], 'integer'],
            [['title', 'slug', 'layout'], 'required'],
            [['slug'], 'unique'],
            ['layout', 'default', 'value' => 'column2'],
            [['content', 'styles'], 'string'],
            [['date_create', 'date_update'], 'safe'],
            [['slug'], 'string', 'max' => 125],
            [['css_class'], 'string', 'max' => 50],
            [['meta_description', 'meta_keywords', 'title', 'meta_title'], 'string', 'max' => 255]
        ];
    }

    public function behaviors() {
        return [
            'nestedSets' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'root',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new PagesTreeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root' => 'Root',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'depth' => 'Depth',
            'parent_id' => 'Родительская страница',
            'styles' => 'CSS стили',
            'slug' => 'Псевдоним',
            'meta_description' => 'МЕТА Описание',
            'meta_keywords' => 'МЕТА Ключевые слова',
            'title' => 'Заголовок',
            'content' => 'Текст',
            'date_create' => 'Дата создания',
            'date_update' => 'Дата последнего редактирования',
            'is_published' => 'Опубликовано',
            'meta_title' => 'МЕТА Заголовок',
            'noindex' => 'Не индексировать',
            'access' => 'Доступ к странице',
            'layout' => 'Шаблон',
            'css_class' => 'Класс иконки css',
            'number' => 'Номер страницы',
            'text_redactor' => 'Текстовый редактор',
            'sort' => 'Сортировка',
            'is_folder' => 'Папка/Каталог (по ссылке открывается первая дочерняя страница)',
        ];
    }

    /**
     * Возвращает карту путей из кеша.
     * @return mixed
     */
    public static function getPathsMap()
    {
        $pathsMap = Yii::$app->cache->get(self::PAGES_PATH_MAP);
        return $pathsMap === false ? self::generatePathsMap() : $pathsMap;
    }
    /**
     * Сохраняет в кеш актуальную на момент вызова карту путей.
     * @return void
     */
    public static function updatePathsMap()
    {
        Yii::$app->cache->set(self::PAGES_PATH_MAP, self::generatePathsMap());
    }

    /**
     * Возвращает массив страниц, в виде дерева
     * @return array
     */
    public function getPagesList()
    {
        $output = array();

        $nodes = self::find()->all();
        foreach ($nodes as $node) {
            if ($node->id == $this->id) {
                continue;
            }
            $output[$node->id] = str_repeat('---', $node->depth) . $node->title;
        }
        return $output;
    }

    public function getParentName()
    {
        $parent = PagesAdmin::find()
            ->select('title')
            ->where('id=:id', [':id'=>$this->parent_id])
            ->one();

        if ($parent == null) {
            return '';
        }

        return $parent->title;
    }

    public function getParent()
    {
        return PagesAdmin::find()
            ->select('title, number')
            ->where('id=:id', [':id'=>$this->parent_id])
            ->one();
    }

    public function getParents($parent_id)
    {
        while ($parent_id) {
            $parent = self::getParentById($parent_id, 'id, slug, title');

            if ($parent) {
                $this->parents[] = $parent;

                if ($parent->parent_id) {
                    $this->getParents($parent->parent_id, 'id, slug');
                } else {
                    break;
                }
            } else {
                break;
            }
        }

        return $this->parents;
    }

    public static function getParentById($id, $select=null)
    {
        $page = self::find();

        if (strlen($select) > 0) {
            $page->select($select);
        }

        $page->where('id=:id', [':id'=>$id]);

        return $page->one();
    }

    /**
     * Генерация карты страниц.
     * Используется при разборе и создании URL.
     * @return array ID узла => путь до узла
     */
    protected static function generatePathsMap()
    {
        $nodes = PagesAdmin::find()
            ->select('id, depth, slug')
            ->orderBy('root, lft')
            ->asArray()
            ->all();

        $pathsMap = array();
        $depths = array();
        foreach ($nodes as $node)
        {
            if ($node['depth'] > 0) {
                $path = $depths[$node['depth'] - 1];
            } else {
                $path = '';
            }

            if ($node['slug'] == null) {
                $path .= $node['id'];
            } else {
                $path .= $node['slug'];
            }

            $depths[$node['depth']] = $path . '/';

            $path = ltrim($path, '/');

            $pathsMap[$node['id']] = $path;
        }

        return $pathsMap;
    }

    /**
     * Генерация массива страниц.
     * @return array
     */
    public static function getListPages()
    {
        $nodes = PagesAdmin::find()
            ->select('id, parent_id, title, access, noindex, is_published, number')
            ->asArray()
            ->all();

        $pages = array();

        foreach ($nodes as $node)
        {
            $pages[$node['parent_id']][$node['id']] = [
                'url' => '/'.self::getPathsMap()[$node['id']],
                'title' => $node['title'],
                'access' => ($node['access'] != null)?$node['access']:0,
                'noindex' => ($node['noindex'] != null)?$node['noindex']:0,
                'is_published' => $node['is_published'],
                'number' => $node['number'],
            ];
        }

        return $pages;
    }

    public function getLink()
    {
        return self::getPathsMap()[$this->id];
    }

    public static function getLinkById($id)
    {
        return '/' . self::getPathsMap()[$id];
    }
    
    public function getSlides()
    {
        return $this->hasMany(PageSlide::className(), ['page_id' => 'id'])->orderBy('sort ASC');
    }

    public function beforeDelete()
    {
        PageFile::deleteAll(['page_id' => $this->id]);
        PageSlide::deleteAll(['page_id' => $this->id]);
        
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}
