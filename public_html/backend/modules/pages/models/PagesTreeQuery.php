<?php
namespace backend\modules\pages\models;

use creocoder\nestedsets\NestedSetsQueryBehavior;

class PagesTreeQuery extends \yii\db\ActiveQuery
{
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }
}