<?php
$file = $model->file;
?>
<div class="row" style="margin-bottom: 20px;padding-bottom: 15px;border-bottom: 1px solid #ccc">
    <div class="col-sm-6">
        <div class="form-group">
            <label>Оригинальное название файла:</label>
            <div>
                <?= $file->original_name; ?>
            </div>
        </div>
        <div class="form-group">
            <label>MIME тип файла:</label>
            <div>
                <?= $file->mime_type; ?>
            </div>
        </div>
        <div class="form-group">
            <label>Размер файла:</label>
            <div>
                <?= ($file->size / 1024); ?> Kb
            </div>
        </div>
        <div class="form-group">
            <label>Дата добавления:</label>
            <div>
                <?= date('d.m.Y H:i:s', strtotime($file->date_create)); ?>
            </div>
        </div>
        <div class="form-group">
            <label>Ссылка на скачивание файла:</label>
            <div>
                <?= $file->getUrlToFile(); ?>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-xs-12 file-info">
        <div class="file-form">
            <input type="hidden" class="page-file-form__file_id" name="PageFile[file_id]" value="<?=$model->file_id?>">
            <input type="hidden" class="page-file-form__page_id" name="PageFile[page_id]" value="<?=$model->page_id?>">
            <div class="form-group">
                <label>Название файла</label>
                <input name="File[name]" class="form-control file-form__name" value="<?=$file->name?>">
            </div>
            <div class="form-group">
                <label>Описание файла</label>
                <textarea name="File[description]" class="form-control file-form__description" style="width: 100%;height: 120px"><?=$file->description?></textarea>
            </div>
            <div class="form-group">
                <label>Сортировка</label>
                <input type="text" class="form-control page-file-form__sort" name="PageFile[sort]" value="<?=$model->sort?>">
            </div>
            <button type="button" class="btn btn-sm btn-default btn-file-save">Сохранить</button>
            <button type="button" class="btn btn-danger btn-sm btn-delete-file" data-id="<?=$model->file_id?>">Удалить</button>
        </div>
    </div>
</div>
