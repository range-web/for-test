<?php
use yii\helpers\Url;
use yii\widgets\Pjax;
?>
<div class="row">
    <?php if (!$model->isNewRecord) : ?>
        <div class="col-md-6">
            <div class="form-group">
                <label>Файлы</label>
                <?= rangeweb\filesystem\widgets\fileWidget\FileWidget::widget([
                    'model'=> $pageFile,
                    'attribute'=>'file_id',
                    'required' => true,
                    'multiple' => true,
                    //'url' => '/adverts/default/upload?room_id='.$room->id,
                    'url' => Url::toRoute(['/pages/default/upload-file', 'page_id' => $model->id]),
                    'removeUrl' => Url::toRoute(['/pages/default/delete-file']),
                    'mimeTypes' => 'application/*, image/*',
                    'hideUploadInfo' => true,
                    'htmlOptions' => [
                        'btn-upload-icon' => 'fa fa-folder-open-o',
                        'btn-remove-icon' => 'fa fa-trash',
                    ],
                    'jsCallbackFunctionDone' => "                
                    var parent = $(e.target).parents('.form-group');
                    parent.removeClass('has-error');
                    parent.find('.help-block').hide();         
                                  
                    $.pjax.reload({container:\"#files-list\", timeout: 20000});      
                ",
                    'jsCallbackFunctionAfterDelete' => "
                    $.pjax.reload({container:\"#files-list\", timeout: 20000});
                "
                ]);?>
            </div>
            <style>
                .files-list .image-item {
                    margin-bottom: 30px;
                }
                .files-list .image-gallery-item a {
                    display: block;
                }
                .files-list .image-gallery-item img {
                    width: 100%;
                    height: auto;
                }
            </style>
            <div class="files-list">
                <?php Pjax::begin(['id' => 'files-list','clientOptions'=>['timeout' => 20000]]); ?>
                <?=  \yii\widgets\ListView::widget([
                    'dataProvider' => $filesDataProvider,
                    'itemView' => '_fileItemUpdate',
                    'emptyText' => 'Нет загруженных файлов',
                    'layout' => '{items}{pager}',
                    'itemOptions' => [
                        'tag' => false,
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
        <div class="col-md-6">
            <strong>Код виджета:</strong> <?= htmlspecialchars('{{w:PageFiles|pageId='.$model->id.'}}'); ?>
            <?//= htmlspecialchars('{{w:PageFiles|tempplateWrapper=<div class="page-files">#items#</div>;templateItems=<div><a href="#link#">#title#</a></div>;}}'); ?><br>
            <strong>templateWrapper:</strong> Переменные шаблона #items#<br>
            <strong>templateItems:</strong> Переменные шаблона #link# - ссылка на файл; #title# - название фильтра

        </div>
        <?php

        $this->registerJs("
                $(function() {
                    $(document).on('click', '.btn-delete-file', function() {
                       
                        $.ajax({
                            url: '" . Url::toRoute(['/pages/default/delete-file']) . "',
                            type: 'post',
                            dataType: 'json',
                            data: {id:$(this).data('id')},
                            success: function(data) {
                                $.pjax.reload({container:\"#files-list\", timeout: 20000});
                            }
                        });
                    })
                    .on('click', '.btn-file-save', function(e) {
                        e.preventDefault();
                        
                        var btn = $(this),
                            form = btn.parents('.file-form');
                        
                        $.ajax({
                            url: '/backend/web/index.php?r=pages%2Fdefault%2Fsave-file',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                PageFile: {
                                    file_id: form.find('.page-file-form__file_id').val(),
                                    page_id: form.find('.page-file-form__page_id').val(),
                                    sort: form.find('.page-file-form__sort').val()
                                },
                                File: {
                                    name: form.find('.file-form__name').val(),
                                    description: form.find('.file-form__description').val(),
                                },
                            },
                            beforeSend: function() {
                                btn.attr('disabled', true);
                            },
                            success: function(data) {
                                btn.attr('disabled', false);
                                if (data.status) {
                                    $.pjax.reload({container:\"#files-list\", timeout: 20000});
                                } else {
                                    alert('При сохранении произошла ошибка');
                                }
                            }
                        });
                    })
                });
               
            ");

        ?>
    <?php else : ?>
        <div class="col-md-6">
            Для добавления файлов сначала сохраните страницу
        </div>
    <?php endif; ?>
</div>