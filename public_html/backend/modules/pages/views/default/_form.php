<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model backend\modules\pages\models\PagesAdmin */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="pages-admin-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-12">

        <?php
        $items = [
            [
                'label' => 'Страница',
                'content' => $this->render('_page', ['form'=>$form, 'model'=>$model]),
                'active' => true
            ],
        ];

        if (!$model->isNewRecord) {
            $items[] = [
                'label' => 'Слайдер',
                'content' => $this->render('_slides', [
                    'form'=>$form,
                    'model'=>$model,
                    'slidesDataProvider' => $slidesDataProvider,
                    'pageSlide' => $pageSlide
                ]),
            ];

            $items[] = [
                'label' => 'Файлы',
                'content' => $this->render('_files', [
                    'form'=>$form,
                    'model'=>$model,
                    'filesDataProvider' => $filesDataProvider,
                    'pageFile' => $pageFile
                ]),
            ];
        }

        $items[] = [
            'label' => 'SEO',
            'content' => $this->render('_seo', ['form'=>$form, 'model'=>$model]),
        ];

        $items[] = [
            'label' => 'Стили',
            'content' => $this->render('_styles', ['form'=>$form, 'model'=>$model]),
        ];


            $items[] = [
                'label' => 'Настройка',
                'content' => $this->render('_setting', ['form'=>$form, 'model'=>$model]),
            ];


        ?>

        <?= \yii\bootstrap\Tabs::widget([
            'items' => $items,
        ]);
        ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['name' => 'save','class' => 'btn btn-xs btn-primary']) ?>
        <?= Html::submitButton('Применить' , ['name' => 'apply','class' => 'btn btn-xs btn-success']) ?>
        <?= Html::a('Отмена' , ['/pages'],['class' => 'btn btn-xs btn-warning']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
