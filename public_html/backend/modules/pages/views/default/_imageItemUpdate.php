<div class="row" style="margin-bottom: 20px;padding-bottom: 15px;border-bottom: 1px solid #ccc">
    <div class="col-sm-6 col-xs-12 image-item">
        <a  rel="fancybox" class="image-gallery-item" data-pjax="0" href="<?=$model->getImageUrl([1000,1000])?>">
            <img src="<?=$model->getImageUrl([1000,1000])?>">
        </a>
    </div>

    <div class="col-sm-6 col-xs-12 image-text">
        <div class="slide-form">
            <input type="hidden" class="slide-form__image_id" name="PageSlide[image_id]" value="<?=$model->image_id?>">
            <input type="hidden" class="slide-form__page_id" name="PageSlide[page_id]" value="<?=$model->page_id?>">
            <div class="form-group">
                <label>Текст на слайде</label>
                <textarea name="PageSlide[content]" class="form-control slide-form__content" style="width: 100%;height: 120px"><?=$model->content?></textarea>
            </div>
            <div class="form-group">
                <label>Сортировка</label>
                <input type="text" class="form-control slide-form__sort" name="PageSlide[sort]" value="<?=$model->sort?>">
            </div>
            <button type="button" class="btn btn-sm btn-default btn-slide-save">Сохранить</button>
            <button type="button" class="btn btn-danger btn-sm btn-delete-slide" data-id="<?=$model->image_id?>">Удалить</button>
        </div>
    </div>
</div>
