<?php
use common\models\Settings;
use yii\bootstrap\Html;
use yii\helpers\Url;
?>

<?php if ($model->id != 1) : ?>

<?= $form->field($model, 'parent_id')->dropDownList($model->getPagesList(), array('prompt'=>'')) ?>

<?= $form->field($model, 'title')->textInput(['maxlength' => 255, 'class'=>'field-title form-control']) ?>
<div class="form-group field-pagesadmin-title required has-error">
    <input type="checkbox" id="slug-auto" checked="checked"> <label for="slug-auto">Автозаполнение псевдонима</label>
</div>
<?= $form->field($model, 'slug')->textInput(['maxlength' => 125, 'class'=>'field-slug form-control']) ?>
<!--
<?= $form->field($model, 'css_class')->textInput(['maxlength' => 50, 'class'=>'form-control']) ?>
Список иконок: <a target="_blank" href="http://fortawesome.github.io/Font-Awesome/icons/">FontAwesome</a>
<a target="_blank" href="http://www.webhostinghub.com/glyphs/">WebHostingHub Glyphs</a-->
<?php endif; ?>
<?//= $form->field($model, 'number')->textInput(['maxlength' => 10, 'class'=>'form-control']) ?>
<?= $form->field($model, 'sort')->textInput(['maxlength' => 10, 'class'=>'form-control']) ?>
<?php if (true) : ?>
<div class="row">
    <div class="col-lg-12">
        <?= Settings::getRedactor($form,$model,'content')?>
    </div>
</div>

<?php endif; ?>
<?php if (false) : ?>
    <div class="row">
        <?= Html::a('Настройка контактных данных', Url::to(['/contacts']),['class' => 'btn btn-success btn-xs']) ?>
    </div>
<?php endif; ?>