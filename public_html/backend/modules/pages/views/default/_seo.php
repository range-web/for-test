<?= $form->field($model, 'meta_title')->textInput(['maxlength' => 255]) ?>

<?= $form->field($model, 'meta_description')->textArea(['maxlength' => 255, 'rows' => 3]) ?>

<?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => 255]) ?>

<?//= $form->field($model, 'noindex')->checkbox() ?>