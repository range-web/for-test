<?php if ($model->isNewRecord || $model->id > 2) : ?>
    <?= $form->field($model, 'is_published')->dropDownList($model->getStatuses()) ?>
    <?= $form->field($model, 'access')->dropDownList($model->getAccessList()) ?>
    <?php $themeClass = Yii::$app->params['theme']['class']; ?>
    <?= $form->field($model, 'layout')->dropDownList($themeClass::layouts()) ?>
<?php endif; ?>

<?= $form->field($model, 'text_redactor')->dropDownList(\common\models\Settings::getEditorsStatic()) ?>
<?= $form->field($model, 'is_folder')->checkbox() ?>
