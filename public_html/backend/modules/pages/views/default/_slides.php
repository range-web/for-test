<?php
use yii\helpers\Url;
use yii\widgets\Pjax;
?>
<div class="row">
    <?php if (!$model->isNewRecord) : ?>
        <div class="col-md-6">
            <div class="form-group">
                <label>Слайды</label>
                <?= rangeweb\filesystem\widgets\fileWidget\FileWidget::widget([
                    'model'=> $pageSlide,
                    'attribute'=>'image_id',
                    'required' => true,
                    'multiple' => true,
                    //'url' => '/adverts/default/upload?room_id='.$room->id,
                    'url' => Url::toRoute(['/pages/default/upload', 'page_id' => $model->id]),
                    'removeUrl' => Url::toRoute(['/pages/default/delete-image']),
                    'mimeTypes' => 'image/*',
                    'hideUploadInfo' => true,
                    'htmlOptions' => [
                        'btn-upload-icon' => 'fa fa-folder-open-o',
                        'btn-remove-icon' => 'fa fa-trash',
                    ],
                    'jsCallbackFunctionDone' => "                
                    var parent = $(e.target).parents('.form-group');
                    parent.removeClass('has-error');
                    parent.find('.help-block').hide();         
                                  
                    $.pjax.reload({container:\"#images-list\", timeout: 20000});      
                ",
                    'jsCallbackFunctionAfterDelete' => "
                    console.log(111);
                    $.pjax.reload({container:\"#images-list\", timeout: 20000});
                "
                ]);?>
            </div>
            <style>
                .images-list .image-item {
                    margin-bottom: 30px;
                }
                .images-list .image-gallery-item a {
                    display: block;
                }
                .images-list .image-gallery-item img {
                    width: 100%;
                    height: auto;
                }
            </style>
            <div class="images-list">
                <?php Pjax::begin(['id' => 'images-list','clientOptions'=>['timeout' => 20000]]); ?>
                <?=  \yii\widgets\ListView::widget([
                    'dataProvider' => $slidesDataProvider,
                    'itemView' => '_imageItemUpdate',
                    'emptyText' => 'Нет загруженных слайдов',
                    'layout' => '{items}{pager}',
                    'itemOptions' => [
                        'tag' => false,
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
            <?php
            echo newerton\fancybox\FancyBox::widget([
                'target' => 'a[rel=fancybox]',
                'helpers' => true,
                'mouse' => true,
                'config' => [
                    'maxWidth' => '90%',
                    'maxHeight' => '90%',
                    'playSpeed' => 7000,
                    'padding' => 0,
                    'fitToView' => false,
                    'width' => '70%',
                    'height' => '70%',
                    'autoSize' => false,
                    'closeClick' => false,
                    'openEffect' => 'elastic',
                    'closeEffect' => 'elastic',
                    'prevEffect' => 'elastic',
                    'nextEffect' => 'elastic',
                    'closeBtn' => false,
                    'openOpacity' => true,
                    'helpers' => [
                        'title' => ['type' => 'float'],
                        'buttons' => [],
                        'thumbs' => ['width' => 68, 'height' => 50],
                        'overlay' => [
                            'css' => [
                                'background' => 'rgba(0, 0, 0, 0.8)'
                            ]
                        ]
                    ],
                ]
            ]);
            ?>
        </div>
        <?php

        $this->registerJs("
                $(function() {
                    $(document).on('click', '.btn-delete-slide', function() {
                        $.ajax({
                            url: '" . Url::toRoute(['/pages/default/delete-image']) . "',
                            type: 'post',
                            dataType: 'json',
                            data: {id:$(this).data('id')},
                            success: function(data) {
                                $.pjax.reload({container:\"#images-list\", timeout: 20000});
                            }
                        });
                    })
                    .on('click', '.btn-slide-save', function(e) {
                        e.preventDefault();
                        
                        var btn = $(this),
                            form = btn.parents('.slide-form');
                        
                        $.ajax({
                            url: '/backend/web/index.php?r=pages%2Fdefault%2Fsave-slide',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                PageSlide: {
                                    image_id: form.find('.slide-form__image_id').val(),
                                    page_id: form.find('.slide-form__page_id').val(),
                                    content: form.find('.slide-form__content').val(),
                                    sort: form.find('.slide-form__sort').val()
                                }
                            },
                            beforeSend: function() {
                                btn.attr('disabled', true);
                            },
                            success: function(data) {
                                btn.attr('disabled', false);
                                if (data.status) {
                                    $.pjax.reload({container:\"#images-list\", timeout: 20000});
                                } else {
                                    alert('При сохранении произошла ошибка');
                                }
                            }
                        });
                    })
                });
               
            ");

        ?>
    <?php else : ?>
        <div class="col-md-6">
            Для добавления слайдов сначала сохраните страницу
        </div>
    <?php endif; ?>
</div>