<div class="row">
    <div class="col-lg-12">
        <?php
        echo $form->field($model, 'styles')->widget(
            'trntv\aceeditor\AceEditor',
            [
                'mode'=>'css', // programing language mode. Default "html"
                'theme'=>'github' // editor theme. Default "github"
            ]
        );
        ?>
    </div>
</div>