<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\pages\models\PagesAdmin */

$this->title = 'Новая страница';
$this->params['breadcrumbs'][] = ['label' => 'Статические страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-admin-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
