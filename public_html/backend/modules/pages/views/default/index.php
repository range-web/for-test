<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\pages\models\search\PagesAdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статические страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-admin-index">


    <p>
        <?= Html::a('Добавить страницу', Url::to(['/pages/default/create']),['class' => 'btn btn-success btn-xs', 'data-header'=>'Новая страница']) ?>
    </p>
    <?php Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'attribute' => 'parent_id',
                'filter' => '',
                'value' => function($model){
                    return $model->parentName;
                    //return Html::a($link, $link);
                }
            ],
            // 'meta_description',
            // 'meta_keywords',
            [
                'label' => 'Ссылка',
                'format' => 'html',
                'filter' => '',
                'value' => function($model){

                    $url = \backend\modules\pages\models\PagesAdmin::getPathsMap()[$model->id];

                    if ($url == '/') {
                        $url = '';
                    }

                    return  'Ссылка:' . Yii::$app->params['frontendUrl'].'/'.$url . '<br> Код для меню: [~' . $model->id . ']';
                    //return Html::a($link, $link);
                }
            ],
            [
                'class' => 'backend\components\ToggleDataColumn',
                'attribute' => 'is_published',
                'filter' => '',
                'headerOptions' => ['style'=>'width:105px'],
                'contentOptions' => ['style'=>'text-align:center'],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['style'=>'width:105px'],
                'template' => '{add_page} {update} {delete}',
                'buttons' => [
                    'add_page' => function($url, $model){
                        return Html::a('<i class="fa fa-plus"></i> <i class="fa fa-file-text-o"></i>',['/pages/default/create', 'parent'=>$model->id], ['data-original-title'=>'Добавить дочернюю страницу','class'=>'btn btn-success btn-minier tooltip-success', 'data-rel'=>'tooltip', 'data-header'=>'Новая страница', 'data-pjax'=>0]);
                    },
                    'update' => function($url, $model){
                        return Html::a('<i class="fa fa-edit"></i>',$url, ['data-original-title'=>'Редактировать','class'=>'btn btn-info btn-minier tooltip-info', 'data-rel'=>'tooltip', 'data-header'=>'Редактирование', 'data-pjax'=>0]);
                    },
                    'delete' => function($url, $model){
                        if ($model->id <= 2) {
                            return '';
                        }
                        return Html::a('<i class="fa fa-trash"></i>',$url, ['data-original-title'=>'Удалить','class'=>'btn btn-danger btn-minier tooltip-error', 'data-rel'=>'tooltip', 'data-confirm'=>"Вы уверены, что хотите удалить эту страницу\r\nТакже будут удалены все дочерние страницы", 'data-method'=>'post', 'data-pjax'=>0]);
                    },
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>


