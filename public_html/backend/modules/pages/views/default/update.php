<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\pages\models\PagesAdmin */

$this->title = 'Редактирование: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Статические страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-admin-update">
    <?= $this->render('_form', [
        'model' => $model,
        'slidesDataProvider' => $slidesDataProvider,
        'pageSlide' => $pageSlide,
        'filesDataProvider' => $filesDataProvider,
        'pageFile' => $pageFile
    ]) ?>
</div>
