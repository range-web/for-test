<?php
namespace backend\modules\rbac;

class Module extends \yii\base\Module
{
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'backend\modules\rbac\controllers';
}