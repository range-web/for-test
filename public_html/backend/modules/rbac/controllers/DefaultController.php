<?php
namespace backend\modules\rbac\controllers;

use common\modules\rbac\rules\UserProfileOwnerRule;
use backend\components\AdminController;
use Yii;
use yii\filters\AccessControl;
use common\modules\users\models\User;
use common\modules\rbac\rules\UserGroupRule;

/**
 * Основной контроллер backend-модуля [[RBAC]]
 */
class DefaultController extends AdminController
{
	/**
	 * Создаём иерархию авторизации
	 */
	public function actionCreate()
	{

		$auth = Yii::$app->getAuthManager();

        $auth->removeAll(); //удаляем старые данные

        //Включаем наш обработчик
        $rule = new UserGroupRule();
        $auth->add($rule);

        $userProfileOwnerRule = new UserProfileOwnerRule();
        $auth->add($userProfileOwnerRule);

        //Добавляем роли
        $guest = $auth->createRole('guest');
        $guest->description = 'Гость';
        $guest->ruleName = $rule->name;
        $auth->add($guest);

        $user = $auth->createRole('user');
        $user->description = 'Пользователь';
        $user->ruleName = $rule->name;
        $auth->add($user);

        $moderator = $auth->createRole('moderator');
        $moderator->description = 'Модератор';
        $moderator->ruleName = $rule->name;
        $auth->add($moderator);

        $admin = $auth->createRole('admin');
        $admin->description = 'Администратор';
        $admin->ruleName = $rule->name;
        $auth->add($admin);

        $superAdmin = $auth->createRole('superadmin');
        $superAdmin->description = 'Главный администратор';
        $superAdmin->ruleName = $rule->name;
        $auth->add($superAdmin);

        $index  = $auth->createPermission('index');
        $login  = $auth->createPermission('login');
        $create  = $auth->createPermission('create');
        $view  = $auth->createPermission('view');
        $update  = $auth->createPermission('update');
        $logout  = $auth->createPermission('logout');
        $delete  = $auth->createPermission('delete');
        $ajaxEditPassword  = $auth->createPermission('ajax-edit-password');
        $saveStatusInfo  = $auth->createPermission('save-status-info');
        $move  = $auth->createPermission('move');
        $toggle  = $auth->createPermission('toggle');
        $addVideo  = $auth->createPermission('add-video');
        $imageUpload  = $auth->createPermission('image-upload');
        $error  = $auth->createPermission('error');
        $fileApiUpload  = $auth->createPermission('fileapi-upload');
            
            
            
        $cache  = $auth->createPermission('cache');
        $saveComment  = $auth->createPermission('save-comment');
        $imagesUpdate  = $auth->createPermission('images-update');
        $countModerate  = $auth->createPermission('count-moderate');
        $getProperties  = $auth->createPermission('get-properties');
            
            

        // Фронтенд
        $sendToEmail  = $auth->createPermission('send-to-email');
        $ajaxChangeStatus  = $auth->createPermission('ajax-change-status');

        $updateOwnProfile = $auth->createPermission('updateOwnProfile');
        $updateOwnProfile->ruleName = $userProfileOwnerRule->name;


            
        $auth->add($login);
        $auth->add($index);
        $auth->add($view);
        $auth->add($create);
        $auth->add($update);
        $auth->add($logout);
        $auth->add($delete);
        $auth->add($ajaxEditPassword);
        $auth->add($saveStatusInfo);
        $auth->add($ajaxChangeStatus);
        $auth->add($move);
        $auth->add($toggle);
        $auth->add($addVideo);
        $auth->add($imageUpload);
        $auth->add($error);
        $auth->add($fileApiUpload);
        $auth->add($updateOwnProfile);
        $auth->add($sendToEmail);
            
        $auth->add($cache);
        $auth->add($saveComment);
        $auth->add($imagesUpdate);
        $auth->add($countModerate);
        $auth->add($getProperties);

        //Добавляем потомков
        $auth->addChild($guest, $login);
        $auth->addChild($guest, $error);
        $auth->addChild($user, $guest);
        $auth->addChild($user, $sendToEmail);
        $auth->addChild($user, $logout);
        $auth->addChild($user, $delete);
        $auth->addChild($user, $index);
        $auth->addChild($user, $view);
        $auth->addChild($user, $create);
        $auth->addChild($user, $update);

            $auth->addChild($user, $saveComment);
            $auth->addChild($user, $imagesUpdate);
            $auth->addChild($user, $getProperties);
            
        $auth->addChild($admin, $user);
        $auth->addChild($admin, $ajaxEditPassword);
        $auth->addChild($admin, $saveStatusInfo);
        $auth->addChild($admin, $ajaxChangeStatus);
        $auth->addChild($admin, $move);
        $auth->addChild($admin, $toggle);
        $auth->addChild($admin, $addVideo);
        $auth->addChild($admin, $imageUpload);
        $auth->addChild($admin, $fileApiUpload);

            $auth->addChild($admin, $cache);    
            $auth->addChild($admin, $countModerate);    

        $auth->addChild($superAdmin, $admin);
        $auth->addChild($user, $updateOwnProfile);

		$this->redirect(['/']);
	}
}