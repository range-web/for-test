<?php
namespace backend\modules\users;

/**
 * Backend-модуль [[Users]]
 */
class Module extends \common\modules\users\Module
{
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'backend\modules\users\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}