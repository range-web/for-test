<?php

namespace backend\modules\users\controllers;

use common\models\UserImages;
use common\models\UserSocialLinks;
use Yii;
use common\modules\users\models\User;
use backend\modules\users\models\search\UserSearch;
use backend\components\AdminController;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * DefaultController implements the CRUD actions for User model.
 */
class DefaultController extends AdminController
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {

            if (!\Yii::$app->user->isGuest && !(Yii::$app->user->can('superadmin') || Yii::$app->user->can('admin'))) {
                throw new ForbiddenHttpException('Доступ запрещен');
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();

        if (isset($_REQUEST['status'])) {
            $searchModel->status = $_REQUEST['status'];
        } else {
            $searchModel->status = User::STATUS_ACTIVE;
        }

        if (isset($_REQUEST['fake'])) {
            $searchModel->fake_user = 1;
        }

        if(isset($_REQUEST['role']) && $_REQUEST['role'] == 'admin') {
            $searchModel->role_id = User::ROLE_ADMIN;
        } else if(isset($_REQUEST['role']) && $_REQUEST['role'] == 'moderator') {
            $searchModel->role_id = User::ROLE_MODERATOR;
        } else {
            $searchModel->role_id = User::ROLE_USER;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($role_id=null, $pseudo_user=null)
    {
        $model = new User();
        
        if ($pseudo_user == null) {
            $model->setScenario('adminCreate');
        } else {
            $model->setScenario('pseudoUser');
            $model->is_pseudo = 1;
        }

        if ($model->load(\Yii::$app->request->post())) {
            
            if ($model->save()) {

                Yii::$app->session->setFlash('success', 'Успешно сохранено!');
                if (isset($_POST['apply'])) {
                    return $this->redirect(['update', 'id'=>$model->id]);
                } else {
                    return $this->redirect(['index']);
                }
            }
        }

        if ($role_id != null) {
            $model->role_id = $role_id;
        }

        return $this->render('update',[
            'model'=>$model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!empty(\Yii::$app->request->post()) && $model->load(\Yii::$app->request->post())) {

            $model->newPassword = \Yii::$app->request->post()['User']['newPassword'];

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Успешно сохранено!');
                if (isset($_POST['apply'])) {
                    return $this->redirect(['update', 'id'=>$model->id]);
                } else {
                    return $this->redirect(['index']);
                }
            }
        }
        
        return $this->render('update',[
            'model'=>$model,
        ]);
    }


    public function actionImagesUpdate($id)
    {
        $model = $this->findModel($id);


        

        return $this->render('images-update',[
            'model'=>$model,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAjaxEditPassword(){
        if ( isset($_POST['password']) ) {
            $user = User::findIdentity($_POST['id']);
            $user->password_hash = $user->setPassword($_POST['password']);
            $user->save();
            echo Json::encode([
                'status'=> 1
            ]);
        }
    }
}
