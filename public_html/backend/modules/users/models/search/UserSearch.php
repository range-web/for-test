<?php

namespace backend\modules\users\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\users\models\User;

/**
 * UserSearch represents the model behind the search form about `common\modules\users\models\User`.
 */
class UserSearch extends User
{
    public $roles = [];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'role_id'], 'integer'],
            [['auth_key', 'password_hash', 'email', 'firstname', 'lastname', 'phone', 'date_create', 'date_update','password_reset_token'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!Yii::$app->user->can('superadmin')) {

            $query->andWhere('role_id != ' . User::ROLE_SUPER_ADMIN);
        }
        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if (!empty($this->roles)) {
            $query->andFilterWhere(['in', 'role_id', $this->statuses]);
        } else {
            $query->andFilterWhere(['role_id' => $this->role_id]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            //'role_id' => $this->role_id,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
        ]);

        
        
        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }

}
