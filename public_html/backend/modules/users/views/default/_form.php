<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\users\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-12">

            <?php
            $tabs = [
                [
                    'label' => 'Пользователь',
                    'content' => $this->render('_user', ['form'=>$form, 'model'=>$model]),
                    'active' => true
                ],
            ];

            $tabs[] = [
                'label' => 'Настройка',
                'content' => $this->render('_setting', ['form'=>$form, 'model'=>$model]),
            ];
            ?>
            
            <?= \yii\bootstrap\Tabs::widget([
                'items' => $tabs,
            ]);
            ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['name' => 'save','class' => 'btn btn-xs btn-primary']) ?>
        <?= Html::submitButton('Применить' , ['name' => 'apply','class' => 'btn btn-xs btn-success']) ?>
        <?= Html::a('Отмена' , ['/users'],['class' => 'btn btn-xs btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
