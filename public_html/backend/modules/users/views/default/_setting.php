<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
?>

<?= $form->field($model, 'role_id')->dropDownList($model->getRoles(),['prompt'=>'Выберите роль']) ?>

<?= $form->field($model, 'status')->dropDownList($model->getStatuses()) ?>