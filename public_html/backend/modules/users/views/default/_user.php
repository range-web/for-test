<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\widgets\Alert;
use common\modules\users\models\User;
?>
<style>
    .profile-edit .rw-file-input .pseudo-input {
        display: none;
    }
    .profile-edit .rw-file-input .input-group-btn:last-child > .btn {
        border-top-left-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .profile-edit .input-group-btn {
        text-align: center;
    }
</style>

<div class="profile-edit row">
    <div class="col-sm-12">
        <?= Alert::widget() ?>
    </div>

    <div class="col-sm-6">
        <h3 style="font-size: 18px;">Персональнык данные</h3>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'firstname')->textInput(['maxlength' => 50]); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'lastname')->textInput(['maxlength' => 50]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'gender')->radioList(['M' => 'Мужской', 'F' => 'Женский'],['maxlength' => 50]); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'birthday')->textInput(['class' => 'datepicker-field form-control']); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'phone')->textInput(['maxlength' => 50, 'class' => 'phone-field form-control']); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'email')->textInput(); ?>
            </div>
        </div>
        
        <?php if ($model->isNewRecord) :?>
            <?= $form->field($model, 'password')->textInput() ?>
        <?php else :?>
            <h3 style="font-size: 18px;">Смена пароля</h3>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'newPassword')->passwordInput(); ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'confirmPassword')->passwordInput(); ?>
                </div>
            </div>
        <?php endif;?>

    </div>
</div>

