<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\users\models\User;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\users\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

        <p>
            <?php
            $statusActive = $statusInActive = $roleModerator = $roleAdmin = 'btn-default';

            if ($searchModel->role_id == User::ROLE_USER) {
                if ($searchModel->status == User::STATUS_ACTIVE) {
                    $statusActive = 'btn-primary';
                }
                if ($searchModel->status == User::STATUS_IN_ACTIVE) {
                    $statusInActive = 'btn-primary';
                }
            } else if ($searchModel->role_id == User::ROLE_ADMIN) {

                $roleAdmin = 'btn-primary';
            }

            ?>

            <?= Html::a('Пользователи', ['index', 'status' => User::STATUS_ACTIVE], ['class' => 'btn btn-xs '.$statusActive]) ?>
            <?//= Html::a('Заблокированые пользователи', ['index', 'status' => User::STATUS_IN_ACTIVE], ['class' => 'btn btn-xs '.$statusInActive]) ?>
            <?= Html::a('Администраторы', ['index', 'role' => 'admin'], ['class' => 'btn btn-xs '.$roleAdmin]) ?>

            <div class="btn-group">
                <button data-toggle="dropdown" class="btn btn-success btn-xs dropdown-toggle">
                    Добавить пользователя
                    <span class="ace-icon fa fa-caret-down icon-on-right"></span>
                </button>

                <ul class="dropdown-menu dropdown-success">
                    <li>
                        <a href="/backend/web/index.php?r=users%2Fdefault%2Fcreate&role_id=<?=User::ROLE_USER?>&pseudo_user=1">Пользователь</a>
                    </li>
                    <li>
                        <a href="/backend/web/index.php?r=users%2Fdefault%2Fcreate&role_id=<?=User::ROLE_ADMIN?>">Администратор</a>
                    </li>
                </ul>
            </div>
        </p>





    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'firstname',
            'lastname',
            //'auth_key',
            //'password_hash',
            'email:email',
            // 'status',
            [
                'attribute' => 'role_id',
                'filter' => '',
                /*'filter' => Html::activeDropDownList(
                    $searchModel,
                    'role_id',
                    User::getRoles(),
                    ['class' => 'form-control', 'prompt' => 'Все']
                ),*/
                'value' => function($model){
                    return $model->getRoleTitleByRole($model->role_id);
                    //return Html::a($link, $link);
                }
            ],            // 'avatar',

            // 'city',
            // 'phone',
             //'date_create',
             'date_update',
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['style'=>'width:70px'],
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function($url, $model){
                        return Html::a('<i class="fa fa-edit"></i>',$url, ['data-original-title'=>'Редактировать','class'=>'btn btn-info btn-minier tooltip-info', 'data-rel'=>'tooltip']);
                    },
                    'delete' => function($url, $model){
                        return Html::a('<i class="fa fa-trash"></i>',$url, ['data-original-title'=>'Удалить','class'=>'btn btn-danger btn-minier tooltip-error', 'data-rel'=>'tooltip', 'data-confirm'=>"Вы уверены, что хотите удалить этого пользователя?", 'data-method'=>'post', 'data-pjax'=>0]);
                    },
                ]
            ],
        ],
    ]); ?>

</div>
