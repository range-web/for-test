<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\users\models\User */

$this->title = 'Редактирование пользователя: ' . ' ' . $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование: ' . $model->email;
?>
<div class="user-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
