var declinationWord = function(number, one, two, five) {
    number = Math.abs(number);
    number %= 100;
    if (number >= 5 && number <= 20) {
        return five;
    }
    number %= 10;
    if (number == 1) {
        return one;
    }
    if (number >= 2 && number <= 4) {
        return two;
    }
    return five;
}

$(function() {

    $('.datepicker-field').datepicker({
        format: "dd.mm.yyyy",
        language: 'ru',
    });

    var ajaxLoader = $('#ajax_loader');
    $(document).ajaxStart(function(){
        if (ajaxLoader.length > 0) {
            ajaxLoader.show();
        }
    });

    $(document).ajaxStop(function(){
        if (ajaxLoader.length > 0) {
            ajaxLoader.hide();
        }
    });

    function replaceQueryParam(param, newval, search) {
        var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
        var query = search.replace(regex, "$1").replace(/&$/, '');

        return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
    }

    $(document).on('click', '.nav-tabs li a', function(e) {
        e.preventDefault();
        var tab = $($(this).attr('href')),
            url =  window.location.href;

        if (tab.length > 0) {
            url = replaceQueryParam('tab', tab.data('tab'), window.location.href);
        }

        history.pushState({}, $('title').text(), url);
    });


    $('.per-page-num').change(function() {
        $(this).parents('form').submit();
    });

    $('#sidebar-shortcuts-mini').on('click', function() {
       return false;
    });

    $('[data-rel=tooltip]').tooltip();

    /*---- Создание редактирование в модальных окнах ----*/

    $('.page-content').on('click', '.btn-open-modal', function() {
        var modalBox = $('#modal-new-item');

        modalBox.find('.modal-header h2').text($(this).data('header'));


        modalBox.modal('show')
            .find('#modal-content')
            .load($(this).attr('href'));
        return false;
    });

    /*$(document).on('submit', '.modal-ajax-update', function() {
        var form = $(this);

        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function(html) {
                if (html == 'OK') {
                    $.pjax.reload({container:'#'+form.data('list'), timeout: 10000});
                    $('#modal-new-item').modal('hide');

                    if (form.data('list') == 'comments-list') {
                        notificationModerateComments();
                    }
                }
            }
        })

        return false;
    });*/


    /*function notificationModerateComments()
    {
        $.ajax({
            url: '/backend/web/index.php?r=comments/default/count-moderate',
            type: 'post',
            dataType: 'json',
            success: function(data){
                if (data) {

                    var countComments = 0;
                    var moderateCommentsLI = $('.moderate-comments');

                    $.each(data, function(i, v) {
                        countComments += parseInt(v);
                    });

                    if (countComments > 0) {
                        if (moderateCommentsLI.length == 0) {
                            $('.top-menu').append('<li class="hover moderate-comments pull-right notice-success"><a href="/backend/web/index.php?r=comments&status=2"><i class="menu-icon fa fa-comments"></i><span class="menu-text"> Новые отзывы (<span class="amount">'+countComments+'</span>) </span></a></li>');
                        } else {
                            moderateCommentsLI.find('.amount').text(countComments);
                        }
                    } else if (countComments == 0 && moderateCommentsLI.length > 0) {
                        moderateCommentsLI.remove();
                    }
                }
            }
        });
    }*/

    /*function notificationModerateAdverts()
    {
        $.ajax({
            url: '/backend/web/index.php?r=adverts/default/count-moderate',
            type: 'post',
            dataType: 'json',
            success: function(data){
                if (data) {

                    var countAdverts = data.count;
                    var moderateAdvertsLI = $('.moderate-adverts');

                    if (countAdverts > 0) {
                        if (moderateAdvertsLI.length == 0) {
                            $('.top-menu').append('<li class="hover moderate-adverts pull-right notice-success"><a href="/s/'+data.first_id+'" target="_blank"><i class="menu-icon fa fa-newspaper-o"></i><span class="menu-text"> Новые объявления (<span class="amount">'+countAdverts+'</span>) </span></a></li>');
                        } else {
                            moderateAdvertsLI.html('<a href="/s/'+data.first_id+'" target="_blank"><i class="menu-icon fa fa-newspaper-o"></i><span class="menu-text"> Новые объявления (<span class="amount">'+countAdverts+'</span>) </span></a>');
                        }
                    } else if (countAdverts == 0 && moderateAdvertsLI.length > 0) {
                        moderateAdvertsLI.remove();
                    }
                }
            }
        });
    }*/

    /*notificationModerateAdverts();
    notificationModerateComments();

    setInterval(function() {
        notificationModerateComments();
    }, 1000 * 60 * 5);
    setInterval(function() {
        notificationModerateAdverts();
    }, 1000 * 60 * 2);*/


    /**
     * Автоподстановка поля slug
     * для поля title добавить класс field-title, для поля slug класс field-slug
     */
    $('body').on('keyup', '.field-title', function() {
        if ($("#slug-auto").prop("checked")) {
            $('.field-slug').val(URLify($('.field-title').val()));
        }
    });

    $('body').on('click', "#slug-auto", function() {
        $('.field-slug').val('');

        if ($(this).prop("checked")) {
            $('.field-slug').val(URLify($('.field-title').val()));
        }
    });


    /*------- Кнопка смены публикации в таблицах -------*/

    $('body').on('click', '.btn-toggle-published', function() {

        $.ajax({
            url: $(this).data('url'),
            type: 'get',
            dataType: 'json',
            success: function(data) {
                if (data.error == 0) {

                }
            }
        });
    });

    /*----- Сортировка -------*/

    $('body').on('click', '.btn-sort-item',function() {
        $.ajax({
            url: $(this).attr('href'),
            type: 'get',
            dataType: 'json',
            success: function(data) {
                if (data.error == 0) {
                    $.pjax.reload({container:'#pjax'});
                }
            }
        });
        return false;
    });


    /*--------- Добавление видео ---------*/
    $('#addVideo').on('click', function() {
        if ($('#video-input').val().length > 0) {
            $.ajax({
                url: $(this).data('url'),
                type: 'post',
                dataType: 'json',
                data: {link:$('#video-input').val(), album:$(this).data('album')},
                success: function(data) {
                    if (data.status == 'error') {
                        alert('Неправильный формат ссылки');
                        return false;
                    }
                        location.reload();
                }
            });
        } else {
            alert('Вставьте ссылку на видеоролик');
        }
        return false;
    });

    /*------------Страница контакты----------------------*/
    $('.btn-add-address').on('click', function() {

        var count = 0;

        if ($('.add-address-form .form-group:last-child').length > 0) {
            count = parseInt($('.add-address-form .form-group:last-child').attr('data-id'));
        }

        count++;

        $('.add-address-form').append('<div class="form-group" data-id="'+count+'">' +
        '<input type="text" class="form-control" name="AddContacts[Address]['+count+']" value="" style="width:100%;"><a href="#" class="btn btn-default btn-xs btn-del-add-contact"  style="position:absolute;top:4px;right:4px;"><span class="fa fa-times"></span></a>' +
        '<label><input type="checkbox" name="AddContacts[AddressView]['+count+']" > Показывать на главной странице</label></div>');
        return false;
    });

    $('.btn-add-phone').on('click', function() {

        var count = 0;

        if ($('.add-phone-form .form-group:last-child').length > 0) {
            count = parseInt($('.add-phone-form .form-group:last-child').attr('data-id'));
        }

        count++;

        $('.add-phone-form').append('<div class="form-group" data-id="'+count+'">' +
        '<input type="text" class="form-control" name="AddContacts[Phone]['+count+']" value="" style="width:100%;"><a href="#" class="btn btn-default btn-xs btn-del-add-contact"  style="position:absolute;top:4px;right:4px;"><span class="fa fa-times"></span></a>' +
        '<label><input type="checkbox" name="AddContacts[PhoneView]['+count+']" > Показывать на главной странице</label></div>');
        return false;
    });

    $('.btn-add-email').on('click', function() {

        var count = 0;

        if ($('.add-email-form .form-group:last-child').length > 0) {
            count = parseInt($('.add-email-form .form-group:last-child').attr('data-id'));
        }

        count++;

        $('.add-email-form').append('<div class="form-group" data-id="'+count+'">' +
        '<input type="text" class="form-control" name="AddContacts[Email]['+count+']" value="" style="width:100%;"><a href="#" class="btn btn-default btn-xs btn-del-add-contact"  style="position:absolute;top:4px;right:4px;"><span class="fa fa-times"></span></a>' +
        '<label><input type="checkbox" name="AddContacts[EmailView]['+count+']" > Показывать на главной странице</label></div>');
        return false;
    });

    $(document).on('click', '.btn-del-add-contact', function() {
        $(this).parent().remove();
        return false;
    });
    /*-------------Меняем пароль в профиле---------------*/
    $("#edit_password").on("click", function() {
        $('#modalPassword').modal('toggle');

        $("#alert_edit_password").empty();
        return false;
    });

    $("#passwordOne").keydown(function(event){
        $("#alert_edit_password").empty();
    });

    $("#passwordTwo").keydown(function(event){
        $("#alert_edit_password").empty();
    });


    $("#savePassword").on("click", function(){
        var one = $("#passwordOne").val();
        var two = $("#passwordTwo").val();
        var pasOne_length = one.replace(/\s+/g,'').length;
        var pasTwo_length = two.replace(/\s+/g,'').length;

        if ( pasOne_length < 6 && pasTwo_length < 6 ){
            $("#alert_edit_password").html("Пароль должен быть не менее 6 символов").attr('style','color:red;margin-bottom:15px');

        } else if  ( one == two ){
            $.ajax({
                url: $(this).data('url'),
                data: {password: two, id: $(this).data('user_id')},
            dataType: "json",
            type: "POST",
            success: function(data){
                    if (data.status) {
                        $('#modalPassword').modal('hide');
                        $(".password_message").show(200).delay(10000).hide(200);
                        $("#passwordOne").val('');
                        $("#passwordTwo").val('');
                    }
                }
            })
            } else {
                $("#alert_edit_password").html("Пароли не совпадают").attr('style','color:red;margin-bottom:15px');
                }
            });



    });



    /*-------------Для layout/login--------------*/
    jQuery(function($) {
        $(document).on('click', '.toolbar a[data-target]', function(e) {
            e.preventDefault();
            var target = $(this).data('target');
            $('.widget-box.visible').removeClass('visible');//hide others
            $(target).addClass('visible');//show target
        });
    });

    $(document)
        .on('click', '.add-dropdown-option', function() {
            var newOption = jQuery('.parameter-dropdown-item').last().clone();
            newOption.find('input').each(function(i, element) {
                var el = jQuery(element);
                var val = el.val();
                if (el.hasClass('parameter-dropdown-id') && val.length > 0) {
                    el.val(parseInt(val) + 1);
                } else {
                    el.val('');
                }
            });
            newOption.appendTo(".parameter-dropdown-list");
            newOption.find('.parameter-dropdown-title').focus();
        })
        .on('click', '.delete-dropdown-item', function() {
            var parent = jQuery(this).parent();
            if (jQuery('.delete-dropdown-item').length > 1) {
                if (parent.find('.parameter-dropdown-title').val().length > 0) {
                    if (!confirm('Удалить значение?')) {
                        return false;
                    }
                }
                parent.remove();
            }
        })
        .on("change keyup", '.parameter-dropdown-id', function() {
            var el = jQuery(this),
                val = el.val();
            if (val.match(/[^0-9]/g)) {
                el.val(val.replace(/[^0-9]/g, ''));
                alert('Поле ID должно быть числом');
            }
        })
        .on('focus', '.required', function() {
            jQuery(this).css({'border-color':'transparent'});
        })
        .on('change','#property-type', function(){
            changePropertyType();
        })
        .on('beforeSubmit','#property-form', function () {
            var empty=0;
            $('.parameter-dropdown-title').each(function(i,elem) {
                if ($(this).val().length == 0 && $('#property-type').val() > 2) {
                    empty++;
                }
            });
            if (empty) {
                alert("Поле 'Значение' в 'Наборе значений' должно быть заполненно");
                return false;
            };
        });

        function changePropertyType() {
            var val = jQuery('#property-type').val(),
                setValueBlock = jQuery('.set-value-block');
            if (val > 2) {
                setValueBlock.show();
            } else {
                setValueBlock.hide();
            }
        }

        changePropertyType();

  