$(function(){

    var answersList = $('.answers-list'),
        answersListItems = answersList.find('.answers-list_item'),
        answersListItemsCount = answersListItems.length;

    $(document)
        .on('click', '.btn-add-answer', function(e) {
            e.preventDefault();

            answersListItems = answersList.find('.answers-list_item');
            answersListItemsCount = answersListItems.length;

            var number = answersListItemsCount + 1;

            answersList.append('<div class="answers-list_item form-group">' +
                '<span class="answers-list_item-number">' + number + '</span>' +
                '<input type="text" name="Answers[' + number + ']" class="form-control">' +
                '<a href="#" class="btn btn-default btn-xs btn-del-answer" ><span class="fa fa-times"></span></a>' +
                '</div>');
        })
        .on('click', '.btn-del-answer', function() {
            $(this).parent().remove();

            answersListItems = answersList.find('.answers-list_item');

            answersListItems.each(function(i, v) {
                var el = $(v),
                    number = i + 1;

                el.find('.answers-list_item-number').text(number);
                el.find('.form-control').attr('name', 'Answers[' + number + ']');
            })

            return false;
        });

});