$(function() {
    var emailPattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
    var scheduleModal = $('#schedule-modal');

    $(document)
        .on('change', '#city_id', function() {
            
            location.href = '/backend/web/index.php?r=manicure%2Fschedule&city_id=' + $(this).val();
        })
        .on('click', '.time-item', function(e) {
            e.preventDefault();
            var $this = $(this);

            //dateInfo.html($this.data('weekday') + ' ' + $this.data('date') + ' ' + $this.data('time'));

            $.ajax({
               url: '/backend/web/index.php?r=manicure%2Fschedule%2Fget-schedule-item',
                type: 'post',
                data: {
                    city_id: $this.data('city-id'),
                    schedule_id: $this.data('schedule-id'),
                    date: $this.data('date'),
                    time: $this.data('time')
                },
                success: function(data) {
                    scheduleModal.find('.modal-body').html(data);
                    scheduleModal.modal('show');
                }
            });
            
            
            //$('#schedule-modal').modal('show');
        })
        .on('change', '.schedule-status', function() {
            var $this = $(this),
                clientInfo = $('#schedule-modal .client-info');

            var parent = $this.parents('.form-group');

            if (parent.hasClass('has-error')) {
                parent.removeClass('has-error');
                parent.find('.help-block').hide();
            }

            if ($this.attr('id') == 'schedule-busy') {
                clientInfo.show();
            } else {
                clientInfo.hide();
            }
        })
        .on('change', '.client_id', function() {
            var $this = $(this),
                newClient = $('#schedule-modal .new-client');

            var parent = $this.parents('.form-group');

            if (parent.hasClass('has-error')) {
                parent.removeClass('has-error');
                parent.find('.help-block').hide();
            }

            if ($this.val().length > 0) {
                newClient.hide();

                $.ajax({
                    url: '/backend/web/index.php?r=manicure%2Fschedule%2Fget-client-info',
                    type: 'post',
                    data: {client_id: $this.val()},
                    success: function(data) {
                        $('#schedule-modal .current-client-info').html(data);
                    }
                });
            } else {
                $('#schedule-modal .current-client-info').html('');
                newClient.show();
            }
        })
        .on('submit', '#schedule-item-form', function(e) {
            e.preventDefault();
            var $this = $(this),
                btn = $this.find('button[type=submit]'),
                status = $this.find('.schedule-status:radio:checked'),
                valid = true;



            if (status.length == 0) {
                valid = false;
                addError($('.schedule-status-wrap'), 'Выберите статус');
            } else {
                if (status.val() == 'busy') {
                    var client = $('#client');
                    var newClientFirstname = $this.find('input.firstname');
                    if (client.val().length == 0 && newClientFirstname.val().length == 0) {
                        addError(client, 'Выберите клиента');
                        valid = false;
                    } else {
                        var newClientLastname = $this.find('input.lastname');
                        var newClientPhone = $this.find('input.phone');
                        var newClientEmail = $this.find('input.email');

                        if (newClientFirstname.val().length == 0) {
                            addError(newClientFirstname, 'Введите имя');
                            valid = false;
                        }
                        if (newClientLastname.val().length == 0) {
                            addError(newClientLastname, 'Введите фамилию');
                            valid = false;
                        }
                        if (newClientPhone.val().length == 0) {
                            addError(newClientPhone, 'Введите номер телефона');
                            valid = false;
                        }
                        if (newClientEmail.val().length > 0) {
                            if (!emailPattern.test(newClientEmail.val())) {
                                addError($input, 'Проверьте правильность E-mail');
                                valid = false;
                            }
                        }
                    }
                }
            }

            if (valid) {
                $.ajax({
                   url: $this.attr('action'),
                    type: 'post',
                    dataType: 'json',
                    data: $this.serialize(),
                    beforeSend: function() {
                        btn.attr('disabled', true);
                    },
                    success: function(data) {
                        if (data.status) {
                            $.pjax.reload({container:"#schedule-table-pjax", timeout: 20000});
                            scheduleModal.modal('hide');
                        } else {
                            if (data.errors) {
                                $.each(data.errors, function(i, v) {
                                    var $input = $this.find('.' + i);
                                    addError($input, v[0]);
                                });
                            }
                        }
                    }
                });
            }
        });


    function addError($input, textError)
    {
        var parent = $input.parent();
        parent.addClass('has-error');

        if (textError != undefined) {
            parent.find('.help-block').html(textError).show();
        }
    }
});