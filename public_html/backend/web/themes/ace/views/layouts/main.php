<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use kartik\icons\Icon;
use backend\extensions\widgets\alert\Alert;

Icon::map($this,Icon::FA);
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <!--meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" /-->


    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="no-skin">
<?php $this->beginBody() ?>


<!-- /section:basics/navbar.layout -->
<div class="main-container" id="main-container">


<!-- #section:basics/sidebar.horizontal -->
<div id="sidebar" class="sidebar h-sidebar navbar-collapse collapse">


<div class="sidebar-shortcuts" id="sidebar-shortcuts">
    <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
        <span class="btn btn-success"></span>
        <span class="btn btn-info"></span>
        <span class="btn btn-warning"></span>
        <span class="btn btn-danger"></span>
    </div>
</div><!-- /.sidebar-shortcuts -->

<?php
$menuItems = [];

    $menuItems[1] = [
        'label' => 'Страницы',
        'icon' => 'fa fa-file',
        'url' => ['/pages'],
        'active' => \Yii::$app->controller->module->id == 'pages',
    ];

    /*$menuItems[4] = [
        'label' => 'Контакты',
        'icon' => 'fa fa-briefcase',
        'url' => ['/contacts'],
        'active' => \Yii::$app->controller->module->id == 'contacts',
    ];*/

    $menuItems[5] = [
        'label' => 'Пользователи',
        'icon' => 'fa fa-users',
        'url' => ['/users'],
        'active' => \Yii::$app->controller->module->id == 'users',
    ];

if (Yii::$app->user->can('superadmin') || Yii::$app->user->can('admin')) {


    $menuItems[8] = [
        'label' => 'Система',
        'icon' => 'fa fa-cogs',
        'items'=> [
            [
                'label' => 'Настройки',
                'icon' => 'cogs',
                'url' => ['/settings'],
                'active' =>  [],
            ],
            [
                'label' => 'Кеширование',
                'icon' => 'cogs',
                'url' => ['/site/cache'],
                'active' => [],
            ],
        ],
        'active' =>  (Yii::$app->controller->id == 'settings' || (
                Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'cache')),
    ];

}
    $menuItems [9] =  [
        'label' => 'На сайт',
        'icon' => 'fa fa-globe',
        'url' => '/',
        'active' => [],
    ];
    $menuItems[10] =  [
                        'label' => 'Выход',
                        'icon' => 'fa fa-sign-out',
                        'url' => ['/site/logout'],
                        'options' => ['class'=>'hover pull-right'],
                        'linkOptions' => ['data-method' => 'post'],
                        'active' => [],
                    ];

    $user = Yii::$app->user->identity;

    if ($user) {
        $menuItems[] =  [
            'label' => $user->firstname .' '.$user->lastname,
            'url' => 'javascript:void(0);',
            'icon' => 'fa fa-user',
            'options' => ['class'=>'hover pull-right'],
            'active' => [],
        ];
    }
    echo \backend\assets\AceNav::widget([
        'options' => ['class' => 'nav-list top-menu'],
        'items' => $menuItems,
    ]);
?>
</div>

<!-- /section:basics/sidebar.horizontal -->
<div class="main-content">
    <div class="page-content">
        <!-- /section:settings.box -->
        <div class="page-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <?= Alert::widget() ?>
                <?= $content ?>
                <!-- PAGE CONTENT ENDS -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->
</div>

<div class="footer">
    <div class="footer-inner">
        <!-- #section:basics/footer -->
        <div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">RangeWebCMS</span>
							Все права защищены &copy; 2013-<?= date('Y') ?> &nbsp;&nbsp;&nbsp;  ООО "Рэйнжвеб"&nbsp;&nbsp;&nbsp; +7 (4942) 504-074
						</span>

        </div>

        <!-- /section:basics/footer -->
    </div>
</div>

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->
<div id="ajax_loader">
    <i class="ace-icon fa fa-spinner fa-spin orange bigger-300"></i>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>