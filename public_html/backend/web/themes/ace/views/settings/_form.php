<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Settings;
/* @var $this yii\web\View */
/* @var $model backend\modules\pages\models\PagesAdmin */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-12">

            <label><?= $model->title ?></label>
            <?php if ($model->type == Settings::TYPE_SELECT_EDITOR) {
                echo  $form->field($model, 'value')->dropDownList($model->getEditors())->label(false);
            } else if ($model->type == Settings::TYPE_BOOL) {
                echo  $form->field($model, 'value')->dropDownList([1=>'Да', 2=>'Нет'])->label(false);
            } else if ($model->type == Settings::TYPE_EDITOR) {
                echo $form->field($model, 'content')->widget(\mihaildev\ckeditor\CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],]);
            } else {
                echo $form->field($model, 'value')->textInput()->label(false);
            }?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
