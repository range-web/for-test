<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Settings;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\pages\models\search\PagesAdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-admin-index">

    <?php Modal::begin([
        'header' => '<h2></h2>',
        'id' =>'modal-new-item',
        'size' => 'modal-lg'
    ]);?>
    <div id="modal-content"></div>
    <?php Modal::end(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'module_name',
                'value' => function ($model) {
                    return $model->module_name;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'module_name',
                    Settings::getModulesList(),
                    ['class' => 'form-control', 'prompt' => 'Все модули']
                )
            ],
            [
                'attribute' => 'section',
                'value' => function ($model) {
                    return $model->section;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'section',
                    Settings::getSectionsList(),
                    ['class' => 'form-control', 'prompt' => 'Все настройки']
                )
            ],
            [
                'attribute' => 'name',
                'value' => function ($model) {
                    return $model->name;
                },
                'filter' => ''
            ],
            [
                'attribute' => 'title',
                'value' => function ($model) {
                    return $model->title;
                },
                'filter' => ''
            ],
            [
                'attribute' => 'value',
                'value' => function ($model) {
                    return $model->value;
                },
                'filter' => ''
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'headerOptions' => ['style'=>'width:20px'],
                'buttons' => [
                    'update' => function($url, $model){
                        return Html::a('<i class="fa fa-edit"></i>',$url, ['data-original-title'=>'Редактировать','class'=>'btn btn-info btn-minier tooltip-info btn-open-modal', 'data-header'=>'Редактировать', 'data-rel'=>'tooltip']);
                    },
                ]
            ],
        ],
    ]); ?>
</div>
