<?php
/* @var $this yii\web\View */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
$this->title = 'Кеширование';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .cache-items label {
        display: block;
    }
</style>

<div class="site-index">

        <div class="col-lg-6">
            <h3>Очистить кеш</h3>
            <?= Html::beginForm(['cache'],'get') ?>
            <div class="form-group">
                <?= Html::radioList('clear_cache', 'all', ['all'=>'Весь кеш', 'assets' => 'JS и CSS скрипты'], ['class'=>'cache-items']) ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Очистить',['class'=>'btn btn-sm btn-success'])?>
            </div>
            <?= Html::endForm() ?>
        </div>

</div>
