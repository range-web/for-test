<?php
/* @var $this yii\web\View */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
$this->title = 'Панель управления';
?>


<div class="site-index">
    <div class="row">
        <div class="col-lg-6">
                <h3 class="header smaller lighter blue"><i class="menu-icon fa fa-dashboard"></i> Панель управления</h3>
            <?php if (Yii::$app->getModule('pages') != null) {
                echo Html::a('<i class="menu-icon fa fa-file-text-o"></i> Добавить страницу', ['/pages/default/create'], ['class'=>'btn btn-xs btn-success']);
            }?>
            <?php if (Yii::$app->getModule('news') != null) {
                echo Html::a('<i class="menu-icon fa fa-newspaper-o"></i> Добавить новость', ['/news/default/create'], ['class'=>'btn btn-xs btn-info']);
            }?>
            <?php if (Yii::$app->getModule('gallery') != null) {
                echo Html::a('<i class="menu-icon fa fa-image"></i> Добавить фотоальбом', ['/gallery/albums/create'], ['class'=>'btn btn-xs btn-yellow']);
            }?>

        </div>
        <div class="col-lg-6">
            <h3 class="header smaller lighter red"><i class="menu-icon fa fa-bell"></i> События</h3>

            <h3 class="header smaller lighter green"><i class="menu-icon fa fa-users"></i> Пользователи</h3>
        </div>
    </div>
</div>
