<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<div class="site-login">

    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($model, 'email')->textInput(['placeholder'=>' Email'])->label(false) ?>
                <?= $form->field($model, 'password')->passwordInput(['placeholder'=>' Пароль'])->label(false) ?>

                <div class="clearfix">
                    <label class="inline">
                        <?= $form->field($model, 'rememberMe')->checkbox() ?>
                    </label>
                    <?= Html::submitButton('Вход', ['class' => 'width-35 pull-right btn btn-sm btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
