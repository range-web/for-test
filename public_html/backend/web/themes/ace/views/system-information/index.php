<?php
/**
 * Author: Eugine Terentev <eugine@terentev.net>
 * @var $this \yii\web\View
 */

use backend\components\SI;
$this->title = 'Информация о системе';
\backend\assets\SystemInfoAsset::register($this)
?>
<div id="system-information-index">
<div class="row connectedSortable">

    <div class="col-lg-6">
        <div class="widget-box">
            <div class="widget-header">
                <i class="fa fa-hdd-o"></i>
                <h5 class="widget-title">Системная информация о сервере</h5>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <dl class="dl-horizontal">

                        <dt>Имя сервера</dt>
                        <dd><?= SI::getHostname() ?></dd>

                        <dt>Внутренний IP</dt>
                        <dd><?= SI::getServerIP() ?></dd>

                        <dt>Внешний IP</dt>
                        <dd><?= SI::getExternalIP() ?></dd>

                        <dt>Порт</dt>
                        <dd><?= $_SERVER['REMOTE_PORT'] ?></dd>

                        <dt>Процессор</dt>
                        <dd><?= SI::getCpuinfo('model name') ?></dd>

                        <dt>Архитектура</dt>
                        <dd><?= SI::getArchitecture() ?></dd>

                        <dt>Кол-во ядер</dt>
                        <dd><?= SI::getCpuCores() ?></dd>

                        <dt>ОЗУ Всего</dt>
                        <dd><?= Yii::$app->formatter->asSize(SI::getTotalMem()) ?></dd>

                        <dt>ОЗУ Свободно</dt>
                        <dd><?= Yii::$app->formatter->asSize(SI::getFreeMem()) ?></dd>

                        <dt>Swap Всего</dt>
                        <dd><?= Yii::$app->formatter->asSize(SI::getTotalSwap()) ?></dd>

                        <dt>Swap Свободно</dt>
                        <dd><?= Yii::$app->formatter->asSize(SI::getFreeSwap()) ?></dd>

                        <dt>Нагрузка</dt>
                        <dd><?= SI::getLoadAverage(5) ?></dd>

                        <dt>Uptime</dt>
                        <dd><?= SI::getUptime() ?></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="widget-box">
            <div class="widget-header">
                <i class="fa fa-hdd-o"></i>
                <h5 class="widget-title">Программное обеспечение</h5>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <dl class="dl-horizontal">
                        <dt>ОС</dt>
                        <dd><?= SI::getOS() ?></dd>

                        <?php if(!SI::getIsWindows()): ?>
                            <dt>Версия</dt>
                            <dd><?= SI::getLinuxOSRelease() ?></dd>

                            <dt>Версия ядра</dt>
                            <dd><?= SI::getLinuxKernelVersion() ?></dd>
                        <?php endif; ?>
                        <dt>Системная дата</dt>
                        <dd><?= Yii::$app->formatter->asDate(time()) ?></dd>

                        <dt>Системное время</dt>
                        <dd><?= Yii::$app->formatter->asTime(time()) ?></dd>

                        <dt>Временная зона</dt>
                        <dd><?= date_default_timezone_get() ?></dd>

                        <dt>Веб-сервер</dt>
                        <dd><?= SI::getServerSoftware() ?></dd>

                        <dt>Версия PHP</dt>
                        <dd><?= SI::getPhpVersion() ?></dd>

                        <dt>База данных</dt>
                        <dd><?=  Yii::$app->db->dsn ?></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div id="cpu-usage" class="widget-box">
            <div class="widget-header">
                <i class="fa fa-area-chart"></i>
                <h5 class="widget-title">Использование CPU</h5>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="chart" style="height: 300px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div id="memory-usage" class="widget-box">
            <div class="widget-header">
                <i class="fa fa-area-chart"></i>
                <h5 class="widget-title">Использование памяти</h5>
            </div>
            <div class="widget-body">
                <div class="widget-main">
                    <div class="chart" style="height: 300px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>