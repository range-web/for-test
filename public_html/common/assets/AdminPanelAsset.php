<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 8:16 PM
 */
namespace common\assets;
use yii\web\AssetBundle;
class AdminPanelAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/themes/ace/assets/';
    public $css = [
        'css/font-awesome.min.css',
    ];
    public $js = [

    ];
} 