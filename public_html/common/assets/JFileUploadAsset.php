<?php
namespace common\assets;

use yii\web\AssetBundle;

/**
 * JFileUploadAsset
 *
 * @author Yurij Borschev <yborschev@gmail.com>
 * @link http://www.rangeweb.ru/
 * @package rangeweb\jfileupload
 */
class JFileUploadAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/themes/ace/assets/js/jquery-file-upload';

    public $css = [
        'css/jquery.fileupload.css'
    ];

    public $js = [
        'js/vendor/jquery.ui.widget.js',
        'js/jquery.iframe-transport.js',
        'js/jquery.fileupload.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}