<?php

namespace common\components;

use common\models\Settings;
use yii\swiftmailer\Mailer;

class CMailer extends Mailer
{
    public function init()
    {
        $this->setTransport([
            'class' => 'Swift_SmtpTransport',
            'host' => Settings::getCacheValue('smtpHost'),
            'username' => Settings::getCacheValue('smtpEmail'),
            'password' => Settings::getCacheValue('smtpPass'),
            'port' => Settings::getCacheValue('smtpPort'),
            'encryption' => 'tls',
        ]);

        parent::init();
    }
}