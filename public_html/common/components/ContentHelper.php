<?php
namespace common\components;

use backend\modules\gallery\models\BaseAlbums;
use backend\modules\news\models\BaseNews;
use backend\modules\pages\models\BasePages;
use Yii;
use yii\helpers\Html;

class ContentHelper
{
    public $html;

    public function generateContentArray($map=false)
    {
        $this->html = '<ul>';
        //$this->html .= '<li>'.Html::a('Главная', '/', ['data-access'=>BaseNews::ACCESS_ALL, 'data-ispublished'=>1, 'data-noindex'=>0]);
        if (Yii::$app->getModule('pages') != null) {
            $pages = BasePages::getListPages();

                $this->html .= '<li class=""> Статические страницы';
                $this->html .= '<ul>';
                $this->tree($pages, 0);
                $this->html .= '</ul>';

        }

        if (Yii::$app->getModule('news') != null) {
            $news = BaseNews::getListNews();
            $html = '';
            foreach ($news['categories'] as $k=>$row) {
                $html .= '<li>'.Html::a($row['title'], $row['url'], ['data-access'=>1, 'data-ispublished'=>$row['is_published'], 'data-noindex'=>0]);
                if (!empty($news['news'][$k])) {
                    $html .= '<ul>';
                    foreach ($news['news'][$k] as $new) {
                        $html .= '<li>'.Html::a($new['title'], $new['url'], ['data-access'=>$new['access'], 'data-ispublished'=>$new['is_published'], 'data-noindex'=>$new['noindex']]);
                    }
                    $html .= '</ul>';
                    unset($news['news'][$k]);
                }
            }

            foreach ($news['news'] as $new) {
                foreach ($new as $n) {
                    $html .= '<li class="">'.Html::a($n['title'], $n['url'], ['data-access'=>$n['access'], 'data-ispublished'=>$n['is_published'], 'data-noindex'=>$n['noindex']]);
                }
            }

            $this->html .= '<li>'.Html::a('Новости', '/news', ['data-access'=>BaseNews::ACCESS_ALL, 'data-ispublished'=>1, 'data-noindex'=>0]);
            $this->html .= '<ul>';
            $this->html .= $html;
            $this->html .= '</ul>';
        }

        if (Yii::$app->getModule('gallery') != null) {
            $albums = BaseAlbums::getListAlbums();

            $this->html .= '<li>'.Html::a('Галерея', '/gallery', ['data-access'=>BaseNews::ACCESS_ALL, 'data-ispublished'=>1, 'data-noindex'=>0]);
            $this->html .= '<ul>';
            foreach ($albums as $album) {
                $this->html .= '<li class="">'.Html::a($album['title'], $album['url'], ['data-access'=>$album['access'], 'data-ispublished'=>$album['is_published'], 'data-noindex'=>$album['noindex']]);
            }
            $this->html .= '</ul>';
        }
        if (Yii::$app->getModule('reviews') != null) {
            $this->html .= '<li>'.Html::a('Отзывы', '/reviews', ['data-access'=>BaseNews::ACCESS_ALL, 'data-ispublished'=>1, 'data-noindex'=>0]);
        }


        //$this->html .= '<li>'.Html::a('Контакты', '/contacts', ['data-access'=>BaseNews::ACCESS_ALL, 'data-ispublished'=>1, 'data-noindex'=>0]);
        if (!$map) {
            $this->html .= '<li>'.Html::a('Регистрация/Авторизация', 'login', ['data-access'=>BaseNews::ACCESS_ALL, 'data-ispublished'=>1, 'data-noindex'=>0]);
        }
        $this->html .= '</ul>';
        return $this->html;
    }

    public function generateXmlMap()
    {
        $this->html = '<?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"> ';

        $this->html .= '<url>
            <loc>'.Yii::$app->params['frontendUrl'].'</loc></url>';
        if (Yii::$app->getModule('pages') != null) {
            $pages = BasePages::getListPages();
            $this->treeXml($pages, 0);
        }

        if (Yii::$app->getModule('news') != null) {
            $news = BaseNews::getListNews();
            $this->html .= '<url><loc>'.Yii::$app->params['frontendUrl'].'/news'.'</loc></url>';
            foreach ($news['categories'] as $k=>$row) {
                $this->html .= '<url><loc>'.Yii::$app->params['frontendUrl'].$row['url'].'</loc></url>';
                if (!empty($news['news'][$k])) {

                    foreach ($news['news'][$k] as $new) {
                        $this->html .= '<url><loc>'.Yii::$app->params['frontendUrl'].$new['url'].'</loc></url>';
                    }

                    unset($news['news'][$k]);
                }
            }

            foreach ($news['news'] as $new) {
                foreach ($new as $n) {
                    $this->html .= '<url><loc>'.Yii::$app->params['frontendUrl'].$n['url'].'</loc></url>';
                }
            }
        }

        if (Yii::$app->getModule('gallery') != null) {
            $albums = BaseAlbums::getListAlbums();

            $this->html .= '<url><loc>'.Yii::$app->params['frontendUrl'].'/gallery'.'</loc></url>';
            foreach ($albums as $album) {
                $this->html .= '<url><loc>'.Yii::$app->params['frontendUrl'].$album['url'].'</loc></url>';
            }
        }
        if (Yii::$app->getModule('reviews') != null) {
            $this->html .= '<url><loc>'.Yii::$app->params['frontendUrl'].'/reviews'.'</loc></url>';
        }


        $this->html .= '<url><loc>'.Yii::$app->params['frontendUrl'].'/contacts'.'</loc></url>';

        $this->html .= '</urlset>';
        return $this->html;
    }

    private function showTree($tree, $pid=0)
    {
        foreach ($tree as $id=>$root) {
            if($id!=$pid)continue;
            if(count($root)) {
                foreach($root as $key => $val)
                {
                    if(count($tree[$key])) {
                        $this->html .= '<li>'.Html::a($val['title'], $val['url'], ['data-access'=>$val['access'], 'data-pageid' => $key, 'data-ispublished'=>$val['is_published'], 'data-noindex'=>$val['noindex'], 'data-number'=>$val['number']]);
                        $this->html .= '<ul>';
                        self::ShowTree($tree,$key);
                        $this->html .= '</ul>';
                    } else {
                        $this->html .= '<li>'.Html::a($val['title'], $val['url'], ['data-access'=>$val['access'], 'data-pageid' => $key, 'data-ispublished'=>$val['is_published'], 'data-noindex'=>$val['noindex'], 'data-number'=>$val['number']]);
                    }
                }
            }
        }
    }

    public function tree($tree, $pid=0) {
        $this->showTree($tree, $pid);
        return $this->html;
    }


    public function treeXml($tree, $pid=0) {
        $this->showTreeXml($tree, $pid);
        return $this->html;
    }

    private function showTreeXml($tree, $pid=0)
    {
        foreach ($tree as $id=>$root) {
            if($id!=$pid)continue;
            if(count($root)) {
                foreach($root as $key => $val)
                {
                    if(count($tree[$key])) {
                        $this->html .= '<url><loc>'.Yii::$app->params['frontendUrl'].$val['url'].'</loc></url>';
                        self::ShowTreeXml($tree,$key);
                    } else {
                        $this->html .= '<url><loc>'.Yii::$app->params['frontendUrl'].$val['url'].'</loc></url>';
                    }
                }
            }
        }
    }

    /**
     * @return ContentHelper
     */
    public static function model()
    {
        return new self;
    }
}