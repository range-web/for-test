<?php
namespace common\components;

/**
 * Facebook provider class.
 *
 * @package application.extensions.eauth.services
 */
class CustomFacebookOAuth2Service extends \nodge\eauth\services\FacebookOAuth2Service
{

    protected function fetchAttributes()
    {
        $info = $this->makeSignedRequest('me', [
            'query' => [
                'fields' => join(',', [
                    'id',
                    'name',
                    'link',
                    'first_name',
                    'last_name',
                    'gender',
                    'birthday',
                    'email',
                ])
            ]
        ]);

        $this->attributes['id'] = $info['id'];
        $this->attributes['name'] = $info['name'];
        $this->attributes['url'] = $info['link'];
        $this->attributes['first_name'] = $info['first_name'];
        $this->attributes['last_name'] = $info['last_name'];
        $this->attributes['gender'] = $info['gender'];
        $this->attributes['birthday'] = $info['birthday'];
        $this->attributes['email'] = $info['email'];

        return true;
    }
}