<?php

namespace common\components;

class DateHelper
{
    public static function getMonthsArray($t=true)
    {
        if ($t) {
            return array(
                '01'=>'Января',
                '02'=>'Февраля',
                '03'=>'Марта',
                '04'=>'Апреля',
                '05'=>'Мая',
                '06'=>'Июня',
                '07'=>'Июля',
                '08'=>'Августа',
                '09'=>'Сентября',
                '10'=>'Октября',
                '11'=>'Ноября',
                '12'=>'Декабря',
            );
        } else {
            return array(
                '01'=>'Январь',
                '02'=>'Февраль',
                '03'=>'Март',
                '04'=>'Апрель',
                '05'=>'Май',
                '06'=>'Июнь',
                '07'=>'Июль',
                '08'=>'Август',
                '09'=>'Сентябрь',
                '10'=>'Октябрь',
                '11'=>'Ноябрь',
                '12'=>'Декабрь',
            );
        }
    }


    public static function getMonthRus($month, $short=true)
    {
        $months = self::getMonthsArray($short);
        return $months[$month];
    }
    public static function getDays()
    {
        return array(
            1 => 'Понедельник',
            2 => 'Вторник',
            3 => 'Среда',
            4 => 'Четверг',
            5 => 'Пятница',
            6 => 'Суббота',
            7 => 'Воскресенье',
        );
    }
    public static function getTitleDay($id)
    {
        $days = self::getDays();
        return $days[$id];
    }

    public static function formatDateToMonthYear($date, $t)
    {
        $strtotime = strtotime($date);
        return self::getMonthRus(date('m', $strtotime), $t).' '.date('Y', $strtotime);
    }

    public static function getRangeYears($start,$end=null)
    {
        $years = array();
        if ($end == null) {
            $end = date('Y');
        }
        foreach (range($start, $end) as $year) {
            $years[$year] = $year;
        }
        return $years;
    }

    public static function setFormatDate($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public static function getFormatDate($date)
    {
        if ($date == '0000-00-00' || $date == null) {
            return null;
        }
        return date('d.m.Y', strtotime($date));
    }

    public static function getFormatDateStringMonth($date)
    {
        $strtotime = strtotime($date);
        return date('d', $strtotime) .' '.self::getMonthRus(date('m', $strtotime)) .' '. date('Y', $strtotime) .' г.';
    }

    public static function setFormatDateTime($datetime=null)
    {
        if ($datetime == null) {
            return date('Y-m-d H:i:s', time());
        }
        return date('Y-m-d H:i:s', strtotime($datetime));
    }

    public static function getFormatDateTime($datetime=null)
    {
        if ($datetime == null) {
            return date('d.m.Y H:i', time());
        }
        return date('d.m.Y H:i', strtotime($datetime));
    }

    public static function getMonthDate($date)
    {
        return self::getMonthRus(date('m', strtotime($date)), false);
    }

    public static function getDatesArrayFromPeriod($startTime, $endTime) {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        //$numDays = round(($endTime - $startTime) / $day) + 1;
        $numDays = round(($endTime - $startTime) / $day); // без +1

        $days = array();
        $days[] = date($format, $startTime);
        for ($i = 1; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }
        $days[] =  date($format, $endTime);;
        return $days;
    }
}