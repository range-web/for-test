<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'icon-framework' => 'fa',
    'uploadPath' => '@frontend/web/uploads',
    'uploadUrl' => '/uploads',
    'theme' => [
        'class' => 'frontend\themes\basic\Theme',
        'layoutsPath' => '@frontend/themes/basic/views/layouts'
    ],
    
    'frontendUrl' => $_SERVER["REQUEST_SCHEME"].'://'.$_SERVER['HTTP_HOST']
];
