<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$url = Url::toRoute(['site/activation', 'email' => $model->email, 'key' => $model->auth_key],true);

?>
<div class="password-reset">
    <p>Здравствуйте <?= Html::encode($model->email) ?>,</p>

    <p>Спасибо за регистрацию.  <?= Html::a('Активировать вашу учетную запись',$url) ?></p>
</div>
