<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$url = Url::toRoute(['site/activation', 'email' => $model->email, 'key' => $model->auth_key],true);
?>
    Здравствуйте <?= $model->email ?>

    Спасибо за регистрацию.  Активировать вашу учетную запись:

<?= $url ?>