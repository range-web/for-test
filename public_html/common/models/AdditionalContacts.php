<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_additional_contacts".
 *
 * @property integer $id
 * @property integer $type
 * @property string $value
 * @property string $value2
 * @property integer $sort
 * @property integer $view_home
 */
class AdditionalContacts extends \yii\db\ActiveRecord
{
    const TYPE_EMAIL = 1;
    const TYPE_PHONE = 2;
    const TYPE_ADDRESS = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_additional_contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'sort', 'view_home'], 'integer'],
            [['value', 'value2'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'value' => 'Value',
            'value2' => 'Value2',
            'sort' => 'Sort',
            'view_home' => 'View Home',
        ];
    }
}
