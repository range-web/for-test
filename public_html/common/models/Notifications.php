<?php

namespace common\models;

use common\components\DateHelper;
use Yii;

/**
 * This is the model class for table "{{%notifications}}".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $advert_id
 * @property integer $user_id
 * @property integer $is_read
 * @property string $date_create
 */
class Notifications extends \yii\db\ActiveRecord
{
    const TYPE_ADVERT_PUBLISHED = 100;
    const TYPE_ADVERT_REJECTED = 101;
    const TYPE_ADVERT_MODERATE = 102;
    const TYPE_ADVERT_DELETED = 103;
    const TYPE_ADVERT_NEW_COMMENT = 104;
    const TYPE_ADVERT_NEW_RESPONSE = 105;

    public static function typesList()
    {
        return [
            self::TYPE_ADVERT_PUBLISHED => 'Объявление опубликовано',
            self::TYPE_ADVERT_REJECTED => 'Объявление отклонено',
            self::TYPE_ADVERT_MODERATE => 'Объявление снято с публикации',
            self::TYPE_ADVERT_DELETED => 'Объявление удалено',
            self::TYPE_ADVERT_NEW_COMMENT => 'Новый отзыв к объявлению',
            self::TYPE_ADVERT_NEW_RESPONSE => 'Новый отклик на объявление',
        ];
    }
    
    public static function getTypeTitle($type)
    {
        return self::typesList()[$type];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notifications}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'advert_id', 'user_id', 'is_read'], 'integer'],
            [['date_create'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'advert_id' => 'Advert ID',
            'user_id' => 'User ID',
            'is_read' => 'Is Read',
            'date_create' => 'Date Create',
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->date_create = DateHelper::setFormatDateTime();
            $this->is_read = 0;
        }
        
        return parent::beforeSave($insert);
    }

    public static function setNotice($type, $advert_id, $user_id)
    {
        $notice = new self;
        $notice->type = $type;
        $notice->advert_id = $advert_id;
        $notice->user_id = $user_id;
        $notice->save();
    }
    
    public static function setRead($advert_id)
    {
        Notifications::updateAll(['is_read' => 1], 'advert_id = :advert_id AND is_read = 0', ['advert_id' => $advert_id]);
    }

    public static function getNotice()
    {
        $notices = Notifications::find()
            ->select('type, count(*) as cnt')
            ->where('user_id = :user_id AND is_read = 0', ['user_id' => Yii::$app->user->id])
            ->groupBy('type')
            ->asArray()
            ->all();
        
        return $notices;
    }
}
