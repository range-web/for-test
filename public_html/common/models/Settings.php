<?php

namespace common\models;

use dosamigos\ckeditor\CKEditor;
use Yii;
use vova07\imperavi\Widget;
/**
 * This is the model class for table "tbl_settings".
 *
 * @property integer $id
 * @property string $module
 * @property string $module_name
 * @property string $section
 * @property string $name
 * @property integer $type
 * @property string $value
 * @property string $title
 */
class Settings extends \yii\db\ActiveRecord
{
    const TYPE_TEXT = 1;
    const TYPE_BOOL = 2;
    const TYPE_EDITOR = 3;
    const TYPE_CHECKBOX = 4;
    const TYPE_SELECT_EDITOR = 5;
    const TYPE_EMAIL = 6;
    const TYPE_COORDS = 7;
    const TYPE_IMAGE = 8;

    const EDITOR_NONE = 0;
    const EDITOR_ACE = 1;
    const EDITOR_IMPERAVI = 2;
    const EDITOR_CKEditor = 3;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_settings';
    }

    public function getEditors()
    {
        return self::getEditorsStatic();
    }

    public static function getEditorsStatic()
    {
        return [
            self::EDITOR_NONE => 'Нет',
            self::EDITOR_ACE => 'Ace',
            self::EDITOR_IMPERAVI => 'Imperavi',
            self::EDITOR_CKEditor => 'CKEditor',
        ];
    }

    public static function getRedactor($form,$model, $attribute)
    {
        $textEditor = self::getCacheValue('textEditor');

        if (property_exists($model, 'textEditor')) {
            $textEditor = $model->text_redactor;
        }

        if ($textEditor == self::EDITOR_ACE) {

            return $form->field($model, $attribute)->widget(
                'trntv\aceeditor\AceEditor',
                [
                    'mode'=>'html', // programing language mode. Default "html"
                    'theme'=>'github' // editor theme. Default "github"
                ]
            );
        } else if ($textEditor == self::EDITOR_IMPERAVI) {
            return $form->field($model, $attribute)->widget(Widget::className(), [
                'settings' => [
                    'lang' => 'ru',
                    'fileUpload'=> \yii\helpers\Url::to(['/site/file-upload']),
                    'imageUpload' => \yii\helpers\Url::to(['/site/image-upload']),
                    //'imageManagerJson' => \yii\helpers\Url::to(['/site/images-get']),
                    //'fileManagerJson' => \yii\helpers\Url::to(['/site/files-get']),
                    'plugins' => [
                        //'clips',
                        'fontsize',
                        'fontcolor',
                        'imagemanager',
                        'filemanager',
                        'table',
                        'video',
                        'fullscreen',

                    ],
                    'minHeight'       => 400,
                    'paragraphize' => false,
                    'replaceDivs' => false,
                    'cleanStyleOnEnter' => true,
                    'pastePlainText'  => false,
                    'autoresize'  => false,
                    'linebreaks' => true,
                ],

            ]);
        } else if ($textEditor == self::EDITOR_CKEditor) {
            return $form->field($model, $attribute)->widget(CKEditor::className(), [
                'options' => ['rows' => 10],
                'preset' => 'full',
                'clientOptions' => [
                    'filebrowserUploadUrl' => '/backend/web/index.php?r=site/cke-image-upload'
                ]
            ]);
        } else {
            return $form->field($model, $attribute)->textArea(['rows' => 8]);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['value', 'title'], 'string'],
            [['module', 'name'], 'string', 'max' => 50],
            [['section'], 'string', 'max' => 125]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module' => 'Модуль',
            'section' => 'Настройки',
            'name' => 'Идентификатор',
            'type' => 'Type',
            'value' => 'Значение',
            'title' => 'Наименование',
            'module_name' => 'Модуль',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->cache->set($this->module.$this->name, $this->value);

        parent::afterSave($insert, $changedAttributes);
    }

    public static function getSectionsList()
    {
        $sections = self::find()
            ->select('section')
            ->groupBy('section')
            ->all();

        $sectionsList = array();
        foreach ($sections as $section) {
            $sectionsList[$section['section']] = $section['section'];
        }
        return $sectionsList;
    }

    public static function getModulesList()
    {
        $modules = self::find()
            ->select('module_name')
            ->groupBy('module_name')
            ->all();

        $modulesList = array();
        foreach ($modules as $module) {
            $modulesList[$module['module_name']] = $module['module_name'];
        }
        return $modulesList;
    }

    public static function getCacheValue($name, $module='app')
    {
        $value = Yii::$app->cache->get($module.$name);
        if($value===false || $value===null) {
            $setting = Settings::find()
                ->select('value')
                ->where('module=:module AND name=:name', [':module'=>$module,':name'=>$name])
                ->one();

            if (!$setting) {
                return false;
            }

            $value = $setting->value;

            Yii::$app->cache->set($module.$name, $value);
        }
        return $value;
    }

    public static function getActValue($name, $module='app')
    {
        $setting = Settings::find()
            ->select('value')
            ->where('module=:module AND name=:name', [':module'=>$module,':name'=>$name])
            ->one();

        if ($setting) {
            return $setting->value;
        }

        return '';
    }

    public function getValue()
    {
        $value = '';
        if ($this->type == 'bool') {
            if ($this->value == 0) {
                $value = 'Нет';
            } else if ($this->value == 1) {
                $value = 'Да';
            }
        } else {
            $value = substr($this->value, 0, 70);
        }
        return $value;
    }
}
