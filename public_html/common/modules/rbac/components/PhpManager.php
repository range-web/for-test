<?php
namespace common\modules\rbac\components;

use Yii;
use yii\rbac\Role;

/**
 * Файловый компонент модуля [[RBAC]]
 */
class PhpManager extends \yii\rbac\PhpManager
{
	/**
	 * @inheritdoc
	 */
	public $authFile = '@common/modules/users/modules/rbac/data/rbac.php';
    public $assignmentFile = '@common/modules/users/modules/rbac/data/assignments.php';
    public $ruleFile = '@common/modules/users/modules/rbac/data/rules.php';

}