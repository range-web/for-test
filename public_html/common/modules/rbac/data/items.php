<?php
return [
    'guest' => [
        'type' => 1,
        'description' => 'Гость',
        'ruleName' => 'userGroup',
        'children' => [
            'login',
            'error',
        ],
    ],
    'user' => [
        'type' => 1,
        'description' => 'Пользователь',
        'ruleName' => 'userGroup',
        'children' => [
            'guest',
            'send-to-email',
            'logout',
            'delete',
            'index',
            'view',
            'create',
            'update',
            'save-comment',
            'images-update',
            'get-properties',
            'updateOwnProfile',
        ],
    ],
    'moderator' => [
        'type' => 1,
        'description' => 'Модератор',
        'ruleName' => 'userGroup',
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Администратор',
        'ruleName' => 'userGroup',
        'children' => [
            'user',
            'ajax-edit-password',
            'save-status-info',
            'ajax-change-status',
            'move',
            'toggle',
            'add-video',
            'image-upload',
            'fileapi-upload',
            'cache',
            'count-moderate',
            'get-schedule-item',
            'save-schedule-item',
            'get-client-info',
            'upload',
            'delete-image',
            'cke-image-upload',
            'save-slide',
            'upload-file',
            'delete-file',
            'save-file',
        ],
    ],
    'superadmin' => [
        'type' => 1,
        'description' => 'Главный администратор',
        'ruleName' => 'userGroup',
        'children' => [
            'admin',
        ],
    ],
    'login' => [
        'type' => 2,
    ],
    'index' => [
        'type' => 2,
    ],
    'view' => [
        'type' => 2,
    ],
    'create' => [
        'type' => 2,
    ],
    'update' => [
        'type' => 2,
    ],
    'upload' => [
        'type' => 2,
    ],
    'upload-file' => [
        'type' => 2,
    ],
    'save-file' => [
        'type' => 2,
    ],
    'delete-image' => [
        'type' => 2,
    ],
    'delete-file' => [
        'type' => 2,
    ],
    'save-slide' => [
        'type' => 2,
    ],
    'logout' => [
        'type' => 2,
    ],
    'delete' => [
        'type' => 2,
    ],
    'ajax-edit-password' => [
        'type' => 2,
    ],
    'save-status-info' => [
        'type' => 2,
    ],
    'ajax-change-status' => [
        'type' => 2,
    ],
    'move' => [
        'type' => 2,
    ],
    'toggle' => [
        'type' => 2,
    ],
    'add-video' => [
        'type' => 2,
    ],
    'cke-image-upload' => [
        'type' => 2,
    ],
    'image-upload' => [
        'type' => 2,
    ],
    'error' => [
        'type' => 2,
    ],
    'fileapi-upload' => [
        'type' => 2,
    ],
    'updateOwnProfile' => [
        'type' => 2,
        'ruleName' => 'isProfileOwner',
    ],
    'send-to-email' => [
        'type' => 2,
    ],
    'cache' => [
        'type' => 2,
    ],
    'save-comment' => [
        'type' => 2,
    ],
    'images-update' => [
        'type' => 2,
    ],
    'count-moderate' => [
        'type' => 2,
    ],
    'get-schedule-item' => [
        'type' => 2,
    ],
    'save-schedule-item' => [
        'type' => 2,
    ],
    'get-client-info' => [
        'type' => 2,
    ],
    'get-properties' => [
        'type' => 2,
    ],
];
