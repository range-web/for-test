<?php
namespace common\modules\rbac\rules;
use Yii;
use yii\rbac\Rule;
use common\modules\users\models\User;
use yii\helpers\ArrayHelper;

class UserGroupRule extends Rule
{
    public $name = 'userGroup';

    public function execute($user, $item, $params)
    {
        if (!\Yii::$app->user->isGuest) {

            $role = Yii::$app->user->identity->role_id;
            if ($item->name === 'superadmin') {
                return $role == User::ROLE_SUPER_ADMIN;
            }
            elseif ($item->name === 'admin') {
                return $role == User::ROLE_ADMIN || $role == User::ROLE_SUPER_ADMIN;
            }
            elseif ($item->name === 'user') {
                return $role == User::ROLE_ADMIN || $role == User::ROLE_SUPER_ADMIN || $role == User::ROLE_USER;
            }
        }
        return 'guest' == $item->name;
    }
}