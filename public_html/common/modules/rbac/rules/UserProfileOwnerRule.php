<?php
namespace common\modules\rbac\rules;

use yii\rbac\Rule;

class UserProfileOwnerRule extends Rule
{
    public $name = 'isProfileOwner';

    public function execute($user, $item, $params)
    {
        if (\Yii::$app->user->can('admin') || \Yii::$app->user->can('superadmin') || \Yii::$app->user->can('moderator') ) {
            return true;
        }
        return isset($params['profileId']) ? \Yii::$app->user->id == $params['profileId'] : false;
    }
}