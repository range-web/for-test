<?php
namespace common\modules\users;


class Module extends \yii\base\Module
{
    public $avatarWidth = 185;
    public $avatarHeight = 185;

    public $imageSizes = [
        'mini' => [50,50],
        'small' => [185,185],
        'large' => [500,500]
    ];

    public $avatarCropMaxSize = [555,555];
    public $avatarCropMinSize = [100,100];

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}