<?php

namespace common\modules\users\models;

use Yii;

/**
 * This is the model class for table "tbl_eauth".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $auth_id
 * @property string $auth_key
 * @property string $username
 * @property string $service
 */
class Eauth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_eauth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['auth_id'], 'string', 'max' => 255],
            [['auth_key', 'username', 'service'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'auth_id' => 'Auth ID',
            'auth_key' => 'Auth Key',
            'username' => 'Username',
            'service' => 'Service',
        ];
    }
}
