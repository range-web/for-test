<?php
namespace common\modules\users\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class FinishRegisterForm extends Model
{
    public $email;
    public $first_name;
    public $last_name;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'first_name'], 'required'],
            ['email', 'email'],
            [['last_name'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'first_name' => 'Имя',
            'last_name' =>'Фамилия',
        ];
    }
    
    public function register()
    {
        if ($this->validate()) {
            $user = User::find()->where(['email'=>$this->email])->one();
       
            if ($user != null) {
                $this->addError('email', 'Данный email занят');
                return null;
            } else {
                $user = new User(['scenario'=>'finishRegister']);
            }

            $user->email = $this->email;
            $user->firstname = $this->first_name;
            $user->lastname = $this->last_name;
            $user->status = User::STATUS_ACTIVE;
            $user->role_id = User::ROLE_USER;

            // Генерируем случайный пароль
            $user->password = md5(time());

            $user->generateAuthKey();
            
            if ($user->save()) {
                return $user;
            }
        }

        return false;
    }
}
