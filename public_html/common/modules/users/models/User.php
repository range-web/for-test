<?php
namespace common\modules\users\models;

use common\models\Cities;
use common\models\Settings;
use common\models\UserSocialLinks;
use rangeweb\filesystem\models\File;
use Yii;
use yii\base\ErrorException;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\rbac\Permission;
use yii\web\IdentityInterface;
use yii\base\Security;
use yii\db\Expression;
use common\components\DateHelper;
use yii\base\ModelEvent;
use yii\helpers\Url;
use common\modules\users\models\Eauth;

/**
 * User model
 *
 * @property integer $id
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property datetime $date_create
 * @property datetime $date_update
 * @property string $password write-only password
 * @property integer $role_id
 * @property string $firstname
 * @property string $lastname
 * @property string $phone
 * @property integer $city_id
 * @property integer $is_pseudo
 * @property string $gender
 * @property string $birthday
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const STATUS_IN_ACTIVE = 5;

    const ROLE_SUPER_ADMIN = 1;
    const ROLE_ADMIN = 2;
    const ROLE_USER = 3;

    public $password;
    public $image;

    public $profile;

    public $oldPassword;
    public $newPassword;
    public $confirmPassword;
    public $activeTab;

    public $minLengthPassword = 6;

    const EVENT_NEW_USER = 'newUser';

    public $_email;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            if ($this->date_create == null) {
                $this->date_create = date('Y-m-d H:i:s');
            } else {
                $this->date_create = DateHelper::setFormatDateTime($this->date_create);
            }

        } else {
            if ($this->date_create != null) {
                $this->date_create = DateHelper::setFormatDateTime($this->date_create);
            }
        }


        if ($this->date_update != null) {
            $this->date_update = DateHelper::setFormatDateTime($this->date_update);
        }

        if ($this->birthday != null) {
            $this->birthday = DateHelper::setFormatDate($this->birthday);
        }

        // Запрет на изменение email
        if (!$this->isNewRecord && $this->_email != $this->email) {
            $this->email = $this->_email;
        }
        
        $this->phone = preg_replace('/[^0-9]/', '', $this->phone);

        $this->date_update = date('Y-m-d H:i:s');

        return parent::beforeSave($insert);
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'email'],'required','on'=>['userCreate','singUp']],
            [['password', 'email'],'required','on'=>['adminCreate','singUp']],
            [['firstname', 'email', 'role_id'], 'required', 'on' => 'finishRegister'],

            [['firstname', 'lastname', 'phone', 'email', 'city_id'], 'required', 'on' => 'userUpdate'],
            
            [['firstname', 'lastname', 'phone', 'city_id'], 'required', 'on' => 'pseudoUser'],

            [['role_id'],'required','on'=>'userCreate'],
            [['role_id'],'required','on'=>'adminCreate'],

            [['role_id'],'default','value'=>User::ROLE_USER,'on'=>'singUp'],
            ['email','email'],
            ['email', 'unique', 'message' => 'Данный email занят'],
            
            [['newPassword'], 'string', 'min' => $this->minLengthPassword, 'tooShort' => 'Пароль слишком короткий, минимум '.$this->minLengthPassword.' зн.', 'on' => 'userUpdate'],
            [['firstname'],'required','on'=>'userUpdate'],

            ['confirmPassword', 'compare', 'compareAttribute' => 'newPassword', 'message' => 'Пароли не совпадают', 'skipOnEmpty' => false],

            ['status', 'default', 'value' => self::STATUS_ACTIVE,'on'=>'userCreate'],
            ['status', 'default', 'value' => self::getStatusBySettings(),'on'=>'singUp'],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED , self::STATUS_IN_ACTIVE]],

            [['email', 'auth_key', 'password_hash',
                'firstname', 'lastname',  'phone','password_reset_token'], 'string'],
            [['date_create', 'date_update','about', 'birthday', 'gender'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'password' => 'Пароль',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'phone' => 'Телефон',
            'email' => 'Email',
            'city_id' => 'Город',
            'date_create' => 'Дата регистрации',
            'date_update' => 'Дата последнего редактирования',
            'role_id' => 'Роль',
            'oldPassword' => 'Старый пароль',
            'newPassword' => 'Новый пароль',
            'confirmPassword' => 'Подтверждение пароля',
            'about' => 'Обо мне',
            'gender' => 'Пол',
            'birthday' => 'Дата рождения',
        ];
    }

    public function beforeValidate()
    {
        if (
            $this->newPassword != null &&
            $this->confirmPassword != null &&
            strlen($this->newPassword) >= $this->minLengthPassword &&
            strlen($this->confirmPassword) >= $this->minLengthPassword
        ) {
            $this->password = $this->newPassword;
            $this->password_hash = $this->setPassword($this->password);
        }

        if($this->isNewRecord) {
            $this->password_hash = $this->setPassword($this->password);
        }
        return parent::beforeValidate();
    }

//    public function afterSave($insert,$changedAttribute)
//    {
//        $event = new ModelEvent;
//        $this->trigger(self::EVENT_NEW_USER, $event);
//
//        return parent::afterSave($insert,$changedAttribute);
//    }

    public function afterFind()
    {
        if ($this->date_create != null) {
            $this->date_create = DateHelper::getFormatDateTime($this->date_create);
        }
        if ($this->date_update != null) {
            $this->date_update = DateHelper::getFormatDateTime($this->date_update);
        }
        if ($this->birthday != null) {
            $this->birthday = DateHelper::getFormatDate($this->birthday);
        }

        $this->_email = $this->email;

        return parent::afterFind();
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        if (Yii::$app->getSession()->has('user-'.$id)) {
            return new self(Yii::$app->getSession()->get('user-'.$id));
        }
        else {
            return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
            //return isset(self::$users[$id]) ? new self(self::$users[$id]) : null;
        }
    }

    /*public static function findByEAuth($service) {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName().'-'.$service->getId();
        $attributes = array(
            'id' => $id,
            'auth_key' => md5($id),
            'profile' => $service->getAttributes(),
        );
        $attributes['profile']['service'] = $service->getServiceName();
        $userService = Eauth::find()
            ->where(['auth_key'=>$attributes['auth_key'], 'service'=>$attributes['profile']['service']])
            ->one();

        if ($userService != null) {
            $user = self::findOne($userService->user_id);
            Yii::$app->user->login($user, 0);
            return false;
        }

        Yii::$app->session->set('eauth', $attributes);

        return true;
    }*/

    public static function findByEAuth($service) {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName().'-'.$service->getId();
        $attributes = [
            'id' => $id,
            'auth_key' => md5($id),
            'profile' => $service->getAttributes(),
        ];
        $attributes['profile']['service'] = $service->getServiceName();

        $userService = Eauth::find()
            ->where(['auth_key'=>$attributes['auth_key'], 'service'=>$attributes['profile']['service']])
            ->one();

        if ($userService != null) {
            $user = self::findOne($userService->user_id);
            Yii::$app->user->login($user, 0);
            return false;
        }

        Yii::$app->session->set('eauth', $attributes);
        return new self($attributes);
    }

    /**
     * Привязка для зарегистрированных пользователей
     * @param $service
     * @return bool|User
     * @throws ErrorException
     */
    public static function findByEAuthAuthUsers($service) {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName().'-'.$service->getId();
        $attributes = [
            'id' => $id,
            'auth_key' => md5($id),
            'profile' => $service->getAttributes(),
        ];
        $attributes['profile']['service'] = $service->getServiceName();

        $userService = Eauth::find()
            ->where(['auth_key'=>$attributes['auth_key'], 'service'=>$attributes['profile']['service']])
            ->one();

        if ($userService != null) {
            if ($userService->user_id != Yii::$app->user->id) {
                throw new ErrorException('Данный аккаунт привязан к другому пользователю');
            }
            return false;
        }

        $userService = new Eauth();
        $userService->username = $attributes['username'];
        $userService->user_id = Yii::$app->user->id;
        $userService->service = $attributes['profile']['service'];
        $userService->auth_key = $attributes['auth_key'];
        $userService->auth_id = $attributes['id'];

        if ($userService->save()){
            $socialLink = UserSocialLinks::find()
                ->where('social_id = :social_id AND user_id = :user_id', [
                    'social_id' => $userService->service,
                    'user_id' => Yii::$app->user->id
                ])
                ->one();

            if ($socialLink == null || strlen($socialLink->link) == 0) {
                $socialLinks = new UserSocialLinks();
                $socialLinks->social_id = $userService->service;
                $socialLinks->user_id = Yii::$app->user->id;
                $socialLinks->link = $attributes['profile']['url'];
                $socialLinks->save();
            }
        }

        return true;
    }


    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }



    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        return $this->password_hash = Yii::$app->security->generatePasswordHash($password,5);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        return $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DELETED => 'Удален',
        ];
    }

    public static function getRoles(){
        $roleArray = [
            self::ROLE_USER => 'Пользователь',
            self::ROLE_ADMIN => 'Администратор',
        ];
        if (Yii::$app->user->can('superadmin')) {
            $roleArray [self::ROLE_SUPER_ADMIN] = 'Главный администратор';
        }
        return $roleArray;
    }

    public function getRoleTitleByRole($role){
        return $this->getRoles()[$role];
    }

    public function getFio()
    {
        $fio = '';

        if ($this->firstname != null) {
            $fio .= $this->firstname;
        }

        if ($this->lastname != null) {
            $fio = (strlen($fio) > 0) ? $fio.' '.$this->lastname:$this->lastname;
        }

        return $fio;
    }


    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


    public function onNewUser($event)
    {
        $model = $event->sender;

        return Yii::$app->mail->compose(['html' => 'userActivate-html', 'text' => 'userActivate-text'], ['model' => $model])
            ->setTo($model->email)
            ->setFrom([Settings::getCacheValue('smtpEmail') => Settings::getCacheValue('smtpName')])
            ->setSubject('Активация пользователя на сайте: ' . Settings::getCacheValue('siteName'))
            ->send();

    }

    public static function getStatusBySettings()
    {
        if (Settings::getCacheValue('confirmAuthByEmail', 'Users') == 1) {
            return self::STATUS_IN_ACTIVE;
        } else {
            return self::STATUS_ACTIVE;
        }
    }

    /**
     * @param bool $formated
     * @param bool $arrayKeyId
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getSocialLinks($formated=false, $arrayKeyId=false)
    {
        if ($this->isNewRecord) {
            return [];
        }
        
        $res = UserSocialLinks::find()
            ->where('user_id=:user_id', ['user_id'=>$this->id])
            ->all();

        $arrLinks = [];

        foreach ($res as $link) {
            if (strlen($link->link) > 0) {
                $arrLinks[] = $link;
            }
        }

        if ($formated && !empty($arrLinks)) {
            $arFormatedRes = [];

            $arSocialParams = UserSocialLinks::getSocialArray();

            foreach ($arrLinks as $link) {
                if (array_key_exists($link->social_id, $arSocialParams)) {
                    $arSocialParams[$link->social_id]['link'] = $link->link;
                    
                    if ($arrayKeyId) {
                        $arrKey = $link->social_id;
                    } else {
                        $arrKey = $arSocialParams[$link->social_id]['sort'];
                    }

                    $arFormatedRes[$arrKey] = $arSocialParams[$link->social_id];
                }
            }

            ksort($arFormatedRes);
            
            return $arFormatedRes;
        }

        return $res;
    }

    public function getAvatarImageUrl($arSize, $imageQuality=80)
    {
        if ($this->image_id != null) {
            $file = File::getResizeImage($this->image_id, $arSize, $imageQuality);

            $url = $file['url'];
        } else {
            $url = '/images/no-avatar.png';
        }
        
        return $url;
    }

    
    public function isOwner()
    {
        return $this->id == Yii::$app->user->id;
    }

    public static function canAdminEdit()
    {
        if (!\Yii::$app->user->isGuest && (Yii::$app->user->can('superadmin') || Yii::$app->user->can('admin'))) {
            return true;
        }
        return false;
    }

    public static function getUsers($cityId = null)
    {
        $condition = [
            'role_id' => self::ROLE_USER
        ];
        
        if ($cityId) {
            $condition['city_id'] = $cityId;
        }
 
        $res = self::find()
            ->where($condition)
            ->orderBy('lastname ASC, firstname ASC')
            ->all();
        
        $users = [];
        
        foreach ($res as $user) {
            $users[$user->id] = $user->lastname . ' ' . $user->firstname . ', тел:' . $user->phone;
        }

        return $users;
    }

    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }
}
