<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\components\rbac\UserGroupRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // Создаем роли
        $guest  = $auth->createRole('guest');
        $admin  = $auth->createRole('admin');
        $superadmin = $auth->createRole('superadmin');
        $user  = $auth->createRole('user');

        //Создаем разрешения
        $login  = $auth->createPermission('login');
        $logout = $auth->createPermission('logout');
        $error  = $auth->createPermission('error');
        $signUp = $auth->createPermission('sign-up');
        $index  = $auth->createPermission('index');
        $view   = $auth->createPermission('view');
        $update = $auth->createPermission('update');
        $delete = $auth->createPermission('delete');

        //Добавляем разрешения
        $auth->add($login);
        $auth->add($logout);
        $auth->add($error);
        $auth->add($signUp);
        $auth->add($index);
        $auth->add($view);
        $auth->add($update);
        $auth->add($delete);

        //Добавляем группы ползователей
        $userGroupRule = new UserGroupRule();
        $auth->add($userGroupRule);


        $guest->ruleName  = $userGroupRule->name;
        $admin->ruleName  = $userGroupRule->name;
        $superadmin->ruleName = $userGroupRule->name;
        $user->ruleName  = $userGroupRule->name;

        // Добавляем роли
        $auth->add($guest);
        $auth->add($admin);
        $auth->add($superadmin);
        $auth->add($user);


        // Guest
        $auth->addChild($guest, $login);
        $auth->addChild($guest, $logout);
        $auth->addChild($guest, $error);
        $auth->addChild($guest, $signUp);
        $auth->addChild($guest, $index);
        $auth->addChild($guest, $view);

        // USER
        $auth->addChild($user, $update);
        $auth->addChild($user, $guest);

        // ADMIN
        $auth->addChild($admin, $delete);
        $auth->addChild($admin, $user);

        // SUPERADMIN
        $auth->addChild($superadmin, $admin);


    }
}