<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\Settings;
class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%settings}}', [
            'id' => 'pk',
            'module' => 'varchar(50)',
            'section' => 'varchar(125)',
            'name' => 'varchar(50)',
            'type' => 'int(4)',
            'value' => 'text',
            'title' => 'varchar(125)',
            'module_name' => 'varchar(125)',
        ]);

        $this->insert('{{%settings}}', array(
            'module' => 'app',
            'module_name' => 'CMS',
            'type' => Settings::TYPE_TEXT,
            'section' => 'Настройки SMTP',
            'name' => 'smtpHost',
            'value' => 'smtp.beget.ru',
            'title' => 'SMTP Сервер',
        ));
        $this->insert('{{%settings}}', array(
            'module' => 'app',
            'module_name' => 'CMS',
            'type' => Settings::TYPE_TEXT,
            'section' => 'Настройки SMTP',
            'name' => 'smtpPort',
            'value' => 2525,
            'title' => 'SMTP Порт',
        ));
        $this->insert('{{%settings}}', array(
            'module' => 'app',
            'module_name' => 'CMS',
            'type' => Settings::TYPE_TEXT,
            'section' => 'Настройки SMTP',
            'name' => 'smtpEmail',
            'value' => '',
            'title' => 'SMTP адрес эл.почты',
        ));
        $this->insert('{{%settings}}', array(
            'module' => 'app',
            'module_name' => 'CMS',
            'type' => Settings::TYPE_TEXT,
            'section' => 'Настройки SMTP',
            'name' => 'smtpPass',
            'value' => '',
            'title' => 'SMTP Пароль',
        ));
        $this->insert('{{%settings}}', array(
            'module' => 'app',
            'module_name' => 'CMS',
            'type' => Settings::TYPE_TEXT,
            'section' => 'Настройки SMTP',
            'name' => 'smtpName',
            'value' => '',
            'title' => 'SMTP Имя отправителя',
        ));

        $this->insert('{{%settings}}', array(
            'module' => 'CMS',
            'module_name' => 'CMS',
            'type' => Settings::TYPE_SELECT_EDITOR,
            'section' => 'Текстовый редактор',
            'name' => 'textEditor',
            'value' => '1',
            'title' => 'Текстовый редактор',
        ));

        $this->insert('{{%settings}}', array(
            'module' => 'contatcs',
            'module_name' => 'Контакты',
            'type' => Settings::TYPE_EDITOR,
            'section' => 'Контакты',
            'name' => 'contactAddress',
            'value' => '',
            'title' => 'Адрес',
        ));

        $this->insert('{{%settings}}', array(
            'module' => 'contatcs',
            'module_name' => 'Контакты',
            'type' => Settings::TYPE_EDITOR,
            'section' => 'Контакты',
            'name' => 'contactAddress',
            'value' => '',
            'title' => 'Адрес',
        ));


        $this->createTable('{{%users}}', [
            'id' => 'pk',
            'firstname' => 'varchar(50)',
            'lastname' => 'varchar(50)',
            'email' => 'varchar(100)',
            'phone' => 'varchar(20)',
            'gender' => 'tinyint(1)',
            'birthday' => 'date',
            'image_id' => 'int(11)',
            'city_id' => 'int(11)',
            'date_create' => 'datetime',
            'date_update' => 'datetime',
            'role_id' => 'tinyint(1)',
            'auth_key' => 'varchar(255)',
            'password_hash' => 'varchar(255)',
            'password_reset_token' => 'varchar(255)',
            'status' => 'tinyint(1)',
        ]);

        $this->insert('{{%users}}', [
            'password_hash' => '$2y$05$mIHSl9Fyh1aH/rlrMoHJ2.NTemNYBop.RzZ2CYENHU8LaBP6hyV0W',
            'email' => 'admin@rangeweb.ru',
            'status' => 10,
            'role_id' => 1,
            'date_create' => date('Y-m-d H:i:s'),
            'date_update' => date('Y-m-d H:i:s')
        ]);

        $this->createTable('{{%pages}}', [
            'id' => 'pk',
            'root' => 'int(10)',
            'lft' => 'int(10) NOT NULL',
            'rgt' => 'int(10) NOT NULL',
            'depth' => 'int(10) NOT NULL',
            'parent_id' => 'int(10) NOT NULL',
            'slug' => 'varchar(125)',
            'meta_description' => 'varchar(255)',
            'meta_keywords' => 'varchar(255)',
            'title' => 'varchar(255) NOT NULL',
            'content' => 'text NOT NULL',
            'date_create' => 'datetime',
            'date_update' => 'datetime',
            'is_published' => 'tinyint(1)',
            'meta_title' => 'varchar(255)',
            'noindex' => 'tinyint(1)',
            'access' => 'int(2)',
            'css_class' => 'varchar(50)',
            'number' => 'int(10)',
            'text_redactor' => 'int(2)',
            'is_folder' => 'tinyint(1)',
            'sort' => 'int(10)',
            'layout' => 'varchar(20)',
        ]);

        $this->createTable('{{%page_slides}}', [
            'page_id' => 'int(11)',
            'image_id' => 'int(11)',
            'content' => 'varchar(255)',
            'sort' => 'int(11)',
            'PRIMARY KEY (page_id, image_id)'
        ]);

        $this->createTable('{{%page_files}}', [
            'page_id' => 'int(11)',
            'file_id' => 'int(11)',
            'sort' => 'int(11)',
            'PRIMARY KEY (page_id, file_id)'
        ]);

        $this->createTable('{{%additional_contacts}}', [
            'id' => 'pk',
            'type' => 'int(2)',
            'value' => 'varchar(255)',
            'value2' => 'varchar(255)',
            'sort' => 'int(10)',
            'view_home' => 'tinyint(1)',
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%users}}');
        $this->dropTable('{{%pages}}');
        $this->dropTable('{{%settings}}');
        $this->dropTable('{{%page_slides}}');
        $this->dropTable('{{%page_files}}');
        $this->dropTable('{{%additional_contacts}}');
    }
}
