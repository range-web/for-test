
DROP TABLE IF EXISTS `blablarent`.`tbl_users`;
DROP TABLE IF EXISTS `blablarent`.`tbl_adverts`;
DROP TABLE IF EXISTS `blablarent`.`tbl_rooms`;
DROP TABLE IF EXISTS `blablarent`.`tbl_countries`;
DROP TABLE IF EXISTS `blablarent`.`tbl_regions`;
DROP TABLE IF EXISTS `blablarent`.`tbl_cities`;
DROP TABLE IF EXISTS `blablarent`.`tbl_streets`;
DROP TABLE IF EXISTS `blablarent`.`tbl_room_users`;
DROP TABLE IF EXISTS `blablarent`.`tbl_bookmarks`;
DROP TABLE IF EXISTS `blablarent`.`tbl_responses`;
DROP TABLE IF EXISTS `blablarent`.`tbl_booking_dates`;
DROP TABLE IF EXISTS `blablarent`.`tbl_room_images`;
DROP TABLE IF EXISTS `blablarent`.`tbl_properties`;
DROP TABLE IF EXISTS `blablarent`.`tbl_property_values`;


