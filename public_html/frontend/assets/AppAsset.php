<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    /*public $basePath = '@webroot';*/
    public $sourcePath = '@frontend/themes/basic/assets/';

    public $baseUrl = '@web';
    public $css = [
        //'css/bootstrap-datepicker.min.css',
        'js/owl-carousel2/assets/owl.carousel.css',
        'css/main.css',
    ];
    public $js = [
        'js/owl-carousel2/owl.carousel.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'frontend\assets\FontAwesomeAsset',
        //'nodge\eauth\assets\WidgetAssetBundle',
        //'kartik\select2\Select2Asset',
        //'kartik\select2\ThemeKrajeeAsset'
    ];
}
