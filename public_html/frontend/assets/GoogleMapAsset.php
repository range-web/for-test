<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class GoogleMapAsset extends AssetBundle
{
    public $baseUrl = '@web';

    public $js = [
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyBg6aXyA_kd-tYeJU8TvZctbPUHBdmrhnc&callback=initMap',
    ];
    public $depends = [];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
