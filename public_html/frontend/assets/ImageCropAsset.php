<?php
namespace frontend\assets;

class ImageCropAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@frontend/themes/basic/assets/';
    public $baseUrl = '@web';
    
    public $css = [
        'js/jcrop/jcrop.min.css',
    ];
    public $js = [
        'js/jcrop/jquery.Jcrop.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
    ];
}
