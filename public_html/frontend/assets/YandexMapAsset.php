<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class YandexMapAsset extends AssetBundle
{
    public $baseUrl = '@web';

    public $js = [
       // 'https://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU',
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
    ];
    public $depends = [];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
