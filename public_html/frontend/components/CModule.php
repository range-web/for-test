<?php
namespace frontend\components;

class CModule extends \yii\base\Module
{
    public function init()
    {
        $this->setLayoutPath(\Yii::$app->params['theme']['layoutsPath']);
        parent::init();
    }
}