<?php
namespace frontend\components;

use common\models\Settings;

class Contacts
{
    public static function getPhone($home=false)
    {
        $phones = [];

        $phones[] = [
            'phone' => Settings::getCacheValue('contactPhone', 'contacts')
        ];

        $addPhones = \Yii::$app->cache->get('additionalPhones');

        if(!empty($addPhones)) {
            foreach ($addPhones as $phone) {
                if ($home) {
                    if ($phone['viewHome'] == 1) {
                        $phones[] = $phone;
                    }
                } else {
                    $phones[] = $phone;
                }
            }
        }

        return $phones;
    }

    public static function getAddress($home=false)
    {
        $addresses = [];

        $addresses[] = [
            'address' => Settings::getCacheValue('contactAddress', 'contacts'),
            'coords' => Settings::getCacheValue('contactAddressCoords', 'contacts'),
        ];

        $addAddresses = \Yii::$app->cache->get('additionalAddresses');

        if(!empty($addAddresses)) {
            foreach ($addAddresses as $address) {
                if ($home) {
                    if ($address['viewHome'] == 1) {
                        $addresses[] = $address;
                    }
                } else {
                    $addresses[] = $address;
                }
            }
        }

        return $addresses;
    }

    public static function getEmail($home=false)
    {
        $emails = [];

        $emails[] = [
            'email' => Settings::getCacheValue('contactEmail', 'contacts')
        ];

        $addEmail = \Yii::$app->cache->get('additionalEmails');

        if(!empty($addEmail)) {
            foreach ($addEmail as $email) {
                if ($home) {
                    if ($email['viewHome'] == 1) {
                        $emails[] = $email;
                    }
                } else {
                    $emails[] = $email;
                }
            }
        }

        return $emails;
    }
}