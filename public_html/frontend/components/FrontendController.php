<?php
namespace frontend\components;

use yii;
use common\models\Settings;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;


/**
 * Site controller
 */
class FrontendController extends Controller
{
    public $enableCsrfValidation = false;

    public $metaTitle;
    public $metaDescription;
    public $metaKeywords;
    public $pageId;
    public $layout = 'column1';
    public $homePage = false;
    public $pageClass = 'page';

    public $shareInfo = [];
    
    public $showTitle = true;
    
    public function init()
    {
        //$this->security();
    }

    public function beforeAction($action)
    {
        // Чистим ассетс
        /*if(defined('YII_DEBUG') && YII_DEBUG){
            Yii::$app->assetManager->forceCopy = true;
        }*/

        if (parent::beforeAction($action)) {

            /*if (\Yii::$app->user->isGuest && $action->id != 'login') {
                return $this->redirect(['/site/login']);
            }

            if (!\Yii::$app->user->isGuest && !(Yii::$app->user->can('user') || Yii::$app->user->can('superadmin') || Yii::$app->user->can('admin'))) {
                throw new ForbiddenHttpException('Доступ запрещен');
            }

            if ( !\Yii::$app->user->can($action->id)) {
                throw new ForbiddenHttpException('Доступ запрещен');
            }*/
            return true;
        } else {
            return false;
        }
    }


    protected function setTags($model=null)
    {
        if ($model !=null) {
            $this->pageId = $model->id;
            $this->view->title = $model->title;

            if ($model->meta_title == null) {
                $this->metaTitle = $model->title;
            } else {
                $this->metaTitle = $model->meta_title;
            }

            if ($model->meta_description == null) {
                $this->metaDescription = Settings::getCacheValue('metaDescription');
            } else {
                $this->metaDescription = $model->meta_description;
            }
            if ($model->meta_keywords == null) {
                $this->metaKeywords = Settings::getCacheValue('metaKeywords');
            } else {
                $this->metaKeywords = $model->meta_keywords;
            }


        } else {
            $this->metaDescription = Settings::getCacheValue('metaDescription');
            $this->metaKeywords = Settings::getCacheValue('metaKeywords');
        }

    }

    protected function setTitle($title)
    {
        $this->view->title = $title;
        $this->metaTitle = $title;
    }

    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
//            'eauth' => array(
//                // required to disable csrf validation on OpenID requests
//                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
//                'only' => array('login'),
//            ),
        ];
    }
}
