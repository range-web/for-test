<?php
namespace frontend\components;

use Yii;
use common\models\Settings;

class Mail
{
    public static function send($to, $fromEmail, $fromName, $subject, $message)
    {
        return Yii::$app->mail->compose(['html' => 'empty-html', 'text' => 'empty-text'], ['content' => $message])
            ->setTo($to)
            ->setFrom([$fromEmail => $fromName])
            ->setSubject($subject)
            // ->setTextBody($message)
            ->send();
    }
}