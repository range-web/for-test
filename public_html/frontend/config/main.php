<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'layoutPath' => '@frontend/themes/basic/views/layouts',
    'viewPath' => '@frontend/themes/basic/views',
    'homeUrl' => '/',
    'components' => [
        /*'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                    'css' => ['css/main.css'],
                ],
            ],
        ],*/
        'request' => [
            'baseUrl' => '',
        ],
        'user' => [
            'identityClass' => 'common\modules\users\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'pages/default/view',
                'header-contact-form' => 'site/header-contact-form',
                'poll-form' => 'site/poll-form',
                'coupon-form' => 'site/coupon-form',
                'get-include-area' => 'site/get-include-area',
                'contacts' => 'site/contacts',
                'portfolio' => 'site/portfolio',
                'responses' => 'site/responses',
                'promotions' => 'site/promotions',
                'online' => 'site/online',
                'online/<city_id:\d+>' => 'site/online',
                'online/rec' => 'site/online-save-form',
                'sitemap' => 'site/site-map',
                'sitemap.xml' => 'site/site-map-xml',
                'login' => 'site/login',
                'login/<service:google|facebook|etc>' => 'site/login',
                'reset-password' => 'site/request-password-reset',
                'signup' => 'site/signup',
                'finish-signup' => 'site/eauth',
                'logout' => 'site/logout',
                'profile/' => '/users/default/index',
                'profile/<id:\d+>' => '/users/default/index',
                'profile/edit' => '/users/default/update',
                'profile/create' => '/users/default/create',
                'profile/images' => '/users/default/images-update',
                'profile/adverts' => '/users/default/adverts',
                'profile/adverts-notifications' => '/users/default/adverts-notifications',
                'profile/bookmarks' => '/users/default/bookmarks',
                
                'comments/add' => '/site/add-comment',
                'comments/update' => '/site/update-comment',
                'comments/delete' => '/site/delete-comment',

               // 'pages/site/captcha' => '/pages/default/captcha',
                
                ['class' => 'app\modules\pages\components\PagesUrlRule'],
                //['class' => 'app\modules\news\components\NewsUrlRule'],

                '<controller>/<action>' => '<controller>/<action>',
                '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'class' => '\rmrevin\yii\minify\View',
            'enableMinify' => true,
            'concatCss' => true, // concatenate css
            'minifyCss' => true, // minificate css
            'concatJs' => true, // concatenate js
            'minifyJs' => true, // minificate js
            'minifyOutput' => true, // minificate result html page
            'web_path' => '@web', // path alias to web base
            'base_path' => '@webroot', // path alias to web base
            'minify_path' => '@webroot/minify', // path alias to save minify result
            'js_position' => [ \yii\web\View::POS_END ], // positions of js files to be minified
            'force_charset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
            'expand_imports' => true, // whether to change @import on content
            'compress_options' => ['extra' => true], // options for compress
            'excludeBundles' => [

            ],
        ]
    ],
    'modules' => [
        'pages' => [
            'class' => 'frontend\modules\pages\Module',
        ],

        /*'news' => [
            'class' => 'frontend\modules\news\Module',
        ],*/
        /*'gallery' => [
            'class' => 'frontend\modules\gallery\Module',
        ],*/
        /*'blogs' => [
            'class' => 'frontend\modules\blogs\Module',
        ],*/
        'comments' => [
            'class' => 'rangeweb\comments\Module',
        ],
        'filesystem' => [
            'class' => 'rangeweb\filesystem\Module',
        ],
        'users' => [
            'class' => 'frontend\modules\users\Module',
        ],
    ],
    'params' => $params,
];
