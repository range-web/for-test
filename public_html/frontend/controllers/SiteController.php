<?php
namespace frontend\controllers;

use backend\modules\polls\models\PollsResult;
use common\components\ContentHelper;
use common\models\Notifications;
use common\models\Settings;
use common\models\UserSocialLinks;
use common\modules\rbac\rules\UserGroupRule;
use common\modules\users\models\Eauth;
use common\modules\users\models\FinishRegisterForm;
use frontend\components\FrontendController;
use frontend\models\CallBackForm;
use frontend\models\CouponForm;
use frontend\models\FeedBackForm;
use frontend\models\HeaderContactForm;
use frontend\models\PollForm;
use frontend\models\ResponseForm;
use frontend\modules\pages\models\Pages;
use frontend\modules\users\models\User;
use frontend\widgets\includeAreaWidget\models\IncludeAreaForm;
use rangeweb\comments\models\Comments;
use rangeweb\comments\models\search\CommentsSearch;
use rangeweb\filesystem\models\File;
use Yii;
use frontend\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\web\Response;
use common\modules\users\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\SignupForm;
/**
 * Site controller
 */
class SiteController extends FrontendController
{

    public function behaviors() {
        return [
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'edit-menu' => [
                'class' => 'frontend\widgets\menu\actions\UpdateAction',
                'callback' => [$this, 'editMenuCallBack']
            ],
            'save-menu' => [
                'class' => 'frontend\widgets\menu\actions\SaveAction',
                'callback' => [$this, 'saveMenuCallBack']
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'height' => 35,
                'backColor' => 0xf9f9f9,
                'foreColor' => 0x333333,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],           
        ];
    }

    public function editMenuCallBack($model)
    {
        
    }

    public function saveMenuCallBack($model)
    {

    }

    public function actionIndex()
    {
        $this->homePage = true;
        $this->showTitle = false;
        $this->pageClass = 'home';

        $page = Pages::find()->where('id=1')->one();

        $this->setTags($page);

        return $this->render('index', ['page'=>$page]);
    }

    public function actionHeaderContactForm()
    {
        $model = new HeaderContactForm();
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->saveContacts();

            if ($model->sendEmail()) {
                return [
                    'status' => true
                ];
            }
        }

        return [
            'status' => false
        ];
    }

    public function actionPollForm()
    {
        $model = new PollForm();
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model->id = $_POST["HeaderContactForm"]['id'];

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail()) {
                return [
                    'status' => true
                ];
            }
        }

        return [
            'status' => false
        ];
    }

    public function actionCouponForm()
    {
        $model = new CouponForm();
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model->id = $_POST["HeaderContactForm"]['id'];

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail()) {
                return [
                    'status' => true
                ];
            }
        }

        return [
            'status' => false
        ];
    }

    /*public function actionContacts()
    {
        $page = Pages::find()->where('id=2')->one();
        $this->setTags($page);
        $this->pageClass = 'contacts';
        $this->layout = 'column1';

        $model = new ContactForm();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Спасибо за обращение! В ближайшее время наш менеждер свяжется с Вами.');
            } else {
                Yii::$app->session->setFlash('error', 'При отправке произошла ошибка, попробуйте отправить через несколько минут.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }*/
    
    /*public function actionTest()
    {
        return $this->render('test');
    }*/
    
    public function actionSiteMap()
    {
        $content = ContentHelper::model()->generateContentArray(true);

        return $this->render('sitemap', ['content'=>$content]);
    }

    public function actionSiteMapXml()
    {
        $content = ContentHelper::model()->generateXmlMap(true);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        echo $content;
    }

    public function actionLogin()
    {
        //$user = User::findByEmail('admin@rangeweb.ru');
        //Yii::$app->user->login($user, 3600 * 24 * 30);

        $serviceName = Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {
                    if (User::findByEAuth($eauth)){
                        return $eauth->redirect(['eauth']);
                    } else {
                        $eauth->redirect();
                    }
                }
                else {
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());
                $eauth->redirect($eauth->getCancelUrl());
            }
        }
        
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = new LoginForm();

            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                if ($model->userNotFound) {
                    return ['status' => true, 'action' => 'registration'];
                } else {
                    
                    return ['status' => true, 'action' => 'auth', 'user' => $model->user];
                }
            }
            return ['status' => false, 'errors' => $model->errors];
        }
    }

    public function actionSignup()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        if (Yii::$app->request->isAjax) {
            $model = new SignupForm();

            if ($model->load(Yii::$app->request->post()) && $user = $model->signup()) {
                
                if (!empty($model->errors)) {
                    return ['status' => false, 'errors' => $model->errors];
                }
                
                if ($model->existingUser) {
                    return ['status' => true];
                } else {
                    if (Yii::$app->getUser()->login($user)) {
                        return ['status' => true, 'user' => [
                            'firstname' => $user->firstname
                        ]];
                    }
                }
            }
        }
        return ['status' => false];
    }


    public function actionEauth()
    {
        $this->setTitle('Завершение регистрации');
        $this->setTags();
        $this->layout = 'column1';

        $eauth = Yii::$app->session->get('eauth');
        if ($eauth === false) {
            return $this->goBack();
        }

        $model = new LoginForm();
        $finishRegisterForm = new FinishRegisterForm();

        if (isset($_POST['auth_type']) && $_POST['auth_type'] == 1) {

            if ($finishRegisterForm->load(Yii::$app->request->post()) && $user = $finishRegisterForm->register()) {
                $userService = new Eauth();
                $userService->username = $eauth['username'];
                $userService->user_id = $user->id;
                $userService->service = $eauth['profile']['service'];
                $userService->auth_key = $eauth['auth_key'];
                $userService->auth_id = $eauth['id'];

                if ($userService->save()){
                    
                    $socialLinks = new UserSocialLinks();
                    $socialLinks->social_id = $userService->service;
                    $socialLinks->user_id = $user->id;
                    $socialLinks->link = $eauth['profile']['url'];
                    $socialLinks->save();
                    
                    Yii::$app->user->login($user, 0);
                    return $this->redirect(['/users/default/index']);
                }
            }

        } else if (isset($_POST['auth_type']) && $_POST['auth_type'] == 2){

            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $userService = new Eauth();
                $userService->username = $eauth['username'];
                $userService->user_id = Yii::$app->user->id;
                $userService->service = $eauth['profile']['service'];
                $userService->auth_key = $eauth['auth_key'];
                $userService->auth_id = $eauth['id'];
                if ($userService->save()){

                    $socialLink = UserSocialLinks::find()
                        ->where('social_id = :social_id AND user_id = :user_id', [
                            'social_id' => $userService->service,
                            'user_id' => Yii::$app->user->id
                        ])
                        ->one();

                    if ($socialLink == null || strlen($socialLink->link) == 0) {
                        $socialLinks = new UserSocialLinks();
                        $socialLinks->social_id = $userService->service;
                        $socialLinks->user_id = Yii::$app->user->id;
                        $socialLinks->link = $eauth['profile']['url'];
                        $socialLinks->save();
                    }

                    return $this->redirect(['/users/default/index']);
                }
            }
        }

        if (isset($eauth['profile']["first_name"]) && $finishRegisterForm->first_name == null) {
            $finishRegisterForm->first_name = $eauth['profile']["first_name"];
        }
        if (isset($eauth['profile']["last_name"]) && $finishRegisterForm->last_name == null) {
            $finishRegisterForm->last_name = $eauth['profile']["last_name"];
        }
        if (isset($eauth['profile']["email"]) && $finishRegisterForm->email == null) {
            $finishRegisterForm->email = $eauth['profile']["email"];
        }

        return $this->render('eauth', [
            'model' => $model,
            'eauth' => $eauth,
            'finishRegisterForm' => $finishRegisterForm
        ]);
    }



    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionFeedBack()
    {
//        var_dump([Settings::getCacheValue('smtpHost'), Settings::getCacheValue('smtpEmail'), Settings::getCacheValue('smtpPass'), Settings::getCacheValue('smtpPort')]);die;
//        var_dump(Yii::$app->mailer->compose()
//            ->setTo('borschev@rangeweb.ru')
//            ->setFrom([Settings::getCacheValue('smtpEmail') => Settings::getCacheValue('smtpName')])
//            ->setSubject('Новая заявка с сайта mntt.ru')
//            ->setTextBody('Тест отправки')
//            ->send());die;


        $model = new FeedBackForm();
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($model->load(Yii::$app->request->post())) {

            if ($model->sendEmail(Settings::getActValue('sendContactFormEmail', 'contacts'))) {
                return [
                    'status' => true
                ];
            }
        }
        return [
            'status' => false
        ];
    }

    public function actionCallBack()
    {
        $model = new CallBackForm();
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($model->load(Yii::$app->request->post())) {

            if ($model->sendEmail(Settings::getActValue('sendContactFormEmail', 'contacts'))) {
                return [
                    'status' => true
                ];
            }
        }
        return [
            'status' => false
        ];
    }
    
    public function goBack($defaultUrl = null)
    {
        if (Url::previous() != null) {
            return $this->redirect([Url::previous()]);
        }
        return parent::goBack($defaultUrl);
    }

    public function actionActivation($username, $key)
    {
        if ($model = User::find()->where(['and', 'username = :username', 'auth_key = :auth_key'], [':username' => $username, ':auth_key' => $key])->one()) {
            $model->status = User::STATUS_ACTIVE;
            if ($model->save(false)) {
                Yii::$app->session->setFlash('success', 'Ваша учётная запись была успешно активирована. Теперь вы можете авторизоватся, и полноценно использовать услуги сайта. Спасибо!');
            }
        } else {
            Yii::$app->session->setFlash('danger', 'Неверный код активации или возмоможно аккаунт был уже ранее активирован. Проверьте пожалуйста правильность ссылки, или напишите администратору.');
        }
        return $this->redirect('login');
    }

    public function actionRequestPasswordReset()
    {
        $this->layout = 'column1';
        $this->setTitle('Восстановление пароля');
        $this->setTags();

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        $this->setTitle('Сброс пароля');
        $this->setTags();

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


    public function actionReviews()
    {
        $this->setTitle('Отзывы о Блабларент');
        $this->setTags();

        $searchModel = new CommentsSearch();
        $searchModel->object = 'site';
        $searchModel->object_id = 1;
        $searchModel->status = Comments::STATUS_PUBLIC;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('reviews', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionGetIncludeArea()
    {
        if (isset($_POST['file_name'])) {
            $fileName = $_POST['file_name'];
            
            $filePath = FileHelper::normalizePath(Yii::getAlias('@frontend/static/include/'.$fileName.'.php'));
            
            if (file_exists($filePath)) {
                
                $model = new IncludeAreaForm();
                $model->text = file_get_contents($filePath);
                $model->fileName = $fileName;

                Yii::$app->response->format = Response::FORMAT_JSON;

                return  $this->renderAjax('include_area_form', [
                        'model' => $model
                    ]);


            } else {
                echo 'Файл не найден';
            }
        }
    }

    public function actionSaveIncludeArea()
    {
        $model = new IncludeAreaForm();
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            return [
                'status' => true
            ];
        } else {
            return ['status' => false, 'errors' => $model->errors];
        }
    }

    public function actionImageCrop()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $result = ['status' => false];

        if (isset($_POST['id']) && isset($_POST['x']) && isset($_POST['y']) && isset($_POST['w']) && isset($_POST['h'])) {
            $file = File::find()
                ->where('id = :id AND user_id = :user_id', ['id' => $_POST['id'], 'user_id' => Yii::$app->user->id])
                ->one();

            $res = Image::crop($file->getPath(), $_POST['w'], $_POST['h'], [$_POST['x'], $_POST['y']])
                ->save($file->getPath(), ['quality' => 100]);


            $file = $file->getUrl([250, 250]);

            if ($file) {
                $result = ['status' => true, 'url' => $file['url']];
            }

        }

        return $result;
    }
}
