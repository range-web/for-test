<?php

namespace frontend\models;

use common\models\Settings;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CallBackForm extends Model
{
    public $name;
    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'phone'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'phone' => 'Телефон',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([Settings::getCacheValue('smtpEmail') => Settings::getCacheValue('smtpName')])
            ->setSubject('Заказ обратного звонка с сайта gutbuh.ru')
            ->setTextBody($this->name.' заказал обратный звонок<br>Номер телефона: '.$this->phone)
            ->send();
    }
}
