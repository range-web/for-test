<?php

namespace frontend\models;

use common\models\Settings;
use frontend\components\Mail;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $body;
    public $verifyCode;
    public $message;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            ['message', 'messageValidate'],
            ['phone', 'safe']
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
        ];
    }

    public function messageValidate($attribute, $params)
    {
        if ($this->$attribute != null) {
            $this->addError($attribute, 'Произошла ошибка');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'email' => 'Email',
            'phone' => 'Телефон',
            'body' => 'Текст сообщения',
            'message' => 'Сообщение',
            'verifyCode' => 'Введите код с картинки',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        $message = '';
        
        $message .= '
        <strong>ФИО:</strong> '.$this->name.'<br>
        <strong>Email:</strong> '.$this->email.'<br>
        <strong>Телефон:</strong> '.$this->phone.'<br>
        <strong>Текст сообщения:</strong> '.$this->body.'<br>
        ';

        return Yii::$app->mail->compose(['html' => 'empty-html', 'text' => 'empty-text'], ['content' => $message])
            ->setTo(Settings::getActValue('sendContactFormEmail', 'contacts'))
            ->setFrom([Settings::getCacheValue('smtpEmail') => Settings::getCacheValue('smtpName')])
            ->setSubject('Сообщение из контактной формы')
            ->send();
    }
}
