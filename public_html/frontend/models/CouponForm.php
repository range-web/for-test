<?php

namespace frontend\models;

use backend\modules\polls\models\Polls;
use backend\modules\polls\models\PollsResult;
use common\models\Settings;
use frontend\components\Mail;
use rangeweb\filesystem\models\File;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CouponForm extends Model
{
    public $id;
    public $email;
    public $subscribe;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'email'], 'required'],
            ['subscribe', 'safe']
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'email' => 'Email',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        $pollResult = PollsResult::find()
            ->where(['id' => $this->id])
            ->one();

        if ($pollResult) {

            $pollResult->email = $this->email;
            $pollResult->subscribe = ($this->subscribe) ? 1 : 0;
            $pollResult->save();

            $message = 'Купон на 1000 рублей<br><br>';

            $couponFile = File::find()
                ->where(['name' => 'kupon'])
                ->one();

            if ($couponFile) {

                $attach = $couponFile->getPath();

                $adminMessage = '';
                $adminMessage .= '
                    <strong>Имя:</strong> '.$pollResult->name.'<br>
                    <strong>Телефон:</strong> '.$pollResult->phone.'<br>
                    <strong>E-mail:</strong> '.$pollResult->email.'<br>
                    <strong>Подписка на рассылку:</strong> ';

                $adminMessage .= ($pollResult->subscribe) ? 'Да':'Нет';

                Yii::$app->mail->compose(['html' => 'empty-html', 'text' => 'empty-text'], ['content' => $adminMessage])
                    ->setTo(Settings::getActValue('sendContactFormEmail', 'contacts'))
                    ->setFrom([Settings::getCacheValue('smtpEmail') => Settings::getCacheValue('smtpName')])
                    ->setSubject($pollResult->name . ' ('. $pollResult->phone . ') Запрос на отправку купона')
                    ->send();

                return $msg = Yii::$app->mail->compose(['html' => 'image-html', 'text' => 'empty-text'], ['content' => $message, 'imageFileName' => $attach])
                    ->setTo($this->email)
                    ->setFrom([Settings::getCacheValue('smtpEmail') => Settings::getCacheValue('smtpName')])
                    ->setSubject('Купон на 1000 рублей')
                    ->send();
            }

            return false;
        }
    }
}
