<?php

namespace frontend\models;

use backend\modules\polls\models\PollsResult;
use common\models\Settings;
use frontend\components\Mail;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class HeaderContactForm extends Model
{
    public $id;
    public $name;
    public $phone;
    public $message;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'id'], 'required'],
            ['message', 'messageValidate'],
        ];
    }

    public function messageValidate($attribute, $params)
    {
        if (strlen($this->$attribute) > 0) {
            $this->addError($attribute, 'Произошла ошибка');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'message' => 'Сообщение',
        ];
    }

    public function saveContacts()
    {
        $pollResult = new PollsResult();
        $pollResult->id = $this->id;
        $pollResult->name = $this->name;
        $pollResult->phone = $this->phone;
        $pollResult->save();
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        $message = '';

        $message .= '
        <strong>Имя:</strong> '.$this->name.'<br>
        <strong>Телефон:</strong> '.$this->phone.'<br>
        ';

        return Yii::$app->mail->compose(['html' => 'empty-html', 'text' => 'empty-text'], ['content' => $message])
            ->setTo(Settings::getActValue('sendContactFormEmail', 'contacts'))
            ->setFrom([Settings::getCacheValue('smtpEmail') => Settings::getCacheValue('smtpName')])
            ->setSubject($this->name . ' ('. $this->phone . ') заполнил форму для прохождения теста')
            ->send();
    }
}
