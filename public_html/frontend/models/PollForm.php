<?php

namespace frontend\models;

use backend\modules\polls\models\Polls;
use backend\modules\polls\models\PollsResult;
use common\models\Settings;
use frontend\components\Mail;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class PollForm extends Model
{
    public $id;
    public $data;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'data'], 'required'],
            ['data', 'dataValidate']
        ];
    }

    public function dataValidate($attribute, $params)
    {
        if (empty($this->$attribute)) {
            $this->addError($attribute, 'Произошла ошибка');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'message' => 'Сообщение',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        $pollResult = PollsResult::find()
            ->where(['id' => $this->id])
            ->one();

        if ($pollResult) {

            $pollResultData = [];

            foreach ($this->data as $id=>$val) {
                $question = Polls::find()
                    ->where(['id' => $id])
                    ->one();

                if ($question) {
                    $pollResultData[] = [
                        'question' =>  $question->question,
                        'answer' => $question->_answers[$val]
                    ];
                }
            }

            $pollResult->results = json_encode($pollResultData);
            $pollResult->save();

            $message = '';

            $message .= '
            <strong>Имя:</strong> '.$pollResult->name.'<br>
            <strong>Телефон:</strong> '.$pollResult->phone.'<br>
            <strong>Результаты опроса: </strong><br><br>';

            foreach ($pollResultData as $value) {
                $message .= '<strong>'.$value['question'] . '</strong><br>
                            &nbsp;&nbsp;&nbsp;'.$value['answer'].'<br><br>';
            }

            return Yii::$app->mail->compose(['html' => 'empty-html', 'text' => 'empty-text'], ['content' => $message])
                ->setTo(Settings::getActValue('sendContactFormEmail', 'contacts'))
                ->setFrom([Settings::getCacheValue('smtpEmail') => Settings::getCacheValue('smtpName')])
                ->setSubject($pollResult->name . ' ('. $pollResult->phone . ') Результаты опроса')
                ->send();
        }
    }
}
