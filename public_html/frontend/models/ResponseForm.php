<?php
namespace frontend\models;

use rangeweb\comments\models\Comments;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class ResponseForm extends Model
{
    public $rating;
    public $text;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            ['text', 'filter', 'filter' => 'trim'],
            ['rating', 'required', 'message' => 'Пожалуйста, оцените качество работы'],
            ['text', 'required', 'message' => 'Пожалуйста, введите текст отзыва'],
            ['rating', 'integer'],
            ['text', 'string', 'min' => 20, 'message' => 'Минимальная длина текста отзыва 20 символов'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'rating' => 'Оценка качества работы',
            'text' => 'Текст отзыва',
        ];
    }

    public function save()
    {
        if (Yii::$app->user->isGuest)
            return false;

        if ($this->validate()) {

            $response = new Comments();
            $response->object = 'site';
            $response->object_id = 1;
            $response->text = $this->text;
            $response->rating = $this->rating;
            $response->show_main = 0;
            
            if ($response->save()) {
                return $response;
            }

        }

        return false;
    }
}
