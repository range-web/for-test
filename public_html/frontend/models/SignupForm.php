<?php
namespace frontend\models;

use common\models\Settings;
use common\modules\users\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $firstname;
    public $lastname;
    public $sex;
    public $birthday;
    public $city;
    public $phone;
    public $email;
    public $password;
    public $agreement;

    public $existingUser = false;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'password', 'phone'], 'filter', 'filter' => 'trim'],
//            ['username', 'required'],
//            ['username', 'unique', 'targetClass' => '\common\modules\users\models\User', 'message' => 'Данный логин занят'],
//            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            //['email', 'unique', 'targetClass' => '\common\modules\users\models\User', 'message' => 'Данный email занят'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6, 'message' => 'Пароль слишком короткий, минимум 6 символов'],

            [['firstname'], 'required'],
            [['firstname', 'lastname'], 'string', 'max' => 50],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'email' => 'Email',
            'password' => 'Пароль',
            'repassword' => 'Подтверждение пароля',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'city' => 'Город',
            'birthday' => 'Дата рождения',
            'phone' => 'Контактный телефон',
            //'agreement' => 'Я соглашаюсь с условиями сервиса',
        ];
    }



    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {

            $user = User::find()->where(['email'=>$this->email])->one();
            // $user->setScenario('singUp');

            if ($user != null) {
                $this->addError('email', 'Данный E-mail занят.');
                return null;
                 /*$this->existingUser = true;
                 return $user;*/
            } else {
                $user = new User(['scenario'=>'singUp']);
            }

            $user->firstname = $this->firstname;
            $user->lastname = $this->lastname;
            $user->city_id = $this->city;
            $user->email = $this->email;
            $user->phone = $this->phone;
            $user->password = $this->password;
            $user->status = User::STATUS_ACTIVE;

            $user->generateAuthKey();

            /*if (Settings::getCacheValue('confirmAuthByEmail', 'Users') == 1) {
                Yii::$app->session->setFlash('success', 'Регистрация прошла успешно. На указанный почтовый адрес отправлена ссылка, для активации учетной записи.');
                if ($user->isNewRecord) {
                    $user->on(User::EVENT_AFTER_INSERT, [$user, 'onNewUser']);
                } else {
                    $user->on(User::EVENT_AFTER_UPDATE, [$user, 'onNewUser']);
                }
            }*/
            //$user->validate();
            
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
