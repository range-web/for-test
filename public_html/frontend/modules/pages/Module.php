<?php

namespace frontend\modules\pages;

use backend\modules\pages\models\PagesAdmin;
use frontend\components\CModule;
use Yii;

class Module extends CModule
{
    public $controllerNamespace = 'app\modules\pages\controllers';

    public function init()
    {
        parent::init();
    }
}
