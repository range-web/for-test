<?php
namespace app\modules\pages\components;

use backend\modules\pages\models\BasePages;
use yii\web\UrlRule;

class PagesUrlRule extends UrlRule
{
    public $connectionID = 'db';

    public function init()
    {
        if ($this->name === null) {
            $this->name = __CLASS__;
        }
    }

    /*public function createUrl($manager, $route, $params)
    {
        if ($route === 'pages/default/view') {
            if (isset($params['manufacturer'], $params['model'])) {
                return $params['manufacturer'] . '/' . $params['model'];
            } elseif (isset($params['manufacturer'])) {
                return $params['manufacturer'];
            }
        }
        return false;  // это правило не подходит
    }*/

    public function parseRequest($manager, $request)
    {
        $pathInfo = BasePages::getPathsMap();

        $id = array_search($request->pathInfo, $pathInfo);

        if ($id === false)
            return false;

        return ['pages/default/view', ['id'=>$id]];
    }
}