<?php

namespace app\modules\pages\controllers;

use backend\modules\polls\models\Polls;
use backend\modules\polls\models\search\PollsSearch;
use frontend\components\FrontendController;
use frontend\modules\pages\models\Pages;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use frontend\models\ContactForm;
use common\models\Settings;

class DefaultController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'height' => 40,
                'backColor' => 0xf9f9f9,
                'foreColor' => 0x333333,
                'fixedVerifyCode' => null,
                'testLimit' => 0
                //'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionView($id=null)
    {
        if ($id == null) {
            $id = 1;
            $this->homePage = true;
            $this->pageClass = 'home';
        }

        $page = $this->findModel($id);
        
        $this->setTags($page);

        if ($page->layout != null) {
            $this->layout = $page->layout;
        }

        if ($page->id == 1) {
            return $this->render('index', ['page'=>$page]);
        } else if ($page->id == 2)  {

            $this->pageClass = 'contacts';

            $model = new ContactForm();

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                if ($model->sendEmail()) {
                    Yii::$app->session->setFlash('success', 'Спасибо за обращение! В ближайшее время мы свяжется с Вами.');
                } else {
                    Yii::$app->session->setFlash('error', 'При отправке произошла ошибка, попробуйте отправить через несколько минут.');
                }

                return $this->refresh();
            } else {

                return $this->render('contacts', [
                    'model' => $model,
                    'page' => $page
                ]);
            }
        }

        return $this->render('view', ['page'=>$page]);
    }

    /**
     * Finds the PagesAdmin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $condition = 'is_published=1 AND id='.(int)$id;

        if (\Yii::$app->user->isGuest) {
            $condition .=' AND access<>'.Pages::ACCESS_REGISTER;
        }

        $model = Pages::find()
            ->where($condition)
            ->one();

        if ($model->is_folder) {
            $model = Pages::find()->where(['parent_id'=>$model->id, 'is_published'=>1])->orderBy('sort ASC')->one();
            if ($model !== null) {
                $this->redirect(Pages::getLinkById($model->id));
            }
        }

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница не найдена.');
        }
    }
}
