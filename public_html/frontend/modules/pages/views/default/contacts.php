<?php
use frontend\components\WidgetBehavior;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use frontend\widgets\Alert;

\frontend\assets\YandexMapAsset::register($this);

foreach ($page->getParents($page->parent_id) as $parent) {
    $this->params['breadcrumbs'][] = [
        'label' => $parent->title,
        'url' => ['/'.$parent->getLink()]
    ];
}

$this->params['breadcrumbs'][] = $page->title;
?>
    <div class="header-slider">
        <?=\frontend\widgets\pageSlider\PageSliderWidget::widget([
                'page' => $page
        ])?>
    </div>

    <div class="container">
        <div class="row">

            <div class="col-xs-12">
                <?php
                echo \yii\widgets\Breadcrumbs::widget([
                    'itemTemplate' => "<li><i>{link}</i></li>\n",
                    'links' => $this->params['breadcrumbs'],
                ]);
                ?>
            </div>

            <div class="col-xs-12">
                <h1 class="page__title"><?=$this->title?></h1>
            </div>
            <div class="col-xs-12 col-lg-6">
                <?php /* ?>

                <h2 class="mb-30">Контактная информация</h2>

                <p>
                    Тел.: 8 (800) 555-55-55
                </p>
                <p>
                    E-mail: plushki-vatrushki@gmail.com
                </p>
                <p>
                    Режим работы: пн-вс 08:00 - 20:00
                </p>

                <h3 class="mb-20">Наши адреса:</h3>

                <p data-coords="[58.476758, 41.537992]">
                    г. Буй, ул. Красной Армии, 10
                </p>
                <p data-coords="[57.720659, 39.826816]">
                    г. Ярославль, Спартаковская ул., 37а
                </p>

                <div id="map" style="width: 100%; height: 300px"></div>
                <?php */ ?>
                <?= WidgetBehavior::prepare($page->content); ?>
            </div>
            <div class="col-xs-12 col-lg-6">
                <h2 class="mb-30">Отправить нам сообщение</h2>

                <?= Alert::widget() ?>

                <?php $form = ActiveForm::begin([
                    'id' => 'contact-form',
                ]); ?>
                <?= $form->field($model, 'name')->textInput(['placeholder'=>'ФИО'])->label(false) ?>
                <?= $form->field($model, 'email')->textInput(['placeholder'=>'Email'])->label(false) ?>
                <?= $form->field($model, 'phone')->textInput(['placeholder'=>'Телефон'])->label(false)  ?>
                <?= $form->field($model, 'body')->textArea(['rows' => 6, 'placeholder'=>'Текст сообщения'])->label(false) ?>
                <?= $form->field($model, 'message')->textInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-default', 'name' => 'contact-button']) ?>
                </div>
                <div class="form-group">
                    Нажимая кнопку «Отправить», вы даете <a href="/privacy" target="_blank">согласие на обработку своих персональных данных</a>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
<?php if (strlen($page->styles) > 0) {
    $this->registerCss($page->styles);
}


$script = "
$(document).ready(function() {
    var myMap;
    var coords = $($('[data-coords]')[0]).data('coords');
    
   
    ymaps.ready(init);

    function init () {
        myMap = new ymaps.Map('map', {
            center: coords,
            zoom: 16,
            controls: ['zoomControl', 'typeSelector',  'fullscreenControl']
        });
        myMap.behaviors.disable('scrollZoom');

        $('[data-coords]').each(function(i, v) {
            var address = $(v);
        
            myMap.geoObjects
                .add(new ymaps.Placemark(address.data('coords'), {
                    hintContent: address.text(),
                }, {
                    preset: 'islands#brownDotIcon',
                }))
        })

        
    }


    $('[data-coords]').on('click', function () {
        
        myMap.setCenter($(this).data('coords'),16);
        
        $('html,body').stop().animate({ scrollTop: $('#map').offset().top - 50 }, 300);
    });

});
";

$this->registerJs($script);


/*$this->registerJs("

$(function() {
    ymaps.ready(init);
    var myMap;
    var coords = $($('[data-coords]')[0]).data('coords');



    $(document)
        .on('click', '[data-coords]', function(e) {
            coords = $(this).data('coords');
            
            init();
        });

    function init(){    
   
        myMap = new ymaps.Map(\"map\", {
            center: coords,
            zoom: 16
        });
        
        myPlacemark = new ymaps.Placemark(coords, { 
            hintContent: 'г. Буй, ул. Красной Армии, 10', 
        }, {
            preset: \"islands#brownDotIcon\",
        });

        myMap.geoObjects.add(myPlacemark);
    }
});

");*/
