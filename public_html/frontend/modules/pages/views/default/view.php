<?php
use frontend\components\WidgetBehavior;

foreach ($page->getParents($page->parent_id) as $parent) {
    $this->params['breadcrumbs'][] = [
        'label' => $parent->title,
        'url' => ['/'.$parent->getLink()]
    ];
}

$this->params['breadcrumbs'][] = $page->title;
?>
    <div class="header-slider">
        <?=\frontend\widgets\pageSlider\PageSliderWidget::widget([
            'page' => $page
        ])?>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php
                echo \yii\widgets\Breadcrumbs::widget([
                    'itemTemplate' => "<li><i>{link}</i></li>\n",
                    'links' => $this->params['breadcrumbs'],
                ]);
                ?>
            </div>
            <div class="col-xs-12">
                <h1 class="page__title"><?=$this->title?></h1>
            </div>
        </div>
    </div>

    <?= WidgetBehavior::prepare($page->content); ?>

<?php if (strlen($page->styles) > 0) {
    $this->registerCss($page->styles);
}?>