<?php

namespace frontend\modules\users;

class Module extends \common\modules\users\Module
{
    public $controllerNamespace = 'frontend\modules\users\controllers';

    public function init()
    {
        $this->setLayoutPath('@frontend/themes/basic/views/layouts');
        parent::init();

        // custom initialization code goes here
    }
}
