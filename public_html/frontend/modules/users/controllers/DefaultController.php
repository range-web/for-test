<?php

namespace frontend\modules\users\controllers;

use common\models\Notifications;
use common\models\UserImages;
use common\models\UserSocialLinks;
use common\modules\adverts\models\Advert;
use common\modules\adverts\models\RoomImage;
use common\modules\adverts\models\search\AdvertSearch;
use common\modules\users\models\Eauth;
use common\modules\users\models\User;
use frontend\components\FrontendController;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\Response;

class DefaultController extends FrontendController
{
    public $_countPhoto;

    public function actions()
    {
        return [
            'upload-avatar' => [
                'class' => 'rangeweb\filesystem\actions\UploadAction',
                'uploadOnlyImage' => true,
                'path' =>'images'
            ],
            'delete-image' => [
                'class' => 'rangeweb\filesystem\actions\DeleteAction',
                'callback' => [$this, 'userImagesDelete']
            ]
        ];
    }

    public function imagesToUser($imageId)
    {
        $userImages = new UserImages();
        $userImages->image_id = $imageId;
        $userImages->save();
    }

    public function userImagesDelete($imageId)
    {
        UserImages::deleteAll('image_id = :image_id', ['image_id' => $imageId]);
    }

    public function actionIndex()
    {
        $this->setTitle('Личный кабинет');
        $this->setTags();

        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['/site/index']);
        }

        $id = Yii::$app->user->id;

        $data = [];
        $model = $this->findModel($id);
        $model->setScenario('userUpdate');

        if ($model->load(\Yii::$app->request->post())) {

            if (isset($_POST['User']['image_id'][0]) && $_POST['User']['image_id'][0] != null) {
                $model->image_id = $_POST['User']['image_id'][0];
            } else if (isset($_POST['User']['image_id_update']) && $_POST['User']['image_id_update'] != null) {
                $model->image_id = $_POST['User']['image_id_update'];
            }

            if ($model->save()) {
                \Yii::$app->session->setFlash('success', 'Успешно сохранено!');
                return $this->redirect(['index']);
            }
        }

        $data['model'] = $model;
        $data['socialLinks'] = $model->getSocialLinks(true);
        
        return $this->render('index', $data);
    }

    public function actionUpdate($id=null)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['/site/login']);
        }

        if ($id != null) {
            $id = intval($id);
        } else {
            $id = \Yii::$app->user->id;
        }

        $this->setTitle('Редактирование профиля');
        $this->setTags();

        $model = $this->findModel($id);

        if (!$model->isOwner()){
            if (!$model->canAdminEdit()) {
                $id = \Yii::$app->user->id;
                $model = $this->findModel($id);
            } else {
                $this->setTitle('Редактирование профиля: '.$model->firstname.' '.$model->lastname);
            }
        }
        
        $model->setScenario('userUpdate');
        
        $socialLinks = $model->getSocialLinks(true, true);

        /*$userEauth = Eauth::find()
            ->where(['user_id' => $model->id])
            ->all();*/

        if (!empty(\Yii::$app->request->post()) && $model->load(\Yii::$app->request->post())) {

            //echo'<pre>';var_dump($_POST);die;

            if (isset($_POST['User']['image_id'][0]) && $_POST['User']['image_id'][0] != null) {
                $model->image_id = $_POST['User']['image_id'][0];
            } else if (isset($_POST['User']['image_id_update']) && $_POST['User']['image_id_update'] != null) {
                $model->image_id = $_POST['User']['image_id_update'];
            }

            if ($model->save()) {

                if (isset($_POST['UserSocialLinks'])) {

                    foreach ($_POST['UserSocialLinks'] as $socialId=>$value) {
                        $userSocialLink = UserSocialLinks::find()
                            ->where('user_id = :user_id AND social_id = :social_id', ['user_id' => $id, 'social_id'=>$socialId])
                            ->one();

                        if(!$userSocialLink) {

                            if (strlen($value) == 0) continue;

                            $userSocialLink = new UserSocialLinks();
                            $userSocialLink->user_id = $id;
                            $userSocialLink->social_id = $socialId;
                        }

                        if ($userSocialLink->link != $value) {
                            $userSocialLink->link = $value;
                            $userSocialLink->save();
                        }
                    }
                }

                \Yii::$app->session->setFlash('success', 'Успешно сохранено!');


                if ($model->canAdminEdit()) {
                    return $this->redirect(['update', 'id' => $model->id]);
                } else {
                    return $this->redirect(['update']);
                }

            }
        }

        return $this->render('update',[
            'model'=>$model,
            'socialLinks' => $socialLinks
        ]);
    }

    public function actionDeleteAvatar()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $status = false;
        if (isset($_POST['delete_avatar']) && !\Yii::$app->user->isGuest) {
            $id = \Yii::$app->user->id;
            $model = $this->findModel($id);

            $model->image_id = null;
            if ($model->save()) {
                $status = true;
            }

        }

        return [
            'status' => $status
        ];
    }

    /**
     * Finds the Advert model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {

            if ($model->status != User::STATUS_ACTIVE && !User::canAdminEdit()) {
                throw new NotFoundHttpException('Пользователь не найден.');
            }

            if ($model->role_id != User::ROLE_USER && !User::canAdminEdit()) {
                throw new NotFoundHttpException('Пользователь не найден.');
            }

            return $model;
        } else {
            throw new NotFoundHttpException('Пользователь не найден.');
        }
    }
}
