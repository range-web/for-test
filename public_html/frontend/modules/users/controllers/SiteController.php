<?php

namespace frontend\modules\users\controllers;

use frontend\components\FrontendController;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\Response;
use common\modules\users\models\User;

class SiteController extends FrontendController
{
    public function actionLogin()
    {

        $redirectUrl = Url::toRoute(['/users/default/update']);

        /**
         * Привязка аккаунтов соц.сетей
         */
        $serviceName = Yii::$app->getRequest()->getQueryParam('service');

        if (isset($serviceName)) {
            
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl($redirectUrl);
            $eauth->setCancelUrl($redirectUrl);

            try {
                if ($eauth->authenticate()) {

                    \Yii::$app->session->setFlash('success', 'Аккаунт успешно привязан!');

                    if (User::findByEAuthAuthUsers($eauth)){
                        return $eauth->redirect($redirectUrl);
                    } else {
                        $eauth->redirect();
                    }
                }
                else {
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());
                $eauth->redirect($eauth->getCancelUrl());
            }
        }
    }
}
