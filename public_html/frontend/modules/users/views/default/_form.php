<?php
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\helpers\Html;

\frontend\assets\ImageCropAsset::register($this);
?>
<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-8">

            <div class="row mb-30">
                <div class="col-sm-4">
                    <div class="avatar">
                        <?= Html::img($model->getAvatarImageUrl([250, 250])) ?>
                    </div>
                    <div class="form-group text-center">
                        <?= rangeweb\filesystem\widgets\fileWidget\FileWidget::widget([
                            'model'=>$model,
                            'attribute'=>'image_id',
                            'required' => true,
                            'url' => '/users/default/upload-avatar',
                            'mimeTypes' => 'image/*',
                            'deleteOldFile' => true,
                            'htmlOptions' => [
                                'btn-upload-icon' => 'fa fa-folder-open-o',
                                'btn-remove-icon' => 'fa fa-trash',
                            ],
                            'jsCallbackFunctionDone' => "

                                var parent = $(e.target).parents('.form-group');
                                parent.removeClass('has-error');
                                parent.find('.help-block').hide();
                                
                                var imageUploaded = parent.find('.uploaded-file');
                                console.log(imageUploaded);
                                $('#crop-target').attr('src', imageUploaded.data('url'));
            
                                $('.UserImage_id').val('');
            
                                if (jcrop_api != undefined) {
                                    jcrop_api.destroy();
                                }
                                
                                $('#crop-target').Jcrop({
                                    minSize:  [10, 10],
                                    aspectRatio: 150 / 150,
                                    boxWidth: 555,
                                    boxHeight: 400,
                                    bgOpacity: .4
                                },function() {
                                    jcrop_api = this;
            
                                    jcrop_api.container.on('cropmove cropend',function(e,s,c){
                                        rwFileInput.x = c.x;
                                        rwFileInput.y = c.y;
                                        rwFileInput.w = c.w;
                                        rwFileInput.h = c.h;
                                        
                                        $('.btn-crop').show();
                                        $('.btn-crop').removeAttr('disabled');
                                        
                                    });
                                });
                                
                                $('#modal-jcrop').modal('show');
                                
                            ",
                                        'jsCallbackFunctionAfterDelete' => "
                                $('.user-avatar-edit img').attr('src', '/images/no-avatar.jpg');
                                
                                $.ajax({
                                    url: '/users/default/delete-avatar',
                                    type: 'post',
                                    dataType: 'json',
                                    data: {delete_avatar:1},
                                    success: function(data) {
                                        
                                    }
                                });
                            "
                        ]);?>
                    </div>

                </div>
                <div class="col-sm-8">
                    <div><span class="user-name"><?=$model->firstname?> <?=$model->lastname?></span></div>
                    <div class="gray-text mb-30">Дата регистрации: <?=$model->date_create?></div>
                    <div class="bonus-info">Бонусы: <span class="qt-bonus"><?=\common\modules\manicure\models\Bonuse::getUserBonus($model->id)?></span> <a href="#" class="gray-text"><span class="fa fa-question-circle-o"></span></a></div>

                    <div style="display: none">
                        <?php echo \nodge\eauth\Widget::widget(['action' => '/users/default/update']); ?>
                    </div>
                </div>
            </div>
            <h3 style="font-size: 18px;">Персональные данные</h3>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'firstname')->textInput(['maxlength' => 50]); ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'lastname')->textInput(['maxlength' => 50]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+9 (999) 999-99-99',
                    ]) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'email')->textInput(); ?>
                </div>
            </div>
            <div class="row mb-30">
                <div class="col-sm-6">
                    <?= $form->field($model, 'city_id')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \common\models\Cities::getCities(),
                        'options' => ['placeholder' => 'Выберите город ...'],
                    ]);
                    ?>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
            <h3 style="font-size: 18px;">Смена пароля</h3>
            <div class="row mb-30">
                <div class="col-sm-6">
                    <?= $form->field($model, 'newPassword')->passwordInput(); ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'confirmPassword')->passwordInput(); ?>
                </div>
            </div>
            <div class="row text-center">
                <button type="submit" class="btn btn-default">Сохранить</button>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php Modal::begin([
    'header' => '<h4>Выберите область на фото</h4>',
    'footer' => '
    <div class="col-md-12 text-left mb-10">Выберите область на фото, которая будет отображаться на Вашей странице</div>
    <div class="col-md-6 col-md-offset-3">
                        <button type="button" class="btn btn-default btn-block btn-crop" style="display:none" disabled="disabled">Сохранить</button>
                    </div>',
    'id' =>'modal-jcrop',
    'closeButton' => [
        'label' => ''
    ]
]);?>
<div class="crop-wrapper">
    <img id="crop-target" src="">
</div>
<?php Modal::end(); ?>
<?php
$this->registerJs("

        var jcrop_api;
        var tmpInput;

        $(document).on('click', '.btn-crop', function() {
            var btn = $(this);

            btn.attr('disabled', 'disabled');

            $.ajax({
                url: '".Url::toRoute(['/site/image-crop'])."',
                type: 'post',
                dataType: 'json',
                data: {x: rwFileInput.x, y: rwFileInput.y, w: rwFileInput.w, h: rwFileInput.h, id: rwFileInput.fileId},
                success: function(data) {
                    if (data.status) {
                    
                        $('.UserImage_id').val(rwFileInput.fileId);
                    
                        $('#modal-jcrop').modal('hide');
                        
                        $('.avatar img').attr('src', data.url);
                        
                        btn.removeAttr('disabled');
                    }
                }
            });
        })
        
        $('#modal-jcrop').on('hidden.bs.modal', function (e) {
            $('.btn-crop').hide();
            $('.btn-crop').attr('disabled', 'disabled');
        })
")

?>
