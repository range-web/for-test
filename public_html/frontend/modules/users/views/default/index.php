<?php
use yii\helpers\Html;

?>

<div class="container profile">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1 class="title">Личный кабинет</h1>
        </div>
        <div class="col-xs-12 col-sm-6 header-buttons">

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 tab-vertical">
            <?php
                $items = [
                    [
                        'label' => 'Мой профиль',
                        'content' => $this->render('_form', ['model'=>$model]),
                        'active' => true
                    ],
                    /*[
                        'label' => 'Бонусы',
                        'content' => $this->render('_bonus', ['model'=>$model]),
                    ],*/
                ];
            ?>

            <?= \yii\bootstrap\Tabs::widget([
                'items' => $items,
            ]);
            ?>
        </div>
    </div>
</div>
