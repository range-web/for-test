<div class="container">
    <div class="row">
        <div class="col-xs-12 col-lg-6 footer__bottom--left">
            &copy; Пекарня "Плюшки Ватрушки" 2015 - <?=date('Y');?>. Все права защищены
        </div>
        <div class="col-xs-12 col-lg-6 footer__bottom--right">
            <a href="/privacy">Политика конфиденциальности</a>
        </div>
    </div>
</div>