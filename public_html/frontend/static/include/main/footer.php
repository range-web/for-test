<div class="footer__contacts">
    <div class="footer__contacts-phone">8 (800) 555-55-55</div>
    <div class="footer__contacts-email">plushki-vatrushki@gmail.com</div>
    <div class="footer__contacts-working-time">пн-вc: 08:00 - 20:00</div>
</div>
<div class="footer__social-wrapper">
    <span class="footer__social-title">Мы в соц. сетях</span>

    <ul class="social-links">
        <li class="social-links__item">
            <a href="/" target="_blank"><span class="fa fa-vk"></span></a>
        </li>
        <li class="social-links__item">
            <a href="/" target="_blank"><span class="fa fa-odnoklassniki"></span></a>
        </li>
        <li class="social-links__item">
            <a href="/" target="_blank"><span class="fa fa-facebook"></span></a>
        </li>
    </ul>
</div>