<?php

namespace frontend\themes\basic;

class Theme
{
    const POS_HEADER = 1;
    const POS_LEFT_SIDEBAR = 2;
    const POS_FOOTER = 3;
    const POS_TOP_CENTER = 4;
    const POS_BOTTOM_CENTER = 5;
    const POS_HEADER_MENU = 6;
    const POS_HOME_PAGE = 7;
    const POS_FOOTER_MENU = 8;
    const POS_HOME_PAGE_SLIDER = 9;


    public static function listPositions()
    {
        return [
            self::POS_LEFT_SIDEBAR => 'Левая колонка',
            self::POS_TOP_CENTER => 'Над контентом',
            self::POS_BOTTOM_CENTER => 'Под контентом',
            self::POS_HEADER_MENU => 'Главное меню',
            self::POS_FOOTER_MENU => 'Нижнее меню',
            self::POS_HOME_PAGE => 'Главная страница',
            self::POS_HOME_PAGE_SLIDER => 'Слайдер на главной странице',
        ];
    }

    public static function layouts()
    {
        return [
            'column1' => 'Одна колонка',
        ];
    }

    /*public static function getPositions($noallow=true)
    {
        $positions = [
            self::POS_HEADER => 'Header',
            self::POS_LEFT_SIDEBAR => 'Left sidebar',
            self::POS_RIGHT_SIDEBAR => 'Right sidebar',
            self::POS_FOOTER => 'Footer',
            self::POS_TOP_CENTER => 'Top center',
            self::POS_BOTTOM_CENTER => 'Bottom center',

        ];

        if ($noallow) {
            $positions[self::POS_HEADER_MENU] = 'Header menu';
        }

        return $positions;

    }*/

    public static function getTitle($pos)
    {
        return self::listPositions()[$pos];
    }
}