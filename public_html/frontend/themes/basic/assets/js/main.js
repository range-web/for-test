$(document).ready(function() {
    var fullSlider = $('#full-slider'),
        userId = $('.user-id').val();

    if (fullSlider.length > 0 && fullSlider.find('.full-slider__item').length > 1) {

        fullSlider.owlCarousel({
            loop:true,
            margin:0,
            animateOut: 'fadeOut',
            mouseDrag: false,
            touchDrag: false,
            pullDrag: false,
            freeDrag: false,
            autoplay: true,
            autoplayTimeout: 7000,
            nav:false,
            items: 1,
            navText: ['','']
        });
    }

    $(document)
        .on('click', '.navbar-toggle', function(e) {
            $('.cnav-collapse').addClass('open');
        })
        .on('click', '.cnav-collapse__close', function(e) {
            $('.cnav-collapse').removeClass('open');
        });

});