<?php
use frontend\widgets\Alert;
use common\widgets\templatePositionsWidget\TplPosWidget;
use frontend\themes\basic\Theme;

?>
<?php $this->beginContent('@app/themes/basic/views/layouts/main.php'); ?>

    <?= $content; ?>

<?php $this->endContent();