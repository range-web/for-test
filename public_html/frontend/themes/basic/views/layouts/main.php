<?php
use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\models\Settings;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;

AppAsset::register($this);
$home = $this->context->homePage;
$showTitle = $this->context->showTitle;
$editType = false;
$isAdmin = (Yii::$app->user->can('superadmin') || Yii::$app->user->can('admin'));

if ($isAdmin) {

    if (isset($_GET['edit-type'])) {
        if ($_GET['edit-type'] == 1) {
            Yii::$app->session->set('edit-type', true);
        } else {
            Yii::$app->session->set('edit-type', false);
        }
    }

    $editType = Yii::$app->session->get('edit-type');
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->context->metaTitle) ?></title>
    <meta name="description" content="<?= Html::encode($this->context->metaDescription) ?>">
    <meta name="keywords" content="<?= Html::encode($this->context->metaKeywords) ?>">
    <meta name="author" content="rangeweb.ru">
    <?php if (!empty($this->context->shareInfo)) : ?>
        <meta name="title" property="og:title" content="<?= Html::encode($this->context->shareInfo['title']) ?>" />
        <meta name="description" property="og:description" content="<?= Html::encode($this->context->shareInfo['description']) ?>" />
        <meta name="image" property="og:image" content="<?= Html::encode($this->context->shareInfo['image']) ?>" />
        <meta name="url" property="og:url" content="<?= Html::encode($this->context->shareInfo['url']) ?>" />
        <script>
            var shareData = {
                title: '<?= Html::encode($this->context->shareInfo['title']) ?>',
                description: '<?= Html::encode($this->context->shareInfo['description']) ?>',
                image: '<?= Html::encode($this->context->shareInfo['image']) ?>',
                url: '<?= Html::encode($this->context->shareInfo['url']) ?>'
            }
        </script>
    <?php endif; ?>

    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/images/favicon/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="/images/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/images/favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/images/favicon/favicon-16x16.png" sizes="16x16">

    <?php $this->head() ?>
</head>
<body class="<?= $this->context->pageClass; ?><?=Yii::$app->user->isGuest ? '': ' user'?><?=($isAdmin) ? ' admin' : ''?><?=$editType ? ' edit-type' : ''?>">
<?php $this->beginBody() ?>

<div class="wrapper">
    <header class="header">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle  collapsed">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="header-top__contacts">
                                <?= \frontend\widgets\includeAreaWidget\IncludeAreaWidget::widget([
                                    'section' => 'main',
                                    'fileName' => 'header-top'
                                ]) ?>
                            </div>
                        </div>
                        <div class="cnav-collapse">
                            <button class="cnav-collapse__close"></button>
                            <?= \frontend\widgets\menu\MenuWidget::widget([
                                'source' => 'top',
                                'options' => [
                                    'class' => 'header-top__nav nav navbar-nav navbar-left'
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-top__logo-wrapper">
                <a href="/" class="header-top__logo"></a>
            </div>
        </div>
    </header>

    <?= $content; ?>
    
    <footer class="footer">
        <div class="footer__top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-lg-5">
                        <?= \frontend\widgets\menu\MenuWidget::widget([
                            'source' => 'bottom',
                            'options' => [
                                'class' => 'footer__nav nav'
                            ]
                        ]) ?>
                    </div>
                    <div class="col-xs-12 col-lg-2 footer__logo-wrapper">
                        <a href="/" class="footer__logo"></a>
                    </div>
                    <div class="col-xs-12 col-lg-5">
                        <?= \frontend\widgets\includeAreaWidget\IncludeAreaWidget::widget([
                            'section' => 'main',
                            'fileName' => 'footer'
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">
                    <?= \frontend\widgets\includeAreaWidget\IncludeAreaWidget::widget([
                        'section' => 'main',
                        'fileName' => 'footer-bottom'
                    ]) ?>
        </div>
    </footer>
</div>

<?php
/**
 * Подключаем панель администратора
 */

if ($isAdmin) : ?>
    <style>
        .admin-panel {
            width: 100%;
            padding: 10px 10px;
            border-top: 1px solid #999;
            background-color: #fff;
            z-index: 999;
        }
    </style>
    <div class="admin-panel">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?= Html::a('<span class="fa fa-cogs"></span> Админ панель', '/admin',['class'=>"btn btn-xs btn-default"]); ?>

                    <?php
                    if ($this->context->module->id == 'pages') {
                        echo Html::a('<span class="fa fa-edit"></span> Редактировать страницу', '/backend/web/index.php?r=pages%2Fdefault%2Fupdate&id='.$this->context->pageId,['class'=>"btn btn-xs btn-default"]);
                    }
                    ?>

                    <? if ($editType) {
                        echo Html::a('<span class="fa fa-edit"></span> Выключить режим редактирования', \yii\helpers\Url::current(['edit-type' => 0]),['class'=>"btn btn-xs btn-danger btn-default"]);
                    } else {
                        echo Html::a('<span class="fa fa-edit"></span> Включить режим редактирования', \yii\helpers\Url::current(['edit-type' => 1]),['class'=>"btn btn-xs btn-default"]);
                    } ?>
                    <?= Html::a('Выход', '/logout',['class'=>"btn btn-xs btn-default"]); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?= Settings::getCacheValue('jsCode') ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
