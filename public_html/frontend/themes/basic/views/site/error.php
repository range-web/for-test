<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="header-slider">
    <?=\frontend\widgets\pageSlider\PageSliderWidget::widget([])?>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php
            echo \yii\widgets\Breadcrumbs::widget([
                'itemTemplate' => "<li><i>{link}</i></li>\n",
                'links' => $this->params['breadcrumbs'],
            ]);
            ?>
        </div>
        <div class="col-xs-12">
            <h1 class="page__title"><?=$this->title?></h1>

            <div class="alert alert-danger">
                <?= nl2br(Html::encode($message)) ?>
            </div>
        </div>
    </div>
</div>
