<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Settings;
?>
<div class="success-message" style="display: none"></div>
<?php
$form = ActiveForm::begin([
    'id' => 'include-area-form',
    'action' => '/',
    'fieldConfig' => [
        'template' => "{input}\n{hint}\n{error}"
    ],
    'enableClientScript' => false,
]);
?>

<div class="row">
    <div class="col-md-12">
        <?= Settings::getRedactor($form,$model,'text')?>
        <?//=$form->field($model, 'text')->textarea(['rows'=>12])->label(false)?>
        <?=$form->field($model, 'fileName')->hiddenInput()?>

        <div class="form-group text-right">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            <?= Html::button('Отмена', ['class' => 'btn btn-warning', 'data-dismiss' => 'modal']) ?>
        </div>
    </div>
</div>

<?php
ActiveForm::end();
?>

