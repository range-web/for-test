<?php
namespace frontend\widgets;
use backend\modules\pages\models\BasePages;
use frontend\modules\pages\models\Pages;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


class Menu extends Widget
{
    public $options = [];
    public $items = [];
    public $encodeLabels = true;
    public $activateItems = true;
    public $activateParents = false;
    public $route;
    public $params;

    public $widgetModel;

    public function init()
    {
        parent::init();

        $menu = $this->widgetModel->menu;
        $this->items = $menu->arrayMenuItems;

        if ($menu->options != null) {
            $this->options['class'] = $menu->options;
        }

        /*if (!empty($menu->options['class'])) {
            $this->options['class'] = $menu->options['class'];
        }*/

        if ($this->route === null && Yii::$app->controller !== null) {
            $this->route = Yii::$app->controller->getRoute();
        }
        if ($this->params === null) {
            $this->params = Yii::$app->request->getQueryParams();
        }
        //Html::addCssClass($this->options, 'nav nav-justified');
    }

    public function run()
    {
        echo $this->renderItems();
    }

    public function renderItems()
    {
        $items = [];
        foreach ($this->items as $i => $item) {
            if (isset($item['visible']) && !$item['visible']) {
                unset($items[$i]);
                continue;
            }
            $items[] = $this->renderItem($item);
        }

        $this->options['class'] = 'nav';
        return Html::tag('ul', implode("\n", $items), $this->options);
    }

    public function renderItem($item)
    {

        if (is_string($item)) {
            return $item;
        }
        if (!isset($item['title'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $label = $encodeLabel ? Html::encode($item['title']) : $item['title'];
        $options = ArrayHelper::getValue($item, 'options', []);
        $items = ArrayHelper::getValue($item, 'children');
        $url = ArrayHelper::getValue($item, 'url', '#');
        $number = ArrayHelper::getValue($item, 'number', []);
        $number = str_pad($number, 2, '0', STR_PAD_LEFT);

        $pageId = ArrayHelper::getValue($item, 'pageid', []);

        if ($pageId) {
            $url = '/'.BasePages::getLinkById($pageId);
        }

        $url = str_replace('//', '/', $url);

        $label = $label .'<span>'.$number.'</span>';

        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);

        if (isset($item['active'])) {
            $active = ArrayHelper::remove($item, 'active', false);
        } else {
            $active = $this->isItemActive($url);
        }

        if ($items !== null) {
            $linkOptions['data-toggle'] = 'dropdown';
            Html::addCssClass($options, 'dropdown');
            Html::addCssClass($linkOptions, 'dropdown-toggle');
            $label .= ' ' . Html::tag('b', '', ['class' => 'caret']);
            if (is_array($items)) {
                if ($this->activateItems) {
                    $items = $this->isChildActive($items, $active);
                }
                $items = $this->renderDropdown($items, $item);
            }
        }

        if ($this->activateItems && $active) {
            Html::addCssClass($options, 'active');
        }

        if (strpos($url, 'login') !== false) {
            if (Yii::$app->user->isGuest) {
                /*$linkOptions['class'] .= ' btn-open-modal';
                $linkOptions['data-size'] .= 'modal-xs';
                $linkOptions['data-header'] = 'Регистрация';*/
                //$html = Html::tag('li', Html::a('Регистрация', ['/join'], $linkOptions) . $items, $options);
                //$linkOptions['data-header'] = 'Авторизация';
                $html .= Html::tag('li', Html::a('Вход', ['/login'], $linkOptions) . $items, $options);
                return $html;
            } else {
                $linkOptions['data-method'] = 'post';
                return Html::tag('li', Html::a('Личный кабинет', ['/docs'], $linkOptions) . $items, $options);
            }
        }

        return Html::tag('li', Html::a($label, $url, $linkOptions) . $items, $options);
    }

    protected function renderDropdown($items, $parentItem)
    {
        return Dropdown::widget([
            'items' => $items,
            'encodeLabels' => $this->encodeLabels,
            'clientOptions' => false,
            'view' => $this->getView(),
        ]);
    }

    protected function isChildActive($items, &$active)
    {
        foreach ($items as $i => $child) {
            if (ArrayHelper::remove($items[$i], 'active', false) || $this->isItemActive($child)) {
                Html::addCssClass($items[$i]['options'], 'active');
                if ($this->activateParents) {
                    $active = true;
                }
            }
        }
        return $items;
    }

    protected function isItemActive($url)
    {


        $currentUrl = explode('/', $_SERVER ["REQUEST_URI"]);
        $currentUrl = preg_replace('/\?.*/', '', $currentUrl[1]);
        $url = explode('/', $url);

        if ($currentUrl == $url[1]) {
            return true;
        }

        /*if (isset($item['url']) && $item['url'] == $_SERVER ["REQUEST_URI"]) {
            return true;
        }*/

        /*if (isset($item['url']) && is_array($item['url']) && isset($item['url'][0])) {
            $route = $item['url'][0];
            if ($route[0] !== '/' && Yii::$app->controller) {
                $route = Yii::$app->controller->module->getUniqueId() . '/' . $route;
            }

            if (ltrim($route, '/') !== $this->route) {
                return false;
            }
            unset($item['url']['#']);
            if (count($item['url']) > 1) {
                foreach (array_splice($item['url'], 1) as $name => $value) {
                    if ($value !== null && (!isset($this->params[$name]) || $this->params[$name] != $value)) {
                        return false;
                    }
                }
            }

            return true;
        }*/

        return false;
    }
}
