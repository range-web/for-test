<?php
namespace frontend\widgets\includeAreaWidget;
use yii\web\AssetBundle;

class IncludeAreaAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/includeAreaWidget/assets';
    public $css = [
        'css/style.css'
    ];
    public $js = [
        'js/script.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}