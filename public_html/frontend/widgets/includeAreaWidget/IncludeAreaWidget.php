<?php
namespace frontend\widgets\includeAreaWidget;

use Yii;
use yii\base\Widget;
use yii\helpers\FileHelper;
use yii\helpers\Html;

class IncludeAreaWidget extends Widget
{
    public $section;
    public $fileName;
    public $filePath;

    public $canEdit = false;

    public function init()
    {
        parent::init();
        
        if ($this->fileName != null) {

            if ($this->section != null) {
                $this->fileName = $this->section .'/'.$this->fileName;
            }
            
            $this->filePath = FileHelper::normalizePath(Yii::getAlias('@frontend/static/include/'.$this->fileName.'.php'));
        }

        $this->canEdit = (!Yii::$app->user->isGuest && (Yii::$app->user->can('superadmin') || Yii::$app->user->can('admin')));
    }

    public function run()
    {

        if (file_exists($this->filePath)) {

            $this->registerClientScript();

            return $this->render('index', [
                'id' => 'inclide_'.$this->getId(),
                'canEdit' => $this->canEdit,
                'section' => $this->section,
                'fileName' => $this->fileName,
                'file' => $this->filePath,
            ]);
        }
    }

    public function registerClientScript()
    {
        if ($this->canEdit) {
            $view = $this->getView();

            IncludeAreaAsset::register($view);
        }
    }
}