$(function() {

    $('body').append('<div id="modal-area-edit" class="fade modal" role="dialog" tabindex="-1">' +
        '<div class="modal-dialog modal-lg">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>' +
        '<h4>Редактирование</h4>' +
        '</div>' +
        '<div class="modal-body"></div>' +
        '</div>' +
        '</div>' +
        '</div>');

    var activeIncludeWidget;

    var modalWrapper = $('#modal-area-edit'),
        modalBody = modalWrapper.find('.modal-body');

    $(document)
        .on('click', '.btn-area-edit', function() {
            var parent = $(this).parents('.include-area');

            getAreaEditor(parent.data('area-file-name'), parent.attr('id'));
        })
        .on('dblclick', '.edit-type  .include-area', function() {
            getAreaEditor($(this).data('area-file-name'), $(this).attr('id'));
        })
        .on('submit', '#include-area-form', function(e) {
            e.preventDefault();

            var form = $(this);
            var successMessageWrapper = $('#modal-area-edit .success-message');

            $.ajax({
                url: '/site/save-include-area',
                type: 'post',
                dataType: 'json',
                data: form.serialize(),
                success: function(data) {
                    if (data.status) {
                        successMessageWrapper.text('Успешно сохранено').show();

                        $('#'+activeIncludeWidget+' .content').html($('#includeareaform-text').val());

                        setTimeout(function() {
                            successMessageWrapper.hide();
                        }, 3000);
                    }
                    
                }
            });

            return false;
        });

    function getAreaEditor(file_name, widget_id)
    {
        activeIncludeWidget = widget_id;
        $.ajax({
            url: '/site/get-include-area',
            type: 'post',
            data: {file_name:file_name},
            beforeSend: function() {},
            success: function(html) {

                modalBody.html(html);
                modalWrapper.modal('show');
            }
        });
    }

    function showEditModal(data)
    {

    }
    
});