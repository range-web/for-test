<?php

namespace frontend\widgets\includeAreaWidget\models;

use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;

/**
 * ContactForm is the model behind the contact form.
 */
class IncludeAreaForm extends Model
{
    public $text;
    public $fileName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['text'], 'required'],
            [['text', 'fileName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Текст',
            'fileName' => 'Файл',
        ];
    }

    public function save()
    {
        $filePath = FileHelper::normalizePath(Yii::getAlias('@frontend/static/include/'.$this->fileName.'.php'));

        if (file_exists($filePath)) {
            file_put_contents($filePath, $this->text);
            return true;
        }

        return false;
    }
}
