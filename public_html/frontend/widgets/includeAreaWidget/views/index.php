<?php
use yii\bootstrap\Modal;
?>
<?php if ($canEdit) :?>
    <div id="<?= $id ?>" class="include-area" data-area-file-name="<?=$fileName?>">
        <button type="button" class="btn btn-info btn-xs btn-area-edit" style="font-size: 12px;height: 24px;padding: 2px 6px;"><span class="fa fa-edit"></span> Редактировать</button>
        <div class="content">
<?php endif;?>

<?php include $file ?>

<?php if ($canEdit) :?>

        </div>
    </div>
<?php endif;?>
