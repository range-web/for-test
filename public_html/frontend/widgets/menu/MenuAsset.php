<?php
namespace frontend\widgets\menu;
use yii\web\AssetBundle;

class MenuAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/menu/assets';
    public $css = [
        'css/style.css'
    ];
    public $js = [
        'js/script.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}