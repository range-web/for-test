<?php
namespace frontend\widgets\menu;

use frontend\modules\pages\models\Pages;
use Yii;
use yii\base\Widget;
use yii\helpers\FileHelper;
use yii\helpers\Html;

class MenuWidget extends Widget
{
    public $source;
    public $options;
    public $filePath;
    public $canEdit = false;

    public function init()
    {
        parent::init();
        $this->filePath = FileHelper::normalizePath(Yii::getAlias('@frontend/static/include/menu/menu.' . $this->source . '.json'));
        $this->canEdit = (!Yii::$app->user->isGuest && (Yii::$app->user->can('superadmin') || Yii::$app->user->can('admin')));
    }

    public function run()
    {
        if (file_exists($this->filePath)) {

            $file = file_get_contents($this->filePath);
            
            $arMenu = [];
            
            if ($file) {
                $arMenu = json_decode($file, true);

                if (is_array($arMenu) && !empty($arMenu)) {
                    foreach ($arMenu as &$item) {

                        $item['active'] = false;

                        if (strpos($item['url'], '[~') !== false) {

                            $id = filter_var($item['url'], FILTER_SANITIZE_NUMBER_INT);

                            $item['url'] = Pages::getLinkById($id);
                        }

                        if ($item['url'] == '/') {
                            if ($_SERVER["REQUEST_URI"] == '/') {
                                $item['active'] = true;
                            }
                            continue;
                        }

                        if (strpos($_SERVER["REQUEST_URI"], $item['url']) === 0) {
                            $item['active'] = true;
                        }
                    }
                }
            }

            $this->registerClientScript();

            return $this->render('index', [
                'id' => 'menu_'.$this->getId(),
                'source' => $this->source,
                'options' => $this->options,
                'arMenu' => $arMenu,
                'canEdit' => $this->canEdit
            ]);
        }
    }

    public function registerClientScript()
    {
        if ($this->canEdit) {
            MenuAsset::register($this->getView());
        }
    }
}