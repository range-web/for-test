<?php

namespace frontend\widgets\menu\actions;

use Yii;
use yii\base\DynamicModel;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;
use yii\web\BadRequestHttpException;
use \yii\web\Response;
use yii\web\UploadedFile;
use yii\base\Action;
use yii\helpers\FileHelper;
use rangeweb\filesystem\models\File;

/**
 * UpdateAction.
 *
 * Usage:
 * ```php
 * public function actions()
 * {
 *      return [
 *          'upload' => [
 *              'class' => 'frontend\widgets\menuWidget\actions\UpdateAction',
 *          ]
 *      ];
 * }
 *
 * ```
 */
class SaveAction extends Action
{
    public $callback;

    public function init()
    {
        
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (Yii::$app->request->isPost) {
            
            $result = ['status' => false];

            if (!Yii::$app->user->isGuest && (Yii::$app->user->can('superadmin') || Yii::$app->user->can('admin'))) {
                
                if (isset($_POST['Menu'])) {
                    $filePath = FileHelper::normalizePath(Yii::getAlias('@frontend/static/include/menu/menu.' . $_POST['Menu']['type'] . '.json'));
                    if (file_exists($filePath)) {
                        $arResult = [];

                        foreach ($_POST['Menu']['items']['title'] as $k=>$v) {
                            if (strlen($v) == 0 || strlen($_POST['Menu']['items']['url'][$k]) == 0) {
                                continue;
                            }
                            $arResult[] = [
                                'title' => $v,
                                'url' => $_POST['Menu']['items']['url'][$k]
                            ];
                        }

                        file_put_contents($filePath, json_encode($arResult));

                        $result = ['status' => true];
                    }
                }
            }
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $result;
        } else {
            throw new BadRequestHttpException('Only POST is allowed');
        }
    }

    /**
     * @param $imageId
     * @return mixed
     * @throws InvalidConfigException
     */
    protected function callback($imageId)
    {
        if (!is_callable($this->callback)) {
            throw new InvalidConfigException('"' . get_class($this) . '::callback" should be a valid callback.');
        }
        $response = call_user_func($this->callback, $imageId);
        return $response;
    }
}
