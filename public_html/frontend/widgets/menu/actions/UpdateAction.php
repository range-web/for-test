<?php

namespace frontend\widgets\menu\actions;

use Yii;
use yii\base\DynamicModel;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;
use yii\web\BadRequestHttpException;
use \yii\web\Response;
use yii\web\UploadedFile;
use yii\base\Action;
use yii\helpers\FileHelper;
use rangeweb\filesystem\models\File;

/**
 * UpdateAction.
 *
 * Usage:
 * ```php
 * public function actions()
 * {
 *      return [
 *          'upload' => [
 *              'class' => 'frontend\widgets\menuWidget\actions\UpdateAction',
 *          ]
 *      ];
 * }
 *
 * ```
 */
class UpdateAction extends Action
{
    public $callback;

    public function init()
    {
        
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (Yii::$app->request->isPost) {
            
            $result = ['status' => false];

            if (!Yii::$app->user->isGuest && (Yii::$app->user->can('superadmin') || Yii::$app->user->can('admin'))) {
                $filePath = FileHelper::normalizePath(Yii::getAlias('@frontend/static/include/menu/menu.' . $_POST['menu'] . '.json'));

                if (file_exists($filePath)) {

                    $file = file_get_contents($filePath);

                    $arMenu = json_decode($file, true);
                    
                    if ($arMenu) {
                        $result = [
                            'status' => true,
                            'items' => $arMenu
                        ];
                    }
                }
            }
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $result;
        } else {
            throw new BadRequestHttpException('Only POST is allowed');
        }
    }

    /**
     * @param $imageId
     * @return mixed
     * @throws InvalidConfigException
     */
    protected function callback($imageId)
    {
        if (!is_callable($this->callback)) {
            throw new InvalidConfigException('"' . get_class($this) . '::callback" should be a valid callback.');
        }
        $response = call_user_func($this->callback, $imageId);
        return $response;
    }
}
