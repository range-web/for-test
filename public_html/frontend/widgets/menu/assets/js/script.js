$(function() {
    
    var activeIncludeWidget;
    
    $(document)
        .on('click', '.btn-menu-edit', function() {
            var parent = $(this).parents('.edit-menu');

            getMenuEditor(parent.data('menu'), parent.attr('id'));
        })
        .on('dblclick', '.edit-menu', function() {
            getMenuEditor($(this).data('menu'), $(this).attr('id'));
        })
        .on('click', '.btn-new-menu-item', function() {
            var modal = $(this).parents('.modal'),
                menuItems = modal.find('.edit-menu__items');

            menuItems.append('<div class="edit-menu__item">' +
                '<div class="form-group">' +
                '<div class="row">' +
                '<div class="col-sm-4">' +
                '<input name="Menu[items][title][]" class="form-control" value="">' +
                '</div>' +
                '<div class="col-sm-6">' +
                '<input name="Menu[items][url][]" class="form-control" value="">' +
                '</div>' +
                '<div class="col-sm-2 text-right">' +
                '<button type="button" class="btn btn-sm btn-info edit-menu__item_up"><span class="fa fa-chevron-up"></span></button>' +
                '<button type="button" class="btn btn-sm btn-info edit-menu__item_down"><span class="fa fa-chevron-down"></span></button>' +
                '<button type="button" class="btn btn-sm btn-danger edit-menu__item_delete"><span class="fa fa-trash"></span></button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>');
        })
        .on('click', '.edit-menu__item_delete', function(e) {
            e.preventDefault();

            var parent = $(this).parents('.edit-menu__item');

            parent.remove();
        })
        .on('click', '.edit-menu__item_up', function(e) {
            e.preventDefault();
            var parent = $(this).parents('.edit-menu__item'),
                prev = parent.prev();

                 prev.before(parent);
        })
        .on('click', '.edit-menu__item_down', function(e) {
            e.preventDefault();
            var parent = $(this).parents('.edit-menu__item'),
                next = parent.next();

            next.after(parent);
        })
        .on('submit', '.edit-menu__form', function(e) {
            e.preventDefault();
            var form = $(this);

            $.ajax({
                url: '/site/save-menu',
                type: 'post',
                dataType: 'json',
                data: form.serialize(),
                success: function(data) {
                    if (data.status) {
                        location.reload();
                        //form.parents('.modal').modal('hide');
                    }
                }
            });
        });

    function getMenuEditor(menu, widget_id)
    {
        var modalWrapper = $('#modal-edit-' + widget_id),
            menuItems = modalWrapper.find('.edit-menu__items');

        activeIncludeWidget = widget_id;

        $.ajax({
            url: '/site/edit-menu',
            dataType: 'json',
            type: 'post',
            data: {menu:menu},
            success: function(data) {

                menuItems.empty();
                
                if (data.items) {
                    $.each(data.items, function(i, v) {
                        
                        menuItems.append('<div class="edit-menu__item">' +
                            '<div class="form-group">' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input name="Menu[items][title][]" class="form-control" value="'+v.title+'">' +
                            '</div>' +
                            '<div class="col-sm-6">' +
                            '<input name="Menu[items][url][]" class="form-control" value="'+v.url+'">' +
                            '</div>' +
                            '<div class="col-sm-2 text-right">' +
                            '<button type="button" class="btn btn-sm btn-info edit-menu__item_up"><span class="fa fa-chevron-up"></span></button>' +
                            '<button type="button" class="btn btn-sm btn-info edit-menu__item_down"><span class="fa fa-chevron-down"></span></button>' +
                            '<button type="button" class="btn btn-sm btn-danger edit-menu__item_delete"><span class="fa fa-trash"></span></button>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>');
                    });
                } 

                modalWrapper.modal('show');
            }
        });
    }

});