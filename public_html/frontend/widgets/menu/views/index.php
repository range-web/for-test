<?php
use yii\bootstrap\Modal;

?>
<?php if ($canEdit) :?>

    <ul id="<?= $id ?>" class="<?=(isset($options['class'])) ? $options['class'] . ' ' : ''?>edit-menu"  data-menu="<?=$source?>">
        <button type="button" class="btn btn-info btn-xs btn-menu-edit"><span class="fa fa-edit"></span> Редактировать меню</button>
       
<?php else : ?>
    <ul <?=(isset($options['class'])) ? 'class="' .$options['class'] . '"' : ''?>>
<?php endif;?>

    <?php foreach ($arMenu as $menuItem) : ?>
        <li>
            <a href="<?=$menuItem['url']?>" <?=$menuItem['active'] ? 'class="active"' : ''?>><?=$menuItem['title']?></a>
        </li>
    <?php endforeach; ?>

    </ul>
<?php if ($canEdit) :?>


    <?php
    $this->registerJs("
        $(function() {
            $('body').append('<div id=\"modal-edit-".$id."\" class=\"fade modal\" role=\"dialog\" tabindex=\"-1\">' +
                        '<div class=\"modal-dialog modal-lg\">' +
                            '<div class=\"modal-content\">' +
                                '<div class=\"modal-header\">' +
                                    '<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>' +
                                    '<h4>Редактирование меню</h4>' +
                                '</div>' +
                                '<div class=\"modal-body\">' +
                                '<div class=\"row\">' +
                                '<div class=\"col-md-4\">Наименование</div>' +
                                '<div class=\"col-md-6\">Ссылка</div>' +
                                '<div class=\"col-md-2\"></div>' +
                                '</div>' +
                                '<form class=\"edit-menu__form\">' +
                                    '<input type=\"hidden\" name=\"Menu[type]\" value=\"" . $source . "\">' +
                                    '<div class=\"edit-menu__items\"></div>' +
                                    '<div class=\"row buttons\">' +
                                        '<div class=\"col-md-4\">' + 
                                        '<button type=\"button\" class=\"btn btn-new-menu-item\">Добавить пункт меню</button>' +
                                        '</div>' +
                                        '<div class=\"col-md-8 text-right\">' +
                                        '<button type=\"submit\" class=\"btn btn-success edit-menu__form_save\">Сохранить</button>' +
                                        '<button type=\"button\" class=\"btn btn-warning edit-menu__form_cancel\"  data-dismiss=\"modal\">Отмена</button>' +
                                        '</div>' +
                                    '</div>' +
                                '</form>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>');   
        });
    
    ")
    ?>

<?php endif;?>