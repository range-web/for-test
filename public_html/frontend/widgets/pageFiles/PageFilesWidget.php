<?php
/**
 * Created by PhpStorm.
 * User: hunter
 * Date: 28.08.15
 * Time: 0:34
 */

namespace frontend\widgets\pageFiles;

use common\models\PageFile;
use rangeweb\filesystem\models\File;
use yii\base\Widget;

class PageFilesWidget extends Widget
{
    public $templateItems = '<div><a href="#link#">#title#</a></div>';
    public $tempplateWrapper = '<div class="page-files">#items#</div>';
    public $pageId;

    public function run()
    {
        if ($this->pageId > 0) {

             $pageFiles = PageFile::find()->where([
                 'page_id' => $this->pageId
             ])->all();

            echo $this->render('index', ['pageFiles' => $pageFiles]);
        }
    }
}