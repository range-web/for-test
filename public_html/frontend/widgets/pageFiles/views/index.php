<?php if(!empty($pageFiles)) :?>
    <div class="page-files">
        <?php foreach ($pageFiles as $pageFile) {

            $file = $pageFile->file;

            if (strlen($file->name) > 0) {
                $name = $file->name;
            } else {
                $name = $file->original_name;
            }

            echo '<div class=file-item"><a href="'.$file->getUrlToFile().'" title="'.$name.'">'.$name.'</a></div>';
        } ?>
    </div>
<?php endif;?>
