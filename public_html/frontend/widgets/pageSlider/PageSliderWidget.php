<?php
/**
 * Created by PhpStorm.
 * User: hunter
 * Date: 28.08.15
 * Time: 0:34
 */

namespace frontend\widgets\pageSlider;

use common\models\PageFile;
use frontend\modules\pages\models\Pages;
use rangeweb\filesystem\models\File;
use yii\base\Widget;

class PageSliderWidget extends Widget
{
    public $page;

    public function run()
    {
        if (!$this->page || !$this->page->slides) {
            $this->page = Pages::findOne(1);
        }

        $slides = $this->page->slides;

        if (!empty($slides)) {

            foreach ($slides as &$slide) {
                
                if (strlen($slide->content) > 0) {
                    if (strpos($slide->content, '<br>') !== false) {
                        $content = '';
                        $arContent = explode('<br>', $slide->content);

                        $i = 0;
                        $countRows = count($arContent);
                        foreach ($arContent as $row) {
                            $content .= '<span>' . $row . '</span>';

                            $i++;
                            if ($i < $countRows) {
                                $content .= '<br>';
                            }
                        }

                        $slide->content = $content;
                    } else {
                        $slide->content = '<span>' . $slide->content . '</span>';
                    }
                }
            }

            return $this->render('full-slider', [
                'slides' => $slides,
            ]);
        }
        
        return '';
    }
}