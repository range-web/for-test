<div class="full-slider">
    <div id="full-slider" class="full-slider__items">
        <?php foreach ($slides as $slide) : ?>
            <div class="full-slider__item" style="background-image: url(<?=$slide->getImageUrl([2560, 2560], $imageQuality=80)?>)">
                <?php if (strlen($slide->content) > 0) : ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="full-slider__item_content_wrapper">
                                    <div class="full-slider__item_content">
                                        <h2>
                                            <?=$slide->content?>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>