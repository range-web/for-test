<div class="row">
    <div class="col-xs-12 col-lg-8 col-lg-offset-2 main-slider">
        <div id="main-slider" class="main-slider">
            <?php foreach ($slides as $slide) : ?>
                <div class="main-slider__item">
                    <img src="<?=$slide->getImageUrl([1140, 1140], $imageQuality=80)?>" alt="">
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>