UPDATE b_uts_user SET UF_REG_SOURCE = '/my/auth/registration/' WHERE UF_IS_B2C = 1 AND UF_HOW_DID_YOU_HEAR = 'registration1'
UPDATE b_uts_user SET UF_REG_SOURCE = '/my/auth/registration2/' WHERE UF_IS_B2C = 1 AND UF_HOW_DID_YOU_HEAR = 'registration2'
UPDATE b_uts_user SET UF_REG_SOURCE = 'my.sokolov.ru' WHERE UF_IS_B2C = 1 AND UF_HOW_DID_YOU_HEAR NOT IN ('registration1', 'registration2')